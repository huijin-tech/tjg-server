<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Rych\\Random' => array($vendorDir . '/rych/random'),
    'PHPExcel' => array($vendorDir . '/phpoffice/phpexcel/Classes'),
    'BaconQrCode' => array($vendorDir . '/bacon/bacon-qr-code/src'),
);
