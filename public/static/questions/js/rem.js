(function (){

    function changeRem() {
        var width = $(window).width();
        var rem = width / 10;
        $('html').css({
            fontSize: rem
        });
    }
    changeRem();
    $(window).resize(changeRem);
})();