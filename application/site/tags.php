<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2018/1/8
 * Time: 9:20
 */
return [
    'app_init' => [\app\common\behaviour\AppInit::class],
    'module_init' => [\app\site\behaviour\ModuleInit::class],
];