<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/8/31
 * Time: 13:35
 */

namespace app\site\controller;


use app\api\model\special\SpecialColumn;
use app\api\model\special\UserSubscribeSpecial;
use app\api\model\user\Users;
use app\common\controller\ClientAuth;
use app\common\model\AgencySalesman;
use app\common\model\teacher\TeacherEssay;

class Share
{

    public function essay_detail(int $id, ClientAuth $ca) {
        return $this->column_essay_detail($id, $ca);
    }

    public function column_essay_detail(int $id, ClientAuth $ca) {
        $uid = checkUserLogin();

        $essay = TeacherEssay::apiDetail($id, $uid);
        $unlock = true;
        if ($essay['special_column_id']>0) {
            $column = SpecialColumn::get($essay['special_column_id']);
            if ($column) {
                if ($column->isFree()) {
                    $unlock = true;
                } else {
                    if ($uid > 0) {
                        $subscribed = UserSubscribeSpecial::checkBought($uid, $column->column_id);
                        if ($subscribed)
                            $unlock = true;
                        else
                            $unlock = false;
                    } else {
                        $unlock = $ca->isAPP();
                    }
                }
            }
        }
        if (!$unlock) $essay['content'] = mb_substr(stripHtml($essay['content']),0,400);

        return view('column_essay_detail', [
            'essay' => $essay,
            'unlock' => $unlock,
        ]);
    }

    public function invite_page($invite_code = null) {
        $inviteUser = Users::get(['extension_code' => $invite_code]);
        if ($inviteUser) {
            $invite_code = $inviteUser->extension_code;
            $name = $inviteUser->nickname?:mobile_hidden($inviteUser->mobile);
        } else {
            $invite_code = '';
            $name = '';
        }

        return view('invite_page', [
            'invite_code' => $invite_code,
            'name' => $name,
        ]);
    }

    public function live() {
        return view('live', [
            'server' => config('server.host'),
        ]);
    }

    public function wechat_invite(){
        return view('wechat_invite');
    }

}