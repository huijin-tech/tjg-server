<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2018/1/31
 * Time: 11:26
 */

namespace app\site\behaviour;


use app\common\controller\ClientAuth;
use think\Request;

class ModuleInit
{
    public function run(& $args) {

        $ca = new ClientAuth();
        $ca->auth(false);
        Request::instance()->bind('ca', $ca);

    }
}