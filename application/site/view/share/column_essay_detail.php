<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2018/1/19
 * Time: 9:44
 */

use app\common\utils\DateTimeUtil;

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?=$essay['title']?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no"/>
    <style>
        body{
            margin: 0;
            padding: 0;
            background: #ffffff;
        }
        .main{
            width: 6.7rem;
            margin: 0 auto;
        }
        .title{
            font-size: 0.36rem;
            color: #333333;
            font-weight: bold;
            width: 100%;
            margin-top: 0.3rem;
        }
        .teacher_info{
            margin-top: 0.3rem;
            height: 0.62rem;
            line-height: 0.62rem;
        }
        .teacher_info .left{
            float: left;
        }
        .teacher_info .left img{
            width: 0.62rem;
            height: 0.62rem;
            border-radius: 50%;
            float: left;
        }
        .teacher_info .left span{
            float: left;
            font-size: 0.24rem;
            color: #989898;
            margin-left: 0.16rem;
        }
        .teacher_info .right{
            float: right;
            font-size: 0.24rem;
            color: #989898;
        }
        .text_info_box{
            width: 100%;
            background: #F5F5F5;
            padding: 0.2rem 0;
            margin-top: 0.3rem;
        }
        .text_info_box .box{
            width: 6rem;
            margin: 0 auto;
            font-size: 0.3rem;
            color: #989898;
        }
        .line{
            width: 100%;
            border-bottom: 1px solid #F2F2F2;
            margin-top: 0.4rem;
            /*margin-bottom: 0.4rem;*/
        }
        .voice_box{
            width: 100%;
            height: 1.4rem;
            background: #f9f9f9;
            margin-top: 0.2rem;
        }
        .voice_box .left_icon{
            width: 1rem;
            height: 1rem;
            margin: 0.14rem 0.2rem;
            border-radius: 50%;
            float: left;
        }
        .voice_box .left_icon img{
            width: 1rem;
            height: 1rem;
        }
        .voice_box .right_box{
            float: left;
            width: 4.78rem;
            margin-top: 0.4rem;
        }
        .voice_box .right_box .title_box{
            width: 100%;
            height: 0.3rem;
            line-height: 0.3rem;
            overflow: hidden;
            font-size: 0.28rem;
            color: #989898;
        }
        .voice_box .right_box .time{
            color: #c9c9c9;
            font-size: 0.24rem;
            margin-top: 0.05rem;
        }
        .textImg_box{
            width: 100%;
            margin: 0.2rem 0;
        }
        .textImg_box .content,
        .textImg_box .content *{
            max-width: 100%!important;
            font-family: "Microsoft Yahei", "Helvetica Neue", Helvetica, Arial, sans-serif!important;
            font-size: 0.3rem!important;
            line-height: 1.6em!important;
            margin: 0!important;
            padding: 0!important;
            word-break: break-all;
            <?php if (!$unlock):?>
            text-indent: 2em!important;
            <?php endif;?>
        }
        .textImg_box .content img{
            max-width: 100%!important;
            height: auto!important;
        }
        .video-box-lock,
        .video-box{
            /*width: 100%;*/
            /*height: 3.77rem;*/
            margin-top: 0.2rem;
            position: relative;
        }
        .video-box-lock{
            width: 100%;
            height: 3.77rem;
            line-height: 3.77rem;
            text-align: center;
            vertical-align: middle;
            background: #555;
        }
        .video-box-lock .play-btn{
            z-index: 99;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            text-align: center;
            vertical-align: middle;
        }
        .video-box-lock .cover{
            max-width: 100%;
            max-height: 100%;
        }
        .video-box-lock .play-btn>img{
            width: 1rem;
        }
        .audio_box{
            width: 100%;
            display: none;
        }
        #stop{
            display: none;
        }
        .middle-align{
            position: relative;
        }
        chimee-screen{display: none}
        chimee-total-time{margin-right: 1em}
    </style>
</head>
<body>
<div class="main" id="main" style="display: none">
    <div class="title"><?=$essay['title']?></div>
    <div class="teacher_info">
        <div class="left">
            <img src="<?=$essay['avatar']??'/favicon.ico'?>" >
            <span><?=$essay['teacher_name']??''?></span>
        </div>
        <div class="right"><?=$essay['created']?></div>
    </div>
    <div class="text_info_box">
        <div class="box" id="summary"><?=$essay['summary']?></div>
    </div>
    <div class="line"></div>
    <?php if (isset($essay['attachment']->audio)):?>
    <div class="voice_box">
        <div class="left_icon">
            <img src="/static/img/ic_zl_paly@2x.png" alt="" id="start">
            <img src="/static/img/ic_zl_pause@2x.png" alt="" id="stop">
        </div>
        <div class="right_box">
            <div class="title_box"><?=$essay['title']?></div>
            <div class="time">
                <span><?= DateTimeUtil::formatDuration($essay['attachment']->audio->duration??0)?></span>
                <progress id="audio-progress" style="width: 60%;" value="0">
            </div>
        </div>
    </div>
    <div class="audio_box"><!--音频容器，放音频链接-->
        <?php if ($unlock):?>
        <audio id="audio" src="<?=$essay['attachment']->audio->url?>"></audio>
        <?php endif?>
    </div>
    <?php endif?>
    <?php if (isset($essay['attachment']->video)):?>
    <?php if ($unlock):?>
    <div class="video-box" id="video-box">
        <video id="video-player" tabindex="-1" webkit-playsinline></video>
    </div><!--视频容器-->
    <?php else:?>
    <div class="video-box-lock">
        <img class="cover middle-align" src="<?=$essay['cover']?>">
        <div class="play-btn"><img id="play-btn" src="http://image.cloud.51taojingu.com/static/img/play-btn.png"></div>
    </div><!--视频容器-->
    <?php endif?>
    <?php endif?>

    <div class="textImg_box">
        <div class="content" id="content"><?=$essay['content']?></div>
    </div><!--图文容器，图片大小控制见js-->

</div>

</body>
<script src="//cdn.bootcss.com/jquery/3.2.1/jquery.js"></script>
<script src="/static/js/chimee-mobile-player.browser.js"></script>
<script>
    !function (window) {

        /* 设计图文档宽度 */
        var docWidth = 750;

        var doc = window.document,
            docEl = doc.documentElement,
            resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize';

        var recalc = (function refreshRem () {
            var clientWidth = docEl.getBoundingClientRect().width;

            /* 8.55：小于320px不再缩小，11.2：大于420px不再放大 */
            docEl.style.fontSize = Math.max(Math.min(20 * (clientWidth / docWidth), 11.2), 8.55) * 5 + 'px';

            return refreshRem;
        })();

        /* 添加倍屏标识，安卓为1 */
        docEl.setAttribute('data-dpr', window.navigator.appVersion.match(/iphone/gi) ? window.devicePixelRatio : 1);

        if (/iP(hone|od|ad)/.test(window.navigator.userAgent)) {
            /* 添加IOS标识 */
            doc.documentElement.classList.add('ios');
            /* IOS8以上给html添加hairline样式，以便特殊处理 */
            if (parseInt(window.navigator.appVersion.match(/OS (\d+)_(\d+)_?(\d+)?/)[1], 10) >= 8)
                doc.documentElement.classList.add('hairline');
        }

        if (!doc.addEventListener) return;
        window.addEventListener(resizeEvt, recalc, false);
        doc.addEventListener('DOMContentLoaded', recalc, false);

    }(window);

</script>
<script>
    window.InIOS = (typeof webkit!=='undefined') &&
        (typeof webkit.messageHandlers!=='undefined') &&
        (typeof webkit.messageHandlers.checkStatus!=='undefined') ;
    window.iosEnablePlay = false;

    /**
     * 提供给app的方法
     * @param param
     */
    function globalPlay(param) {
        if (typeof player !== "undefined" && player) {
            window.iosEnablePlay = true;
            player.play();
        }
        if (typeof audio !== "undefined" && audio) {
            audio.play();
        }
    }
    /**
     * 提供给app的方法
     * @param param
     */
    function globalPause(param) {
        if (typeof player !== "undefined" && player) {
            window.iosEnablePlay = false;
            player.pause();
        }
        if (typeof audio !== "undefined" && audio) {
            audio.pause();
        }
    }

    function checkAllowed() {
        <?php if ($unlock):?>
        return true;
        <?php else:?>
        if (typeof checkStatus !== "undefined") {
            return checkStatus.showInfoFromJs();
        }

        // ios用观测者模式交互，这里发送通知
        if (InIOS) {
            if (iosEnablePlay) {
                window.iosEnablePlay = false;
                return true;
            } else {
                window.webkit.messageHandlers.checkStatus.postMessage("调用iOS");
            }
        }

        return false;
        <?php endif?>
    }

    <?php if ($unlock):?>
    <?php if (isset($essay['attachment']->video)):?>
    window.player = new ChimeeMobilePlayer({
        wrapper: '#video-box',
        src: "<?=$essay['attachment']->video->url?>",
        poster: "<?=$essay['cover']?>",
        box:'native',
        autoplay: false,
        controls: false,
        playsInline: true,
        preload: true,
        x5VideoPlayerFullscreen: false,
        x5VideoOrientation: true,
        xWebkitAirplay: true
    });
    player.addEventListener('beforePlay', function () {
        if (checkAllowed() === false) return false;
        this.controls = true;
    });
    player.addEventListener('beforeFullscreen', function () {
        return false;
    });

    <?php endif?>

    <?php if (isset($essay['attachment']->audio)):?>
    window.audio = document.getElementById('audio');
    audio.addEventListener('play', function (e) {
        $('#start').hide();
        $('#stop').show();
    });
    audio.addEventListener('pause', function (e) {
        $('#stop').hide();
        $('#start').show();
    });
    audio.addEventListener('timeupdate', function (e) {
        var t = e.target;
        $("#audio-progress").val(t.currentTime / t.duration)
    });
    $(document).on('touchstart', '#start',function () {
        if (checkAllowed() === false) return;
        audio.play();
    });
    $(document).on('touchstart', '#stop',function () {
        audio.pause();
    });
    <?php endif;?>
    <?php else:?>
    $(document).on('touchstart', '#play-btn', function () {
        checkAllowed()
    });
    $(document).on('touchstart', '#start', function () {
        checkAllowed()
    });
    <?php endif;?>


    $("#main").fadeIn(200);

    $(function () {
        $(".middle-align").each(function () {
            var div = this.parentNode;
            var img = this;
            var _img = new Image();
            _img.src = this.src;
            _img.onload = function () {
                var imgRate = this.height / this.width;
                var divRate = $(div).height() / $(div).width();
                if (imgRate > divRate) {
                } else {
                    var h = this.height / (this.width / $(div).width());
                    var t = ($(div).height() - h) / 2;
                    $(img).css({top: t});
                }
            };
        })
    })
</script>
</html>
