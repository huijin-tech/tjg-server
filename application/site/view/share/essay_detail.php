<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/8/31
 * Time: 13:45
 *
 * @var $essay array
 */


?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>文章</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <style>
        body{
            margin: 0;
            padding: 0;
            font-size: 0.274rem;
        }
        .title_box{
            max-height: 2.54rem;
            /*border-bottom: 1px solid #c5c5c5;*/
            font-size: 0.5rem;
        }
        .title_box .title{
            overflow: hidden;
            max-height: 1.6rem;
            line-height: 0.8rem;
            margin-top: 0.4rem;
        }
        .title_box .title_teacher{
            height: 1rem;
            line-height: 1rem;
            color: #666666;
            font-size: 0.3rem;
        }
        .content_box{
            background: #ffffff;
        }
        .content_box .content{
            margin-top: 0.5rem;
        }
        .jianjie_box{
            background: #e6f2ff;
            line-height: 0.6rem;
        }
        .jianjie_left_box{
            width: 0.7rem;
            height: 0.7rem;
            border-left: 0.16rem solid #4b90fc;
            border-top: 0.16rem solid #4b90fc;
            /*float: left;*/
        }
        .jianjie_right_box{
            width: 0.7rem;
            height: 0.7rem;
            float: right;
            border-right: 0.16rem solid #4b90fc;
            border-bottom: 0.16rem solid #4b90fc;
            /*clear: both;*/
        }
        .jianjie_midle_box{
            width: 8.6rem;
            /*clear: both;*/
            margin:  0 auto ;
        }
        .nav_margin{
            width: 9.5rem;
            margin: 0 auto;
        }
        .clear{clear: both}
        .fontsizi_2{
            font-size: 0.32rem;
        }
        .fontsizi_1{
            font-size: 0.4rem;
        }
        .content_text *{
            font-size: 0.4rem!important;
            line-height: 0.7rem!important;
            margin: 0!important;
        }
    </style>
</head>
<body>
<div class="title_box">
    <div class="nav_margin">
        <div class="title"><strong><?=$essay['title']?></strong></div>
        <div class="title_teacher">
            <span class="fontsizi_2"><?=$essay['teacher_name']?></span>
            <span style="margin-left: 0.5rem">
                <?=$essay['created']?>
            </span>
        </div>
    </div>
</div>
<div class="jianjie_box nav_margin fontsizi_1">
    <div class="jianjie_left_box"></div>
    <div class="jianjie_midle_box"><?=$essay['summary']?></div>
    <div class="jianjie_right_box"></div>
    <div class="clear"></div>
</div>
<div class="content_box">
    <div class="content_text nav_margin"><?=$essay['content']?></div>
</div>
</body>
<script src="https://cdn.bootcss.com/jquery/3.2.0/jquery.min.js"></script>
<script>
    (function (){

        var imgArr = $(".content_text img");
        $.each(imgArr, function(){
            $(this).attr('class','imgWitch')
            $('.imgWitch').css('max-width','100%')
        });

        function changeRem() {
            var width = $(window).width();
            var rem = width / 10;
            $('html').css({
                fontSize: rem
            });
        }
        changeRem();

        $(window).resize(changeRem);

    })();
</script>
</html>