<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>直播</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="stylesheet" href="/static/css/live.css">
    <link href="//nos.netease.com/vod163/nep.min.css" rel="stylesheet">
    <script src="//nos.netease.com/vod163/nep.min.js"></script>
</head>
<body>
<div class="live_box" id="publisher">
    <!--<video id="my-video" class="video-js vjs-big-play-centered vjs-fluid"  x-webkit-airplay="allow" webkit-playsinline controls preload="auto" data-setup="{}">-->
    <!--<source  src="http://pullhls3f1b8d8b.live.126.net/live/a0a4d8f3c2894effb0989365b469b6ea/playlist.m3u8" type="application/x-mpegURL">-->
    <!--</video>-->
</div>
<div class="list_box">
    <div class="list_title">
        <div>时间</div>
        <div>节目名称</div>
        <div>老师</div>
        <!--<div>操作</div>-->
    </div>
    <div id="box">
    </div>
</div>
<a href="#"><img class="down" src="/static/img/dowm.png" alt=""></a>
</body>
<script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
<!--<script src="https://cdn.bootcss.com/vue/2.4.2/vue.js"></script>-->
<script>
    (function (){

        function changeRem() {
            var width = $(window).width();
            var rem = width / 10;
            $('html').css({
                fontSize: rem
            });
        }
        changeRem();
        $(window).resize(changeRem);
    })();

    $.ajax({
        url:"<?=$server?>/api/live/share",    //请求的url地址
        headers: {
            appid: '1',
            appsecret:'39b3567d0b8ac89c025b78beec26acb0'
        },
        dataType:"json",   //返回格式为json
        async:true,//请求是否异步，默认为异步，这也是ajax重要特性
        data:{},    //参数值
        type:"post",   //请求方式 get 或者post
        success:function(req){
            //请求成功时处理
            console.log(req.data)
            var data = req.data;
            console.log('测试测试',data.list)
            for(var i=0;i<data.list.length;i++){
                console.log('测试测试');
                $('#box').append(
                    '           <div class="list">'+
                    '                <div>'+data.list[i].live_s_time+'-'+data.list[i].live_e_time+'</div>'+
                    '                <div>'+data.list[i].live_title+'</div>'+
                    '                <div>'+data.list[i].teacher_name+'</div>'+
                    //                    '                <div>1</div>'+
                    '            </div>');
            }
            if(data.current == null){
                $('.list_title').css('margin-top','0')
                $('#publisher').html('<img src="img/notList.png" alt="">');
            }else {
                if(data.current.is_fee == false){
                    $('.list_title').css('margin-top','1rem')
                    var video_html = '<video id="my-video" class="video-js vjs-big-play-centered vjs-fluid" poster="'+data.current.cover+'"  x-webkit-airplay="allow" webkit-playsinline controls preload="auto" data-setup="{}">'+
                        '            <source  src="'+data.current.pull_url+'" type="application/x-mpegURL">'+
                        '        </video>';
                    $('#publisher').html(video_html);
                    var myPlayer = neplayer("my-video", {"preload": "auto"}, function(){});
                    myPlayer.release();
                    $('#publisher').html(video_html);
                    var myPlayer = neplayer("my-video", {"preload": "auto"}, function(){});
                    myPlayer.play();
                }
                if(data.current.is_fee == true){
                    $('.list_title').css('margin-top','0')
                    $('#publisher').html('<img src="'+data.current.cover+'" alt="">');
                }
            }

        },
        complete:function(){
            //请求完成的处理
        },
        error:function(){
            //请求出错处理
        }
    });
</script>
</html>