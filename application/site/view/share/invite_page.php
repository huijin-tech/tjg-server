<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>注册送特权-掌乾财经</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1.0, user-scalable=no"/>
    <link href="https://cdn.bootcss.com/layer/3.1.0/mobile/need/layer.css" rel="stylesheet">
    <style>
        body{
            background: #0b0a62;
            margin: 0 auto;
            padding: 0;
            font-size: 0.2rem;
            max-width: 420px;
        }
        img{
            display: block;
            pointer-events: none;
        }
        .content_background{
            width: 100%;
            /*height: 5rem;*/
            max-width: 420px;
            margin: 0 auto;
        }
        .content_background img{
            width: 100%;
        }

        .logo img{
            width: 1.6rem;
        }
        .text img{
            width: 100%;
        }

        .xiangzi img{
            width: 4.48rem;

        }
        .share_name{
            color: #ffffff;
            position: absolute;
            z-index: 2;
            text-align: center;
            width: 100%;
            top: 2.6rem;
            max-width: 420px;
            margin: 0 auto;
        }
        .share_name span{
            color: #de7e0d;

        }


        .activity_box .title img{
            width: 2.82rem;
        }

        .register_input_box{
            width: 6.7rem;
            margin: 0 auto;

        }
        .register_input_box .beijing{
            width: 100%;
            padding-top: 0.6rem;
            position: relative;
            z-index: -1;
            /*height: 3.96rem;*/
        }
        .register_content{
            width: 6.2rem;
            /*height: 3.96rem;*/
            /*margin-top: -4.5rem;*/
            margin:  -4.7rem auto;
        }
        .clear{
            clear: both;
        }
        .register_input_txet{
            text-align: center;
            color: #ffffff;
            margin-top: 0.3rem;
        }
        .register_input_txet a{
            color: #068ae8;
            text-decoration: none;

        }
        .register_alert_text{
            color: #ffffff;
            clear: both;
            padding-top: 1.4rem;
            padding-bottom: 0.4rem;
            width: 6.7rem;
            margin: 0 auto;
            text-align: center;
        }
        .register_alert_text span{
            color: #ce7514;
        }




        .content{
            clear: both;
            /*padding-top: 1rem;*/
        }

        .login_input_box{
            width: 6rem;
            height: 0.88rem;
            background: #F7FAFF;
            border-radius: 0.08rem;
            border: 1px solid #4B90FC;
            clear: both;
            /*margin-left: 0.2rem;*/
            margin-top: 0.3rem;
            float: left;
        }

        .login_input_box .left_icon{
            float: left;
            width: 0.28rem;
            margin-left: 0.2rem;
            margin-top: 0.2rem;
        }
        .login_input_box .left_icon img{
            width: 0.5rem;
        }
        .phone{
            float: left;
            margin-left: 0.3rem;
            font-size: 0.28rem;
            height: 0.5rem;
            margin-top: 0.19rem;
            border: 0;
            outline: none;
            background: transparent;
            width: 4.6rem;
        }
        .yzm_btn{
            float: right;
            width: 2rem;
            height: 0.88rem;
            text-align: center;
            color: #ffffff;
            background: #4B90FC;
            line-height: 0.8rem;
            font-size: 0.26rem;

            outline: none;
            margin-right: 0.25rem;
            margin-top: 0.3rem;
            border-radius: 0.08rem;
            border: 1px solid #4b90fc;
            /*border-bottom-left-radius: 0;*/
            /*border-top-left-radius: 0;*/
            /*position: relative;*/
            /*height: 100%;*/
        }
        .register{
            text-align: center;
            color: #ffffff;
            background: #4B90FC;
            font-size: 0.34rem;
            line-height: 0.88rem;
        }
        .golink{
            clear: both;
            font-size: 0.26rem;
            color: #ffffff;
            width: 100%;
            text-align: center;
            padding-top: 0.3rem;
        }
        .golink a{
            color: #8FFFE7;
            text-decoration: none;
        }
        .info span{
            color: #FEB240;
        }
    </style>
</head>
<body>
<div class="share_name">Hi~,我是 <span><?=$name?></span></div>
<div class="content_background">
    <img src="/static/AppShare/img/new.png" alt="">
</div>
<div class="content">
    <div class="register_input_box">
        <img class="beijing" src="/static/AppShare/img/00000.png" alt="">
        <div class="register_content">
            <div class="login_input_box">
                <div class="left_icon">
                    <img src="/static/AppShare/img/phone.png" alt="">
                </div>
                <input class="phone" id="phone" type="text" maxlength="11">
            </div>
            <div class="login_input_box" style="width: 3.4rem">
                <div class="left_icon">
                    <img src="/static/AppShare/img/yzm.png" alt="">
                </div>
                <input class="phone" style="width: 2rem" id="yzmNum" maxlength="4" type="text" maxlength="11">
            </div>
            <input  class="yzm_btn" id="yzm" type="button" value="获取验证码"/>
            <div class="login_input_box register" id="register">注册</div>
            <div class="clear"></div>
            <div class="register_input_txet">点击“注册”即同意 <a href="http://www.51zqcj.com//app_page/agreement.html">《用户协议和隐私条款》</a></div>
        </div>
        <div class="register_alert_text">*活动解释权归掌乾财经所有，详情咨询掌乾财经 <br>小助手 <span>（微信：zqcj2017）</span></div>
    </div>
</div>
</body>
<script src="//cdn.bootcss.com/jquery/3.2.1/jquery.js"></script>
<script>
    /**
     * YDUI 可伸缩布局方案
     * rem计算方式：设计图尺寸px / 100 = 实际rem  【例: 100px = 1rem，32px = .32rem】
     */
    !function (window) {

        /* 设计图文档宽度 */
        var docWidth = 750;

        var doc = window.document,
            docEl = doc.documentElement,
            resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize';

        var recalc = (function refreshRem () {
            var clientWidth = docEl.getBoundingClientRect().width;

            /* 8.55：小于320px不再缩小，11.2：大于420px不再放大 */
            docEl.style.fontSize = Math.max(Math.min(20 * (clientWidth / docWidth), 11.2), 8.55) * 5 + 'px';

            return refreshRem;
        })();

        /* 添加倍屏标识，安卓为1 */
        docEl.setAttribute('data-dpr', window.navigator.appVersion.match(/iphone/gi) ? window.devicePixelRatio : 1);

        if (/iP(hone|od|ad)/.test(window.navigator.userAgent)) {
            /* 添加IOS标识 */
            doc.documentElement.classList.add('ios');
            /* IOS8以上给html添加hairline样式，以便特殊处理 */
            if (parseInt(window.navigator.appVersion.match(/OS (\d+)_(\d+)_?(\d+)?/)[1], 10) >= 8)
                doc.documentElement.classList.add('hairline');
        }

        if (!doc.addEventListener) return;
        window.addEventListener(resizeEvt, recalc, false);
        doc.addEventListener('DOMContentLoaded', recalc, false);

    }(window);
</script>
<script>
    /**
     * 访问统计客户端
     * Created by Administrator on 2016/5/23.
     */


    /**
     * 全局变量初始化
     */
    var Gcfg = {
        //默认提交地址
        url : "/api/app/wechat_jsapi_sign",
        //初始化分享失败
        initFx : -1,
        //访问提交数据
        init : 0,
        //尝试分享提交
        tryFx : 1,
        //分享成功提交
        SucFx : 2,
        //取消分享
        CanFx : 3,
        //分享错误
        ErrFx : 4,
        //项目唯一名字
        proName : null,

        //分享服务器计算
        fenxiangreq : "/site/share/invite_page",
        //分享的url,要到服务器验证的本项目地址
        fenxiangurl : getProjectPath(),
        //分享的title :
        fenxiangtitle : "分享",
        //分享的描述
        fenxiangdesc : "分享描述",
        //分享链接
        fenxianglink : getProjectPath(),
        //分享缩略图
        fenxiangimage : "http://image.cloud.51taojingu.com/static//static/AppShare/img/logo_1.png",
        //点击分享回调
        fenxiangcallback : null,
        //分享成功回调
        fenxiangsuccall: null
    };

    /**
     * 初始化
     * @param params
     * 可包含参数
     * {
     * url:服务器地址
     * name:项目名
     * }
     */
    function init(params){
        params = params || {};
        for(var key in params){
            Gcfg[key] = params[key];
        }
        initfenxiang();
        sendToServer(Gcfg.init);
    }

    function sendToServer(type){
        //项目名字
        var name = getProjectName();
        $.ajax({
            url:Gcfg.url,
            type:"POST",
            dataType:"text",
            headers: {
                appid: "1",
                appsecret: "39b3567d0b8ac89c025b78beec26acb0"
            },
            data : {
                url: location.origin + '/site/share/invite_page'
            },
            error: function(data){
                console.log("erro ",data);
            },
            success: function(data){
                console.log("success ",data);
            }
        });
    }

    function getProjectName(){
        if(Gcfg.proName != null)
            return Gcfg.proName;
        var url = window.location;
        var name = url.pathname.substr(1,url.pathname.length);
        var index = name.indexOf("/");
        if(index == -1) {
            return name;
        }
        return "fwl_" + name.substr(0,index);
    }

    function getProjectPath(){
        var url = window.location.href;
        return url;
    }


    /**
     微信分享
     */

    function initfenxiang(){
        $.ajax({
            url: Gcfg.fenxiangreq,
            type: "post",
            headers: {
                appid: "1",
                appsecret: "39b3567d0b8ac89c025b78beec26acb0"
            },
            data:{"url":Gcfg.fenxiangurl},
            dataType:"json",
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                /* alert(XMLHttpRequest.status);
                 alert(XMLHttpRequest.readyState);
                 alert(textStatus);
                 */
            },
            success: function (data) {
                wx.config({
                    debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
                    appId: data.appId, // 必填，公众号的唯一标识
                    timestamp: data.timestamp, // 必填，生成签名的时间戳
                    nonceStr: data.nonceStr, // 必填，生成签名的随机串
                    signature: data.signature,// 必填，签名，见附录1
                    jsApiList: [// 必填，需要使用的JS接口列表，所有JS接口列表见附录2
                        'checkJsApi',
                        'onMenuShareTimeline',
                        'onMenuShareAppMessage',
                        'onMenuShareQQ',
                        'onMenuShareWeibo',
                        'hideMenuItems',
                        'showMenuItems',
                        'hideAllNonBaseMenuItem',
                        'showAllNonBaseMenuItem',
                        'translateVoice',
                        'startRecord',
                        'stopRecord',
                        'onRecordEnd',
                        'playVoice',
                        'pauseVoice',
                        'stopVoice',
                        'uploadVoice',
                        'downloadVoice',
                        'chooseImage',
                        'previewImage',
                        'uploadImage',
                        'downloadImage',
                        'getNetworkType',
                        'openLocation',
                        'getLocation',
                        'hideOptionMenu',
                        'showOptionMenu',
                        'closeWindow',
                        'scanQRCode',
                        'chooseWXPay',
                        'openProductSpecificView',
                        'addCard',
                        'chooseCard',
                        'openCard'
                    ]
                });


                wx.ready(function () {
                    // config信息验证后会执行ready方法，所有接口调用都必须在config接口获得结果之后，config是一个客户端的异步操作，所以如果需要在页面加载时就调用相关接口，则须把相关接口放在ready函数中调用来确保正确执行。对于用户触发时才调用的接口，则可以直接调用，不需要放在ready函数中。
                    wx.onMenuShareTimeline({
                        title: Gcfg.fenxiangtitle,
                        link: Gcfg.fenxianglink,
                        imgUrl: Gcfg.fenxiangimage,
                        trigger: function (res) {
                            // 不要尝试在trigger中使用ajax异步请求修改本次分享的内容，因为客户端分享操作是一个同步操作，这时候使用ajax的回包会还没有返回
                            //this.title = getTitle();
                            if(Gcfg.fenxiangcallback != null) {
                                var change = Gcfg.fenxiangcallback();
                                this.title = change.title != null ? change.title : this.title;
                                this.link = change.link != null ? change.link : this.link;
                                this.imgUrl = change.imgUrl != null ? change.imgUrl : this.imgUrl;
                            }
                            sendToServer(Gcfg.tryFx);
                        },
                        success: function (res) {
                            sendToServer(Gcfg.SucFx);
                            if(Gcfg.fenxiangsuccall != null) {
                                Gcfg.fenxiangsuccall();
                            }
                        },
                        cancel: function (res) {
                            sendToServer(Gcfg.CanFx);
                        },
                        fail: function (res) {
                            sendToServer(Gcfg.ErrFx);
                        }
                    });

                    wx.onMenuShareAppMessage({
                        title: Gcfg.fenxiangtitle,
                        desc: Gcfg.fenxiangdesc, // 分享描述
                        link: Gcfg.fenxianglink,
                        imgUrl: Gcfg.fenxiangimage,
                        trigger: function (res) {
                            // 不要尝试在trigger中使用ajax异步请求修改本次分享的内容，因为客户端分享操作是一个同步操作，这时候使用ajax的回包会还没有返回
                            if(Gcfg.fenxiangcallback != null) {
                                var change = Gcfg.fenxiangcallback();
                                this.title = change.title != null ? change.title : this.title;
                                this.link = change.link != null ? change.link : this.link;
                                this.imgUrl = change.imgUrl != null ? change.imgUrl : this.imgUrl;
                                this.desc = change.desc != null ? change.desc : this.desc;
                            }
                            sendToServer(Gcfg.tryFx);
                        },
                        success: function (res) {
                            sendToServer(Gcfg.SucFx);
                            if(Gcfg.fenxiangsuccall != null) {
                                Gcfg.fenxiangsuccall();
                            }
                        },
                        cancel: function (res) {
                            sendToServer(Gcfg.CanFx);
                        },
                        fail: function (res) {
                            sendToServer(Gcfg.ErrFx);
                        }
                    });
                });

                wx.error(function (res) {
                    // config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，也可以在返回的res参数中查看，对于SPA可以在这里更新签名。
                    sendToServer(Gcfg.initFx);
                });
            }
        });
    }


</script>
<script src="https://cdn.bootcss.com/layer/3.1.0/mobile/layer.js"></script>
<script type="text/javascript" src="//res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
<script>
    var countdown=60;
    var yzmDisable = false;
    var duan;
    if ((navigator.userAgent.match(/(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i))) {
//        alert('手机端')
        duan = 'touchstart';

    }else {
//        alert('PC端')
        duan = 'click';
    }
    $(document).on(duan, '#yzm',function (obj) {
        if (yzmDisable) {
            return false;
        }

        var phone = $('#phone').val();
        if( phone.length<1){
            alert('手机号码不能为空哦~！');
            return false
        }
        if(!(/^1[3-9]\d{9}$/.test(phone))){
            alert('请检查您输入的手机号码~！');
            return false
        }

        yzmDisable = true;
        $.ajax({
            url:"/api/sms/send_code",    //请求的url地址
            headers: {
                appid: "1",
                appsecret: "39b3567d0b8ac89c025b78beec26acb0"
            },
            dataType:"json",   //返回格式为json
            async:true,//请求是否异步，默认为异步，这也是ajax重要特性
            data:{'mobile':phone},    //参数值
            type:"post",   //请求方式
            success:function(req){
                //请求成功时处理
            },
            error:function(){
                //请求出错处理
            }
        });

        function settime() {
            var yzm = $('#yzm');

            if (countdown == 0) {
                yzm.attr("disabled");
                yzm.val('获取验证码');
                yzmDisable = false;
                countdown = 60;
                return;
            } else {
                yzm.attr("disabled", true);
                yzm.val("重新发送(" + countdown + ")");
                yzmDisable = true;
                countdown--;
                console.log(countdown+'秒后再试~');
            }
            setTimeout(settime,1000)
        }
        settime();
    });
    $(document).on('touchstart', '#register',function () {
        console.log('dssads')
        var phone = $('#phone').val();
        var yzmNum = $('#yzmNum').val();
        if( phone.length<1){
            alert('手机号码不能为空哦~！');
            return false
        }
        if(!(/^1[3-9]\d{9}$/.test(phone))){
            alert('请检查您输入的手机号码~！');
            return false
        }
        if( yzmNum.length<1){
            alert('验证码不能为空~！');
            return false
        }

        $.ajax({
            url:"/api/user/fast_login",    //请求的url地址
            headers: {
                appid: "1",
                appsecret: "39b3567d0b8ac89c025b78beec26acb0"
            },
            dataType:"json",   //返回格式为json
            async:true,//请求是否异步，默认为异步，这也是ajax重要特性
            data:{'mobile':phone,'code':yzmNum,'invite_code':'<?=$invite_code?>'},    //参数值
            type:"post",   //请求方式
            success:function(res){
                if (200 == res.code) {
                    layer.open({
                        content: '注册成功，进入掌乾财经!',
                        skin: 'msg',
                        time: 1, //2秒后自动关闭
                        end: function () {
                            location.href = 'http://wechat.zqcj.net.cn/#/course'
                        }
                    });
                } else if(3000 == res.code) {
                    layer.open({
                        content: '登录成功！',
                        skin: 'msg',
                        time: 1, //2秒后自动关闭
                        end: function () {
                            location.href = 'http://wechat.zqcj.net.cn/#/course'
                        }
                    });
                }else{
                    layer.open({
                        content: res.msg,
                        skin: 'msg',
                        time: 5 //2秒后自动关闭
                    });
                }
            },
            error:function(){
                //请求出错处理
            }
        });



    })

    init({
        fenxiangcallback: function () {
            var backInfo = {};
            backInfo.title = "掌乾财经，最有趣的金融知识学习社区！";
            backInfo.link = "http://image.cloud.51taojingu.com/static//static/AppShare//static/AppShare/img/invite/logo_1.png";
            backInfo.desc = "免费课程、免费战法、免费直播，通通免费！";
            return backInfo;
        }
    });

</script>
</html>