<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2018/1/2
 * Time: 12:53
 */
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>掌乾财经</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <style>
        body{
            margin: 20px 0 0;
            padding: 0;
        }
        header h3{
            color: #555;
            text-align: center;
        }
        .info{
            width: 100%;
            float: left;
            padding: 20px 0;
            border-width: 1px 0;
            border-style: solid;
            border-color: #BBB;
            color: #555;
            background: #fff8f5;
        }
        .info .item{
            width: 100%;
            overflow: hidden;
            padding: 2px 0;
        }
        .info .item>div{
            float: left;
        }
        .info .item .name{
            width: 32%;
            text-align: right;
        }
        .info .item .value{
            width: 64%;
            float: right;
        }
        .loading{
            text-align: center;
            height: 100px;
            width: 100%;
            padding: 40px 0;
            float: left;
        }
        .loading img{width: 50px; height: 50px;}
    </style>
</head>
<body>
<header>
    <h3>微信支付</h3>
</header>
<main>
    <div class="info">
        <div class="item" style="color: grey">
            <div class="name">市场价：</div>
            <div class="value" style="text-decoration: line-through;">&yen;<?=$bill['bill_payable_amount']??''?></div>
        </div>
        <div class="item" style="color: red">
            <div class="name">购买价：</div>
            <div class="value">&yen;<?=$bill['bill_pay_amount']??''?></div>
        </div>
        <div class="item">
            <div class="name">购买内容：</div>
            <div class="value"><?=$bill['bill_item_title']?></div>
        </div>
        <div class="item">
            <div class="name">信息：</div>
            <div class="value"><?=$bill['bill_item_detail']?></div>
        </div>
    </div>
    <div class="loading">
        <img src="http://image.cloud.51taojingu.com/static/img/loading-1.gif">
        <div id="tip-call-pay" style="padding-top: 10px; color: gray">请稍候，正在拉起微信支付……</div>
        <div id="tip-jump-wap" style="padding-top: 10px; color: gray;display: none"><span id="err_msg"></span>! 请稍候，正在跳转……</div>
    </div>
</main>
<footer></footer>
<script>
    var successUrl = '<?=$success_url?>';
    var cancelUrl = '<?=$cancel_url?>';
    //调用微信JS api 支付
    function jsApiCall(params)
    {
        WeixinJSBridge.invoke(
            'getBrandWCPayRequest',
            params,
            function(res) {
                $("#tip-call-pay").hide();
                $("#tip-jump-wap").show();
                WeixinJSBridge.log(res.err_msg);
                if (res.err_msg == "get_brand_wcpay_request:ok") {
                    $("#err_msg").text('支付成功');
                    location.href = successUrl
                } else {
                    $("#err_msg").text('您已取消支付');
                    location.href = cancelUrl
                }
            }
        );
    }

    function order() {
        $.ajax({
            url: '/api/buy/order_pay',
            type: 'POST',
            headers: JSON.parse('<?=$headers?>'),
            data: JSON.parse('<?=$params?>'),
            success: function (resp) {
                if (resp.code != 200) {
                    alert(resp.msg);
                    location.href = cancelUrl;
                    return
                }
                jsApiCall(JSON.parse(resp.data.weixinpay));
            }
        })
    }

    function callpay()
    {
        if (typeof WeixinJSBridge == "undefined"){
            if( document.addEventListener ){
                document.addEventListener('WeixinJSBridgeReady', order, false);
            }else if (document.attachEvent){
                document.attachEvent('WeixinJSBridgeReady', order);
                document.attachEvent('onWeixinJSBridgeReady', order);
            }
        }else{
            order();
        }
    }

    $(function () {
        callpay();
    })

</script>
</body>
</html>
