<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: yunwuxin <448901948@qq.com>
// +----------------------------------------------------------------------

return [

    // 检测节目直播状态，并对状态进行更新
    \app\admin\command\LiveStatusScan::class,

    // 直播节目预约节目开播提醒
    \app\admin\command\LiveMakeRemind::class,
    // 直播节目预约数据状态更新
    \app\admin\command\LiveMakeStatus::class,

    // 数据库迁移工具
    \app\admin\command\Migrate::class,

    // 测试用
    "\app\admin\command\Test",

    // 客服状态刷新
    \app\admin\command\CustomerServiceStatus::class,

    // 直播频道清理
    \app\admin\command\LiveChannelClean::class,

    // 新浪新股日历数据采集
    \app\admin\command\CollectionSinaNewStock::class,

    // 重新老师统计老师的文章数、视频数、语音数
    \app\admin\command\TeacherNum::class,

    // 网易命令工具
    \app\admin\command\Netease::class,

    // 客户名单导入
    \app\admin\command\ImportCustomer::class,

    // 创建短信队列与发送短信
    \app\admin\command\SmsTask::class,

    // apple pay 清除沙盒订单
    \app\admin\command\AppleClean::class,

    // 问答状态修改与用户积分返还
    \app\admin\command\AskStatus::class,

    // 直播间在线人数更新
    \app\admin\command\LiveOnline::class,

    // 直播间状态更新
    \app\admin\command\LiveChannelStatus::class,

    //神箭手采集行业数据
    \app\admin\command\ShenjianNews::class,
    \app\admin\command\LiveVideoUrl::class,
];
