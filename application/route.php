<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\Route;
// 'method'=>'post'
// 前台业务接口
Route::group('api', function () {

    // 用户
    Route::group('user', [
        // 快速登录
        'fast_login' => ['api/User/fast_login', /*['method'=>'post']*/],

        // 普通登录
        'login' => ['api/User/login', /*['method'=>'post']*/],

        // 退出登录
        'logout' => ['api/User/logout', /*['method'=>'post']*/],

        // 密码找回
        'pass_reset' => ['api/User/pass_reset', /*['method'=>'post']*/],

        // 修改密码(登录状态下)
        'update_password' => ['api/User/update_password', ['method'=>'post']],

        // 我的课程（已购课程）
        'buy_curriculum' => ['api/User/user_buy_curriculum', ['method'=>'post']],
        // 我的直播（已购买已预约的直播）
        'my_live' => ['api/User/user_buy_live', ['method'=>'post']],
        // 我的专栏 v1.3
        'special' => ['api/User/user_special', ['method'=>'post']],

        // 技能雷达图
        'ability' => ['api/User/user_ability', ['method'=>'post']],

        // 实名认证
        'realname' => ['api/User/user_real_name', ['method'=>'post']],
        'invite/clients' => ['api/User/myInvitedUsersList', ['method'=>'post|get']],
    ]);

    // 直播
    Route::rule('live', 'api/Live/live_list', 'post');
    Route::group('live', [
        // 直播节目详情
        'detail' => ['api/Live/live_detail', ['method'=>'post']],

        // 直播节目预约
        'make' => ['api/Live/live_make', ['method'=>'post']],

        // 直播节目点赞
        'like' => ['api/Live/live_like', ['method'=>'post']],

        'share' => ['api/Live/live_share'],

        // V1.1
        'program' => ['api/Live/live_program', ['method'=>'post']],

        'operate_suggest' => ['api/Live/operate_suggest', ['method' => 'post']],

        'shaidan' => ['api/Live/shaidan', ['method' => 'post']],

        'room_list' => ['api/Live/room_list'],
        'room_detail' => ['api/Live/room_detail'],
        'playback_list' => ['api/Live/playback_list'],
        'playback_filters' => ['api/Live/playback_filters'],
        'playback_detail' => ['api/Live/playback_detail'],

        'teacher' => ['api/Live/live_of_teacher'],

        'checkPassword'=>['api/Live/checkPassword', ['method' => 'post']],

        // v1.4 pc
        // 'pc_index' => ['api/Live/pc_index', ['method' => 'post']]
    ]);

    // 课程
    Route::rule('curriculum', 'api/Curriculum/curriculum', 'get|post'); // v1.1
    Route::group('curriculum', function () {
        // 课程体系
        Route::rule('system', 'api/Curriculum/curriculum_system', 'post');
        // 课程体系详情
        Route::rule('system_detail', 'api/Curriculum/curriculum_system_detail', 'post');
        Route::rule('share_system_detail', 'api/Curriculum/share_curriculum_system_detail', 'post');

        // 学习进阶
        Route::rule('lps', 'api/Curriculum/study_progress', 'post');

        // 课程首页详细数据
        Route::rule('detail_data', 'api/Curriculum/curriculum_detail_data', 'post');
        // 课程首页详细数据详情
        Route::rule('detail_data_list', 'api/Curriculum/curriculum_detail_data_list', 'post');

        // 课程视频详情
        Route::rule('video_detail', 'api/Curriculum/curriculum_video_detail', 'post');

        // 相关课程视频
        Route::rule('related_video', 'api/Curriculum/curriculum_related_video', 'post');

        // 免费课程视频
        Route::rule('free_video', 'api/Curriculum/curriculum_free_video', 'post');

        // 课程分类
        Route::rule('class', 'api/Curriculum/curriculum_class', 'post');

        // 课程评价提交 TODO 已升级到v1.1
        Route::rule('save_comment', 'api/Curriculum/save_comment', 'post');
        // 课程评价列表 TODO 已升级到v1.1
        Route::rule('comment_list', 'api/Curriculum/comment_list', 'post');
        // 课程晒单
        Route::rule('post_bill', 'api/Curriculum/post_bill', 'post');

        // 课程解锁
        Route::rule('unlock', 'api/Curriculum/curriculum_unlock', 'post');
        // 视频学习状态更新
        Route::rule('update_study', 'api/Curriculum/update_study_status', 'post');

        // 课程模拟购买
        Route::rule('buy', 'api/Curriculum/curriculum_buy', 'post');

        // 课程筛选条件 v1.1
        Route::rule('filter', 'api/Curriculum/filter_field', 'post');

        // 推荐课程 v1.1
        Route::rule('recommend', 'api/Curriculum/curriculum_recommend', 'post');

        // 最新课程 v1.1
        Route::rule('new', 'api/Curriculum/curriculum_new', 'post');

        // 积分商城课程
        Route::rule('credit_curriculum', 'api/Curriculum/credit_curriculum', 'post');

        // 课程详情 v1.1
        Route::rule('detail', 'api/Curriculum/curriculum_detail', 'get|post');

        // 课程视频 v1.1
        Route::rule('video', 'api/Curriculum/curriculum_video', 'post');
        // 课程视频播放 v1.1
        Route::group('video', [
            'player' => ['api/Curriculum/video_player', ['method'=>'post']],
        ]);

        // 老师课程 v1.1
        Route::rule('teacher', 'api/Curriculum/curriculum_teacher', 'get|post');

        // 网易加密视频
        Route::rule('video_play_src','api/Curriculum/video_play_src');

        // vip课程
        Route::rule('vip', 'api/Curriculum/vip_course', 'get|post'); // v1.4.1

        // iosVIP课程列表
        Route::rule('vlists', 'api/Curriculum/iosVIPList', 'get|post'); // v1.4.1
    });

    // 测试
    Route::group('exam', function () {
        // 获取课程视频测试题目
        Route::rule('questions', 'api/Exam/video_exam', 'post');

        // 提交测试答案
        Route::rule('submit', 'api/Exam/action_answer', 'post');
    });

    // 老师
    Route::group('teacher', function () {
        // 大咖课程视频
        Route::rule('curriculum_video', 'api/Teachers/teacher_video', 'post');
        // 大咖课程
        Route::rule('curriculum', 'api/Teachers/teacher_curriculum', 'post');
        Route::rule('curriculum_web', 'api/Teachers/teacherCurriculumWeb', 'post');
    });

    // 商品购买
    Route::group('buy', [
        // 支付提交
        'pay' => ['api/Buy/pay', ['method'=>'post']],
    ]);

    // 短信相关
    Route::group('sms', [
        // 发送短信验证码
        'send_code' => ['api/Sms/send_code', []], // 临时取消请求方式限制解决前端接口请求出错的问题

    ]);

    // 阿里OSS相关
    Route::group('oss', [
        // 获取客户端上传文件需要用到的policy与签名
        'get_policy' => ['api/AliOSS/get_policy', ['method'=>'post']],
        'get_token' => ['api/AliOSS/get_token', ['method'=>'post']],
        // base64编码图像上传
        'base64_upload' => ['api/AliOSS/base64ImgUpload', ['method'=>'post']],
    ]);

    // web版新增接口 v1.1
    Route::group('web', function () {
        // 首页 todo v1.4.1废弃
        /*Route::group('index', [
            // 直播列表
            'live' => ['api/App/web_index_live', ['method'=>'post']],

            // 热门资讯
            'news' => ['api/App/web_index_news', ['method'=>'post']],
        ]);*/


        // 课程
        Route::rule('curriculum', 'api/Curriculum/curriculumWeb', 'get|post');
        Route::group('curriculum', [
            // 学员晒单
            'post_bill' => ['api/Curriculum/web_curriculum_post_bill', ['method'=>'post']],
            // 学员评价
            'comment' => ['api/Curriculum/web_curriculum_comment', ['method'=>'post']],
            // 课程播放
            'player' => ['api/Curriculum/web_curriculum_player', ['method'=>'post']],

            // 播放统计与学习记录数据处理
            'count' => ['api/Curriculum/web_curriculum_player_count', ['method'=>'post']],
        ]);

        // 直播首页
        Route::rule('live', 'api/Live/web_live', 'post');
        Route::group('live', [
            // 直播节目播放请求
            'player' => ['api/Live/web_live_player', ['method'=>'post']],
        ]);

        // 资讯接口
        Route::group('news', [
            // 资讯列表
            'list' => ['api/News/news_list', ['method'=>'post']],
        ]);

        // 商品购买接口
        Route::group('buy', [
            // 支付
            'pay' => ['api/WebBuy/pay', ['method'=>'get']],
        ]);
    });

    // 其他公用独立接口 v1.1
    Route::group('other', [
        // 新股日历
        'new_stock' => ['api/Stock/new_stock', ['method'=>'post']],

        // 当前周周历
        'week_calendar' => ['api/App/week_calendar', ['method'=>'post']],
    ]);

    // 名师动态
    Route::rule('dynamic', 'api/App/dynamic', 'post');

    // 专栏 v1.3
    Route::group('special', [
        'column' => ['api/Special/column', ['method'=>'post']],
        'detail' => ['api/Special/detail', ['method'=>'post']],
        'article/list' => ['api/Special/article_list', ['method'=>'post']],
        'article/detail' => ['api/Special/article_detail', ['method'=>'post']],
        'article/message/commit' => ['api/Special/article_message_commit', ['method'=>'post']],
        'article/message/list' => ['api/Special/article_message_list', ['method'=>'post']],
    ]);

    // 自选股
    Route::group('self', [
        // 发送短信验证码
        'stocks' => ['api/SelfSelectStock/index', ['method'=>'get|post']], // 自选股列表
        'add' => ['api/SelfSelectStock/add', ['method'=>'post']],
        'delete' => ['api/SelfSelectStock/delete', ['method'=>'post']],
        'set_top' => ['api/SelfSelectStock/setTop', ['method'=>'post']],
    ]);

    //图文直播
    Route::group('topics/essay', [
        'list' => ['api/TopicsEssay/list', ['method'=>'get|post']],
    ]);
});


// 支付相关
Route::group('pay', [
    // 异步通知
    'notify' => ['callback/WebPay/wx_notify', ['method'=>'get']],
]);








return [
    '__pattern__' => [
        'name' => '\w+',
    ],
    '[hello]'     => [
        ':id'   => ['index/hello', ['method' => 'get'], ['id' => '\d+']],
        ':name' => ['index/hello', ['method' => 'post']],
    ],
];