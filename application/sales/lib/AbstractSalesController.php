<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/12/27
 * Time: 13:24
 */

namespace app\sales\lib;


use app\common\model\ApiResponse;
use think\Validate;

abstract class AbstractSalesController
{
    /**
     * 处理post表单验证，
     * @param $validateRules
     * @return mixed
     */
    protected function getAndCheckForm($validateRules = []) {
        $form = input('param.');
        if (count($validateRules) == 0) return $form;

        $validate = new Validate($validateRules);
        if(!$validate->check($form)) {
            ApiResponse::error(ApiResponse::ERR_FORM_INVALID, $validate->getError());
        }
        return $form;
    }

}