<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/12/27
 * Time: 11:50
 */

namespace app\sales\behaviour;


use app\common\model\ApiResponse;
use app\sales\lib\ApiAuth;
use think\Request;

class SalesSystemApi
{

    public function run(Request $request) {
        // 检查签名
        $headers['timestamp'] = $request->header('timestamp');
        $headers['nonce'] = $request->header('nonce');
        $headers['sign'] = $request->header('sign');
        $data = $request->param();
        $re = ApiAuth::getInstance()->checkSign($data, $headers);
        if (!$re) ApiResponse::error(ApiResponse::ERR_INVALID_REQUEST);
    }

}