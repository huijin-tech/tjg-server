<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/8/14
 * Time: 17:48
 */

namespace app\admin\controller;


use app\admin\model\ApiResponse;
use app\admin\lib\AdminController;
use app\common\utils\OssUtil;

class Alioss extends AdminController
{

    /**
     * gmt_iso8601
     * 生成ISO8601标准格式的时间
     *
     * @param $time
     * @return string
     */
    private function gmt_iso8601($time) {
        $dtStr = date("c", $time); //格式为2016-12-27T09:10:11+08:00
        $mydatetime = new \DateTime($dtStr);
        $expiration = $mydatetime->format(\DateTime::ISO8601); //格式为2016-12-27T09:12:32+0800
        $pos = strpos($expiration, '+');
        $expiration = substr($expiration, 0, $pos);//格式为2016-12-27T09:12:32
        return $expiration."Z";
    }

    /**
     * get_policy
     * 获取客户端真传文件的policy和签名
     * @api http://xxx/api/oss/get_policy
     *      dir 上传目录
     *      maxsize 最大允许文件大小，单位：MB
     *      expiretime 过期时间，单位：s（秒）
     *
     * @return mixed
     */
    public function get_policy() {
        // 获取并验证表单
        $form = $this->getAndCheckForm([
            ['type', 'require', '上传类型不能为空'],
        ]);

        $type = $form['type'];
        if (!in_array($type, ['image','video','audio','apk','document'])) {
            return json(array('code'=>1010, 'msg'=>'类型不正确', 'data'=>null));
        }
        $dir = $type; // 上传目录

        // 最大允许文件大小
        $maxSize = intval($form['maxsize'] ?? 500*1024*1024);

        $bucket = config('aliYunOSS.Bucket');
        // 计算过期时间
        $end = time() + 30;
        // 生成ISO8601格式时间
        $expiration = $this->gmt_iso8601($end);

        $conditions = [];
        $conditions[] = [0=>'content-length-range', 1=>0, 2=>$maxSize]; // 最大文件大小.用户可以自己设置 100M

        $start = [0=>'starts-with', 1=>'$key', 2=>$dir]; //表示用户上传的数据,必须是以$dir开始, 不然上传会失败,这一步不是必须项,只是为了安全起见,防止用户通过policy上传到别人的目录
        $conditions[] = $start;

        $arr = ['expiration'=>$expiration,'conditions'=>$conditions];
        $policy = json_encode($arr);
        $base64_policy = base64_encode($policy);
        $string_to_sign = $base64_policy;
        $signature = base64_encode(hash_hmac('sha1', $string_to_sign, config('aliYunOSS.AccessKeySecret'), true));

        $response = [];
        $response['accessid'] = config('aliYunOSS.AccessKeyId');
        $response['host'] = 'http://'.$bucket.'.'.config('aliYunOSS.EndPoint');
        $response['bucket'] = $bucket;
        $response['policy'] = $base64_policy;
        $response['signature'] = $signature;
        $response['expire'] = $end;
        $response['dir'] = $dir;  //这个参数是设置用户上传指定的前缀
        $response['dns'] = OssUtil::DNS_HOST;

        return ApiResponse::success($response);
    }


}