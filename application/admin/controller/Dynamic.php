<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2018/1/31
 * Time: 15:04
 */

namespace app\admin\controller;


use app\admin\lib\AdminController;
use app\admin\model\ApiResponse;
use app\api\model\DynamicFlow;
use think\db\Query;
use think\Request;

class Dynamic extends AdminController
{
    public function index(Request $request) {
        if ($request->isGet()) {
            return $this->list();
        } elseif ($request->isDelete()) {
            return $this->del($request);
        } else {
            return json([], 404);
        }
    }

    private function list() {

        $list = DynamicFlow::manageList($this->rows, $this->page);

        return ApiResponse::success($list);
    }

    private function del(Request $request) {
        $id = $request->param('id');
        $re = DynamicFlow::del($id);
        return ApiResponse::success($re);
    }

}