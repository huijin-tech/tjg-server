<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2018/8/16
 * Time: 9:47
 */

namespace app\admin\controller;

use app\admin\lib\AdminController;
use app\admin\model\ApiResponse;
use app\admin\model\CourseEnclosure;

class Enclosure extends AdminController
{
    public function list(){
        $form = $this->getAndCheckForm([
            ['page', 'integer',],
            ['rows', 'integer',],
            ['date', 'date',],
        ]);
        $form['page']=$form['page']??1;
        $form['rows']=$form['rows']??10;

        $condition=[];
        if(isset($form['curriculum_id']) && $form['curriculum_id']>0){
            $condition['ce.curriculum_id']=$form['curriculum_id'];
        }
        if(isset($form['course_service_id']) && $form['course_service_id']>0){
            $condition['ce.course_service_id']=$form['course_service_id'];
        }
        if(isset($form['title']) && $form['title']){
            $condition['ce.title']=['like','%'.$form['title'].'%'];
        }
        if(isset($form['select_time']) && count($form['select_time'])==2 && $form['select_time'][0]){
            $condition['ce.create_time']=['between',[strtotime($form['select_time'][0]),strtotime($form['select_time'][1])]];
        }
        $res=CourseEnclosure::getPage($form['rows'],$condition);
        return ApiResponse::success($res);
    }
    public function save(){
        $form = $this->getAndCheckForm([
            ['type', 'require',],
            ['course_service_id', 'require',],
            ['title', 'require',],
            ['curriculum_id', 'require',],
            ['enclosure', 'require',],
            ['size', 'require',],
        ]);
        if(isset($form['id']) && $form['id']>0){
            if(!CourseEnclosure::findById($form['id'])){
                return ApiResponse::error('1001','该附件不存在');
            }
            $data=[
                'type'=>$form['type'],
                'title'=>$form['title'],
                'curriculum_id'=>intval($form['curriculum_id']),
                'enclosure'=>$form['enclosure'],
                'size'=>$form['size'],
            ];
            CourseEnclosure::modify($data,['id'=>$form['id']]);
        }else{
            $data=[];
            foreach ($form['course_service_id'] as $item){
                $data[]=[
                    'type'=>$form['type'],
                    'course_service_id'=>intval($item),
                    'title'=>$form['title'],
                    'curriculum_id'=>intval($form['curriculum_id']),
                    'enclosure'=>$form['enclosure'],
                    'create_time'=>time(),
                    'size'=>$form['size'],
                ];
            }
            CourseEnclosure::insertAllData($data);
        }
        return ApiResponse::success();
    }

    public function delete(){
        $form = $this->getAndCheckForm([
            ['id', 'require',],
        ]);
        if(!CourseEnclosure::findById($form['id'])){
            return ApiResponse::error('1001','该附件不存在');
        }
        CourseEnclosure::destroy($form['id']);
        return ApiResponse::success();
    }

}