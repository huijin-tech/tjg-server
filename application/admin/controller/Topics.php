<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/10/25
 * Time: 11:05
 */

namespace app\admin\controller;


use app\admin\model\ApiResponse;
use app\admin\lib\AdminController;
use app\common\model\DynamicFlow;
use app\common\model\Topic;
use app\common\model\TopicChat;
use app\common\model\TopicEssay;
use app\common\utils\DateTimeUtil;
use think\Db;

class Topics extends AdminController
{
    /**
     * 话题列表
     * @return mixed
     */
    public function topic_list() {
        $form = $this->getAndCheckForm([
            ['teacher_id', 'require|integer',],
            ['rows', 'integer',],
            ['page', 'integer',],
        ]);

        $today = date('Y-m-d');
        $query = Db::table('topic a');
        $query->where('a.teacher_id','=', $form['teacher_id']);
        $query->order('a.id','desc');
        $query->field("a.*, a.play_date='$today' is_current");
        $result = $query->paginate($form['rows']??10,false,['page'=>$form['page']])
            ->toArray();

        $result['code'] = 200;
        return $result;
    }

    /**
     * 话题添加或修改
     * @return ApiResponse
     */
    public function topic_save() {
        $rules = [
            ['title', 'require',],
            ['teacher_id', 'require|integer',],
        ];
        $form = $this->getAndCheckForm($rules);

        Db::transaction(function () use ($form) {
            $today = date('Y-m-d');
            $topic = Topic::get(['teacher_id'=>$form['teacher_id'],'play_date'=>$today]);
            $isNew = false;
            if (empty($topic)) {
                $topic = new Topic();
                $topic->teacher_id = intval($form['teacher_id']);
                $topic->play_date = $today;
                $isNew = true;
            }
            $topic->title = $form['title'];
            $topic->save();

            if ($isNew) $topic->makeDynamicData($isNew, DynamicFlow::SOURCE_ADMIN);

        });

        return ApiResponse::success();
    }

    /**
     * 动态图文列表
     *
     * @return mixed
     */
    public function essay_list() {
        $form = $this->getAndCheckForm([
            ['topic_id', 'require|integer',],
            ['rows', 'integer',],
            ['page', 'integer',],
        ]);

        $query = Db::table('topic_essay e');
        $query->where('e.topic_id','=', $form['topic_id']);
        $query->order('e.id','desc');
        $page = $query->paginate($form['rows']??10,false,['page'=>$form['page']??1]);
        $result = $page->toArray();
        foreach ($result['data'] as & $item) {
            $item['created'] = DateTimeUtil::format($item['created']);
        }

        $result['code'] = 200;
        return $result;
    }

    /**
     * 动态图文保存
     * @return ApiResponse
     */
    public function essay_save() {
        $form = $this->getAndCheckForm([
            ['topic_id','require|integer',],
            ['title','require',],
            ['content','require',],
        ]);

        Db::transaction(function () use ($form) {
            if (!empty($form['id'])) {
                $model = TopicEssay::update($form,[], TopicEssay::getSaveFields());
                $isNew = false;
            } else {
                $model = TopicEssay::create($form, TopicEssay::getSaveFields());
                $isNew = true;
            }

//            $model->makeDynamicData($isNew, DynamicFlow::SOURCE_ADMIN);
        });

        return ApiResponse::success();
    }

    /**
     * 互动列表
     * @return mixed
     */
    public function chat_list() {
        $form = $this->getAndCheckForm([
            ['topic_id', 'require|integer',],
            ['rows', 'integer',],
            ['page', 'integer',],
        ]);

        $result = TopicChat::list($form['topic_id'],$form['rows'],$form['page'],true);

        $result['code'] = 200;
        return $result;
    }

    /**
     * 发表互动
     *
     * @return ApiResponse
     */
    public function chat_create() {
        $form = $this->getAndCheckForm([
            ['content', 'require',],
            ['topic_id', 'require|integer',],
        ]);

        $data = [
            'content' => $form['content'],
            'topic_id' => $form['topic_id'],
        ];
        if (isset($form['id'])) {
            $data['id'] = $form['id'];
            TopicChat::update($data);
        } else {
            TopicChat::create($data);
        }

        return ApiResponse::success();
    }

    /**
     * 回复互动
     * @return ApiResponse
     */
    public function chat_reply() {
        $form = $this->getAndCheckForm([
            ['reply_id','require|integer',],
            ['content','require',],
        ]);

        $re_chat = TopicChat::get($form['reply_id']);
        if (empty($re_chat)) ApiResponse::error(ApiResponse::ERR_OPERATE_FAILED,'回复目标不存在');

        $data = [
            'content' => $form['content'],
            'reply_target_id' => $re_chat->id,
            'topic_id' => $re_chat->topic_id,
        ];
        TopicChat::create($data);

        return ApiResponse::success();
    }


}