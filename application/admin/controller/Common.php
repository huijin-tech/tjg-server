<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2018/3/1
 * Time: 16:33
 */

namespace app\admin\controller;


use app\admin\lib\AdminController;
use app\admin\model\ApiResponse;
use app\admin\model\CourseService;
use app\admin\model\live\LiveChannel;
use app\api\model\curriculum\Curriculum;
use app\common\controller\Files;
use app\common\model\app\ConstLinkType;
use app\common\model\SpecialColumn;
use app\common\model\teacher\Teacher;

class Common extends AdminController
{
    public function link_types() {
        return ApiResponse::success((object)ConstLinkType::getNames());
    }

    public function column_names() {
        $form = $this->getAndCheckForm([
            ['teacher_id', 'integer',],
        ]);
        $re = SpecialColumn::simpleList($form['teacher_id']??0, $form['keywords']??null);
        return ApiResponse::success($re);
    }

    public function teacher_names() {
        $list = Teacher::listNames();
        return ApiResponse::success($list);
    }

    public function curriculum_names() {
        $list = Curriculum::names();
        return ApiResponse::success($list);
    }

    public function channel_names() {
        $form = $this->getAndCheckForm([
            ['teacher_id', 'integer',],
        ]);
        $list = LiveChannel::listNames($form['teacher_id']??-1);
        return ApiResponse::success($list);
    }

    public function live_titles($keywords = null) {

    }

    /**
     * img_upload
     * 图片上传
     *
     * @author zhengkai
     * @date 2018-05-07
     *
     * @param string $way 上传方式：editor=编辑器，input=表单
     * @return ApiResponse|\think\response\Json
     */
    public function img_upload($way)
    {
        /*$form = request()->file();
        print_r($form);
        exit;*/
        $result = Files::upload('img', 'jpg,jpeg,png,gif', 10240, 'upload');

        switch ($way) {
            case 'editor':
                if ($result['code']) {
                    $info = $result['data'];
                    $backcall = [
                        'fileName' => $info['fileName'],
                        'uploaded' => 1,
                        'url' => $info['url'],
                    ];
                } else {
                    $backcall = [
                        'error' => [
                            'message' => $result['msg']
                        ],
                    ];
                }
                return json($backcall);
                break;
            case 'input':
                if ($result['code']) {
                    $info = $result['data'];

                    return ApiResponse::success([
                        'url' => $info['url']
                    ]);
                } else {
                    return ApiResponse::error(ApiResponse::ERR_OPERATE_FAILED, $result['msg']);
                }
                break;
        }
    }


    public function course_service(){
        $condition['service_id']=['in',array_values(config('course_serivce'))];
        $list=CourseService::getList($condition);
        return ApiResponse::success($list);
    }

    public function enclosure_curriculums() {
        $condition=[
            'article_reference'=>'true',
            'article_report'=>'true',
        ];
        $list = Curriculum::curriculum_list($condition);
        return ApiResponse::success($list);
    }
}