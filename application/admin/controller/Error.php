<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/8/9
 * Time: 12:59
 */

namespace app\admin\controller;


use app\admin\lib\AdminController;
use app\admin\model\ApiResponse;

class Error extends AdminController
{
    public function index() {
        ApiResponse::error(ApiResponse::ERR_INVALID_URI, '访问资源不存在');
    }
}