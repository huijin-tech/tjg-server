<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/9/18
 * Time: 10:59
 */

namespace app\admin\controller;


use app\admin\model\ApiResponse;
use app\admin\lib\AdminController;
use app\common\model\app\AppVersion;
use app\common\utils\OssUtil;
use think\Db;

class App extends AdminController
{
    /**
     * 版本记录列表
     *
     * @return ApiResponse
     */
    public function version_list() {

        $form = $this->getAndCheckForm([
            ['rows','integer',],
            ['page','integer',],
            ['platform','^[0-2]$',],
        ]);

        $query = Db::table('app_version')
            ->order('id','desc');
        $platform = intval($form['platform']??0);
        if ($platform > 0) {
            $query->where('platform','=', $platform);
        }
        $re = $query->paginate($form['rows'] ?? 10,false, [
                'page' => $form['page']??1
        ])->toArray();
        $re['code'] = 200;

        return $re;
    }

    /**
     * 保存版本记录
     *
     * @return ApiResponse
     */
    public function version_save() {

        $form = $this->getAndCheckForm([
            ['platform','require|^[1-2]$','平台必填|取值超范围'],
            ['code','require','版本号不能为空'],
            ['version','require','版本名称不能为空'],
            ['apk_url','url','apk下载地址不正确'],
        ]);

        $data = [
            'platform' => $form['platform'],
            'code' => $form['code'],
            'version' => $form['version'],
            'important' => $form['important'] ?? 1,
            'apk_url' => $form['apk_url'] ?? '',
            'intro' => $form['intro'] ?? '',
        ];

        $id = intval($form['id'] ?? 0);
        $ver = AppVersion::get($id);
        if ($ver) {

            if ($ver->code != $data['code'] && AppVersion::get(['code' => $data['code'],'platform'=>$data['platform']]) )
                return ApiResponse::error(ApiResponse::ERR_OPERATE_FAILED,'版本号不能重复');
            $ver->code = $data['code'];

            if ( $ver->version != $data['version'] && AppVersion::get(['version' => $data['version'],'platform'=>$data['platform']]) )
                return ApiResponse::error(ApiResponse::ERR_OPERATE_FAILED,'版本名称不能重复');
            $ver->version = $data['version'];

            if ($ver->apk_url && $ver->apk_url!=$data['apk_url'])
                OssUtil::delete($ver->apk_url);

            $data['id'] = $ver->id;
            AppVersion::update($data);

        } else {

            if ( AppVersion::get(['code' => $data['code'],'platform'=>$data['platform']]) )
                return ApiResponse::error(ApiResponse::ERR_OPERATE_FAILED,'版本号不能重复');

            if ( AppVersion::get(['version' => $data['version'],'platform'=>$data['platform']]) )
                return ApiResponse::error(ApiResponse::ERR_OPERATE_FAILED,'版本名称不能重复');


            AppVersion::create($data);

        }

        return ApiResponse::success();
    }

    /**
     * 删除版本记录
     *
     * @return ApiResponse
     */
    public function version_del() {

        $form = $this->getAndCheckForm([['id','require|integer',],]);
        $id = intval($form['id']);

        $ver = AppVersion::get($id);
        OssUtil::delete($ver->apk_url);
        AppVersion::destroy($id);

        return ApiResponse::success();
    }


}