<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2018-04-19
 * @Time: 09:05
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： Position.php
 */
namespace app\admin\controller;

use app\admin\lib\AdminController;
use app\admin\model\ApiResponse;
use app\common\model\PositionData;
use think\Db;
use think\Log;

class Position extends AdminController {
    /**
     * lists
     * 推荐位内容列表
     *
     * @return ApiResponse
     * @throws \think\exception\DbException
     */
    public function lists()
    {

        $form = $this->getAndCheckForm([
            ['page', 'integer',],
            ['limit', 'integer',],
        ]);

        if (!isset($form['page'])) $form['page'] = 0;
        if (!isset($form['limit'])) $form['limit'] = 10;

        $data = PositionData::getAll($form['page'], $form['limit']);

        foreach ($data as $key=>&$val) {
            $val['positionsStr'] = PositionData::$position[$val['positions']];
            $val['typesStr'] = PositionData::$positionType[$val['types']];
            $content = json_decode($val['content'], true);
            $val['itemTypeStr'] = PositionData::$contentType[$content['itemType']];
            $val['itemTitle'] = $content['itemTitle'];
            $val['itemImg'] = $content['itemImg'];
            $val['itemUrl'] = $content['itemUrl'];
        }

        return ApiResponse::success($data);
    }

    /**
     * 推荐位置
     *
     * @return ApiResponse
     */
    public function positions()
    {
        return ApiResponse::success(PositionData::$position);
    }

    /**
     * save
     * 保存推荐位内容数据
     *
     * @return ApiResponse
     * @throws \think\exception\DbException
     */
    public function save()
    {
        $form = $this->getAndCheckForm([
            ['itemTitle', 'require', '标题不能为空'],
            ['types', 'require', '内容类型不能为空'],
            ['positions', 'require', '推荐位不能为空'],
        ]);

        if (isset($form['id'])) { // 更新
            $getData = PositionData::get($form['id']);
            $content = json_decode($getData['content'], true);
            $content = [
                'itemId' => $content['itemId'],
                'itemImg' => $form['itemImg'],
                'itemUrl' => $form['itemUrl'],
                'itemType' => $content['itemType'],
                'itemTitle' => $form['itemTitle'],
            ];
            $data = [
                'positions' => $form['positions'],
                'types' => $form['types'],
                'content' => json_encode($content),
            ];

            try {
                PositionData::where('id', $form['id'])->update($data);
                return ApiResponse::success();
            } catch (\Exception $e){
                Log::error($e->getMessage());
                return ApiResponse::error(ApiResponse::ERR_OPERATE_FAILED, $e->getMessage());
            }
        } else { // 新增
            $content = [
                'itemId' => isset($form['itemId'])?$form['itemId']:0, // 内容id
                'itemImg' => $form['itemImg'], // 图片
                'itemUrl' => $form['itemUrl'], // 链接
                'itemType' => isset($form['itemType'])?$form['itemType']:PositionData::CONTENT_TYPE_AD, // 内容类型
                'itemTitle' => $form['itemTitle'], // 标题
            ];
            $data = [
                'positions' => $form['positions'],
                'types' => $form['types'],
                'content' => json_encode($content),
            ];
            try {
                PositionData::create($data);
                return ApiResponse::success();
            } catch (\Exception $e){
                Log::error($e->getMessage());
                return ApiResponse::error(ApiResponse::ERR_OPERATE_FAILED, $e->getMessage());
            }
        }
    }

    /**
     * del
     * 删除推荐内容
     *
     * @return ApiResponse
     */
    public function del()
    {
        $form = $this->getAndCheckForm([
           ['id', 'require|integer', 'id不能为空|参数类型不正确'],
        ]);

        Db::startTrans();
        try {
            PositionData::destroy($form['id']);
            Db::commit();
            return ApiResponse::success();
        } catch (\Exception $e) {
            Db::rollback();
            Log::error($e->getMessage());
            return ApiResponse::error(ApiResponse::ERR_OPERATE_FAILED, '操作失败');
        }
    }
}