<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/11/15
 * Time: 14:01
 */

namespace app\admin\lib;


use app\admin\model\ApiResponse;
use think\Request;
use think\Validate;

/**
 * Class AdminController
 * @package app\admin\lib
 *
 * @property mixed id
 * @property integer limit
 * @property integer offset
 * @property integer page
 * @property integer rows
 */
class AdminController
{

    /**
     * 处理post表单验证，
     * @param $validateRules
     * @param bool $get
     * @return mixed
     */
    protected function getAndCheckForm($validateRules = [], $get = false) {
        $form = input('param.');
        if (count($validateRules) == 0) return $form;

        $validate = new Validate($validateRules);
        if(!$validate->check($form)) {
            ApiResponse::error(ApiResponse::ERR_FORM_INVALID, $validate->getError());
        }
        return $form;
    }

    protected function parsePageParams() {
        $request = Request::instance();
        $this->limit = (int) $request->param('limit',10);
        $this->offset = (int) $request->param('offset',0);
        $this->page = (int) $request->param('page');
        $this->rows = (int) $request->param('rows');
        if ($this->page && $this->rows) {
            $this->limit = $this->rows;
            $this->offset = intval(($this->page - 1) * $this->rows);
        } else {
            $this->rows = $this->limit;
            $this->page = intval($this->offset / $this->limit) + 1;
        }
    }

    public function __construct()
    {
        $this->parsePageParams();
    }

    public function _empty() {
        ApiResponse::error(ApiResponse::ERR_INVALID_URI, '访问资源不存在');
    }

}