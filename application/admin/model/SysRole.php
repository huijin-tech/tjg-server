<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/10/18
 * Time: 15:29
 */

namespace app\admin\model;


use think\Db;
use think\Model;

class SysRole extends Model
{
    protected $table = 'sys_role';
    protected $pk = 'id';

    /**
     * @return mixed
     */
    public static function getAll() {
        $sql = <<<SQL
SELECT * FROM sys_role WHERE id>1
SQL;
        $list = Db::query($sql);
        return $list;
    }
}