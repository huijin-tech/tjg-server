<?php

namespace app\admin\model;

use think\Model;


class CourseService extends Model
{
    protected $table = 'course_service';
    protected $pk = 'service_id';
    protected  $field = [
        'service_id',
        'service_name',
        'sort'
    ];

    public static function getList($condition=[]){
        return self::field('service_id,service_name')->where($condition)->order('sort')->select();
    }



}