<?php

namespace app\admin\model;

use think\Model;


class CourseEnclosure extends Model
{
    protected $table = 'course_enclosure';
    protected $pk = 'id';


    public static function getPage($rows,$condition=[]){
        return self::alias('ce')
            ->join('curriculum c','c.curriculum_id=ce.curriculum_id','left')
            ->join('course_service cs','cs.service_id=ce.course_service_id','left')
            ->field('ce.*,cs.service_name,c.curriculum_name')
            ->where($condition)
            ->order('ce.create_time','desc')
            ->paginate($rows)
            ->toArray();
    }

    public static function insertAllData($data){
        return self::insertAll($data);
    }

    public static function modify($data,$condition){
        return self::update($data,$condition);
    }

    public static function findById($id){
        return self::where('id',$id)->find();
    }

    public static function getApiPage($rows,$condition=[]){
        return self::field('id,create_time,title,enclosure,type,size')
            ->where($condition)
            ->order('create_time','desc')
            ->paginate($rows)
            ->toArray();
    }



}