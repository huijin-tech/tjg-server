<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/8/4
 * Time: 13:07
 */

namespace app\admin\model;


class PageResponse
{
    public $code = 200;
    public $list;
    public $total;
    public $msg;
    
    public static function success($list, $total) {
        $page = new static();
        $page->list = $list;
        $page->total = $total;
        return $page;
    }
    
    public static function error($code, $msg) {
        $page = new static();
        $page->code = $code;
        $page->msg = $msg;
        return $page;
    }
    
}