<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2017-12-29
 * @Time: 11:22
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： SmsTask.php
 */
namespace app\admin\command;

use library\ServerAPI;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\Output;
use think\Db;
use think\Log;

/**
 * @deprecated
 *
 * Class SmsTask
 * @package app\admin\command
 *
 * 短信队列任务
 * php think sms [c/s] 创建/发送
 */
class SmsTask extends Command {
    protected function configure()
    {
        // 参数：c=创建短信任务 s=发送短信
        $this->addArgument('type', Argument::REQUIRED);

        $this->setName('sms')->setDescription('短信任务');
    }

    protected function execute(Input $input, Output $output)
    {
        // 接收参数
        $args = $input->getArguments();

        switch ($args['type']) {
            case 'c':
                $this->create();
                break;
            case 's':
                $this->send();
                break;
        }
    }

    // 生成短信发送列表
    private function create()
    {
        $customer = Db::table('customer_list')->select();

        // echo $customer;

        // $smsContent = '年终大血拼，小乾有话说：登录就送888课程，江氏交易天机精品课程3折起，万元折扣等你来抢，名额仅剩99个，戳 http://t.cn/RHMU97F 立即领取。回复TD退订';
        $smsContent = '掌乾财经联合华西证券共同举办《股市深度学习分享策略会》- 详解2018中美贸易摩擦对股市的影响。同时掌乾八面交易哲学之《龙头股量化战法》首次公开分享，指导中小投资者践行专业投资。本周日（5.13）下午1：30-5：30在高升桥路9号罗浮广场812准时开讲。加V：zqcj2017了解详情，在线报名：http://sina.lt/f5n3(回复TD退订)';
        foreach ($customer as $key=>$val) {
            Db::startTrans();
            try {
                $data = [
                    'sms_api' => 1,
                    'sms_content' => $smsContent,
                    'sms_mobile' => $val['customer_mobile'],
                    'sms_addtime' => time(),
                    // 'sms_execute_time' => '1'
                ];
                $isCheck = Db::table('sms_list')
                    ->where('sms_content', $smsContent)
                    ->where('sms_mobile', $val['customer_mobile'])
                    ->where('sms_send_status', 0)
                    ->count('sms_id');
                if ($isCheck) {
                    echo "客户：{$val['customer_mobile']} 短信任务已存在，不再重复创建。".PHP_EOL;
                } else {
                    Db::table('sms_list')->insert($data);

                    echo $key."客户：{$val['customer_mobile']} 短信任务创建成功。".PHP_EOL;
                }

                Db::commit();
            } catch (\Exception $e) {
                Log::error('短信队列创建失败：'.$e->getMessage());

                echo "短信队列创建失败！".PHP_EOL;

                Db::rollback();
            }
        }
    }

    // 发送短信
    private function send()
    {
        $v163sms = ServerAPI::getInstance();

        $smsList = Db::table('sms_list')
            ->where('sms_send_status', ['=', 0], ['=',-1], 'or')
            ->select();

        if ($smsList) {
            foreach ($smsList as $key=>$val) {
                // $result = $v163sms->sendMessage(4102093, [$val['sms_mobile']]);
                $result = $v163sms->sendMessage(4092857, [$val['sms_mobile']], [
                    '1:30-5:30',
                    'zqcj2017',
                    'http://sina.lt/f5n3'
                ]);
                Db::table('sms_list')->where('sms_id', $val['sms_id'])->update([
                    'sms_send_status' => $result['code']===200?1:-1,
                    'sms_send_time' => time(),
                    'sms_send_other' => $result['obj']??null,
                ]);

                if ($result['code']===200) {
                    echo $key."短信已向 {$val['sms_mobile']} 发送".PHP_EOL;
                } else {
                    echo $key."短信向 {$val['sms_mobile']} 发送失败".PHP_EOL;
                }

                if ($key>58) {
                    sleep(5);
                    $this->send();
                }
            }
        } else {
            echo "当前没有可发送队列数据".PHP_EOL;
        }

    }
}