<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2017-10-09
 * @Time: 10:46
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： LiveMakeStatus.php
 */
namespace app\admin\command;

use think\console\Command;
use think\console\Input;
use think\console\Output;
use think\Db;

/**
 * 定时任务
 *
 * @author zhengkai
 *
 * Class LiveMakeStatus
 * @package app\admin\command
 */
class LiveMakeStatus extends Command {
    /**
     * 直播节目预约状态更新
     * 命令：php think LiveMakeStatus
     */
    protected function configure()
    {
        $this->setName('LiveMakeStatus')->setDescription('直播节目预约状态更新');
    }

    public function execute(Input $input, Output $output)
    {
        Db::table('user_live_subscribe')
            ->alias('uls')
            ->join('live l', 'uls.live_id=l.id', 'LEFT')
            ->field('
                uls.id as subscrib_id, 
                uls.subscribe_time,
                l.id as live_id,
                l.title as live_title, 
                l.status as live_status,
                l.live_date,
                l.start_time as live_start_time
            ')
            // ->find();
            ->chunk(100, function($query){
                foreach ($query as $key=>$val) {
                    if (($val['live_status']>0) && (time()>strtotime($val['live_start_time']))) {
                        Db::table('user_live_subscribe')->where('id', $val['subscrib_id'])->update(['valid'=>'false']);
                    }
                }
                return false;
            }, 'uls.id');
    }
}