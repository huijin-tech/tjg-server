<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/11/22
 * Time: 13:33
 */

namespace app\admin\command;


use app\common\model\NeteaseUser;
use library\v163Class;
use think\console\Command;
use think\console\Input;
use think\console\Output;

class Netease extends Command
{
    protected function configure()
    {
        $this->setName('netease')->setDescription('网易参数配置命令工具');
    }

    protected function execute(Input $input, Output $output)
    {
        $v163 = v163Class::getInstance();

        $re = $v163->set_upload_callback();
        echo 'set upload callback',PHP_EOL;
        print_r($re);
        echo PHP_EOL;

        $re = $v163->set_transcode_callback();
        echo 'set transcode callback',PHP_EOL;
        print_r($re);
        echo PHP_EOL;

        $re = $v163->set_live_record_callback_signKey();
        echo 'set live record callback signKey',PHP_EOL;
        print_r($re);
        echo PHP_EOL;

        $re = $v163->set_live_record_callback_url();
        echo 'set live record callback url',PHP_EOL;
        print_r($re);
        echo PHP_EOL;

        $re = NeteaseUser::setVodUser(NeteaseUser::DEFAULT_USER_ID, NeteaseUser::defaultToken());
        print_r($re);
        echo PHP_EOL;
    }
}