<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2017-08-07
 * @Time: 15:56
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： LiveStatusScan.php
 */
namespace app\admin\command;

use app\admin\model\live\LiveChannel;
use app\api\model\live\Live;
use library\v163Class;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\Output;
use think\Db;

/**
 * 定时任务
 *
 * @author zhengkai
 *
 * Class LiveStatusScan
 * @package app\admin\command
 */
class LiveStatusScan extends Command {
    /**
     * 命令：php think liveStatus [date]
     * 参数：date 可选 指定直播日期，格式0000-00-00
     */
    protected function configure()
    {
        // 直播日期参数
        $this->addArgument('date', Argument::OPTIONAL); // 可选

        $this->setName('liveStatus')->setDescription('直播节目状态检测与更新');
    }

    protected function execute(Input $input, Output $output)
    {
        // 接收参数
        $args = $input->getArguments();

        $v163 = v163Class::getInstance();

        // 直播日期
        $now = time();
        $live_date = date('Y-m-d', $now);
        if ($args['date']) $live_date = $args['date'];
        //$live_date = '2017-08-05';

        $sql = <<<SQL
SELECT l.id,l.title,l.live_date,l.start_time,l.end_time,l.status,l.live_channel_id,c.cid 
FROM live l
LEFT JOIN live_channel c ON l.live_channel_id=c.id
WHERE l.live_date=:live_date 
SQL;
        $live_data = Db::query($sql, ['live_date'=>$live_date]);

        $ch = 'update live_channel set status=:stat where id=:id';
        foreach ($live_data as $val) {
            $live_s_time = strtotime($val['start_time']); // 直播开始时间
            $live_e_time = strtotime($val['end_time']); // 直播结束时间

            $live_s_time_str = date('H:i', $live_s_time);
            $live_e_time_str = date('H:i', $live_e_time);

            if (($now>=$live_s_time && $now<=$live_e_time) && $val['status']===0) { // 开始直播

                Live::setStatus($val['id'], Live::STATUS_START);

                $v163->channel_resetRecord($val['cid']);
                echo "节目《{$val['title']}》正在直播，直播时间{$live_s_time_str}~{$live_s_time_str}"."\r\n";
                $live = Live::get($val['id']);
                $live->makeDynamicData(false,0);
            } else if (($now>=$live_s_time && $now>=$live_e_time) && $val['status']<=1) { // 结束直播

                Live::setStatus($val['id'], Live::STATUS_END);

                $v163->channel_resetRecord($val['cid']);
                echo "节目《{$val['title']}》直播结束，直播时间：$live_date {$live_s_time_str}~{$live_e_time_str}"."\r\n";
            }
        }
    }
}