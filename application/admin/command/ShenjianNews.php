<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2018/7/4
 * Time: 16:38
 */

namespace app\admin\command;

use Shenjian\ShenjianClient;
use think\console\Command;
use think\console\Input;
use think\console\Output;
use think\Env;

class ShenjianNews extends Command
{
    /**
     * 命令：php think shenjianNews
     */
    protected function configure()
    {
        $this->setName('shenjianNews')->setDescription('神箭手采集行业数据');
    }

    protected function execute(Input $input, Output $output)
    {
        $user_key = Env::get('shenjian.APP_KEY');
        $user_secret = Env::get('shenjian.APP_SECRET');
        $app_id = Env::get('shenjian.APP_ID');
        $params['node'] = 1;
        $params['follow_new'] = true;
        $params['follow_change'] = false;
        $params['dup_type'] = 'skip';
        $params['change_type'] = 'update';
        $params['timer_type'] = 'once';
        $params['once_date_start'] = '2018-07-05';
        $params['time_start'] = '10:00';
        try{
            $shenjian_client = new ShenjianClient($user_key, $user_secret);
            $status=$shenjian_client->clearCrawlerSource($app_id);
            $status = $shenjian_client->startCrawler($app_id, $params);
        }catch (ShenjianException $e){
            printf($e->getMessage() . "\n");
            return;
        }
        print("Crawler Status: " . $status);
        echo 11;exit;
    }

}