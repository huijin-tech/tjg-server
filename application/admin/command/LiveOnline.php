<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2018/3/8
 * Time: 18:09
 */

namespace app\admin\command;


use app\admin\model\live\LiveChannel;
use app\api\model\live\Live;
use app\common\lib\rongcloud\RongCloud;
use think\console\Command;
use think\console\Input;
use think\console\Output;
use think\Db;

class LiveOnline extends Command
{
    protected function configure()
    {
        $this->setName('liveOnline')->setDescription('更新在线人数');
    }

    protected function execute(Input $input, Output $output)
    {
        $chatRoom = RongCloud::getInstance()->Chatroom();
        $list = LiveChannel::all();
        $sql = <<<SQL
update live set number_online_peak = (CASE WHEN number_online_peak>:num THEN number_online_peak ELSE :num END )
where live_channel_id=:cid AND status=:stat AND live_date=DATE(now())
SQL;
        foreach ($list as $item) {
            $ret = $chatRoom->queryUser($item->chat_room_id,1,1);
            $number_online = intval($ret['total']??0);
            LiveChannel::update(['number_online' => $number_online], ['id' => $item->id]);
            Db::execute($sql, ['num'=>$number_online, 'cid'=>$item->id, 'stat'=>Live::STATUS_START]);
        }
    }
}