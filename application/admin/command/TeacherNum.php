<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/11/13
 * Time: 11:31
 */

namespace app\admin\command;


use think\console\Command;
use think\console\Input;
use think\console\Output;
use think\Db;

class TeacherNum extends Command
{
    protected function configure()
    {
        $this->setName('teacherNum')->setDescription('定时统计老师的文章数、视频数等');
    }

    protected function execute(Input $input, Output $output)
    {
        $sql = <<<SQL
UPDATE teacher
SET num_essay          = (SELECT count(e.id)
                          FROM teacher_essay e
                          WHERE e.teacher_id = teacher.id),
  num_speaking         = (SELECT count(s.id)
                          FROM teacher_speaking s
                          WHERE s.teacher_id = teacher.id),
  num_curriculum_video = (SELECT count(v.*)
                          FROM curriculum_video v
                          WHERE v.video_teacher = teacher.id);

SQL;
        Db::execute($sql);
    }

}