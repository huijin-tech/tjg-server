<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/10/9
 * Time: 18:10
 */

namespace app\admin\command;


use app\admin\model\live\LiveChannel;
use library\v163Class;
use think\console\Command;
use think\console\Input;
use think\console\Output;

class LiveChannelClean extends Command
{
    protected function configure()
    {
        $this->setName('liveChannelClean')->setDescription('清理无效的直播频道');
    }

    protected function execute(Input $input, Output $output)
    {
        $v163 = v163Class::getInstance();
        $list = LiveChannel::all();
        $chData = $v163->channel_list(100);
        $chs = $chData['ret']['list'];

        foreach ($list as $item) {
            try{
                $ret = $v163->channel_get($item->cid);
                $output->writeln("有效频道：{$item->id} - {$item->cid}");

            }catch (\Exception $e){
                $output->writeln("删除无效频道: {$item->cid}");
                $item->delete();
                continue;
            }
        }


    }
}