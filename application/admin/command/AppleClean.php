<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2018/2/1
 * Time: 17:47
 */

namespace app\admin\command;


use app\common\lib\pay\ApplePay;
use app\common\model\BuyBill;
use think\console\Command;
use think\console\Input;
use think\console\Output;

class AppleClean extends Command
{
    protected function configure()
    {
        $this->setName('appleClean')->setDescription('清除苹果支付的沙盒支付的订单');
    }

    protected function execute(Input $input, Output $output)
    {
        echo '开始',PHP_EOL;
        $list = BuyBill::all(['bill_pay_method'=>4,'bill_have_paid'=>true]);
        foreach ($list as $bill) {
            if (empty($bill->pay_trade_no)) continue;
            $re = ApplePay::getInstance()->checkValidReceipt($bill->pay_trade_no);
            if ($re->success && $re->isSandbox) {
                $bill->is_sandbox = true;
                $bill->save();
            }
        }
        echo '结束',PHP_EOL;
    }
}