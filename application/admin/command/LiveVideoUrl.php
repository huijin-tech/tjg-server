<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/11/13
 * Time: 11:31
 */

namespace app\admin\command;


use app\api\model\live\Live;
use library\v163Class;
use think\console\Command;
use think\console\Input;
use think\console\Output;
use think\Db;
use think\Env;

class LiveVideoUrl extends Command
{
    private $v163;
    private $url_pre;

    protected function configure()
    {
        $this->setName('liveVideoUrl')->setDescription('直播回看地址');
    }

    protected function execute(Input $input, Output $output)
    {
        $this->url_pre=Env::get('v163.videoUrlPre');
        $live=Live::alias('l')
            ->field('l.id,ll.cid,video_url,start_time,end_time')
            ->join('live_channel ll','l.live_channel_id=ll.id','left')
            ->where('l.status',2)
            ->where('created','>','2018-10-30 17:00')
            ->order('l.id','desc')
            ->find();
        if($live){
            $this->v163 = v163Class::getInstance();
            $videos=$this->v163->channel_vodVideoList($live->cid,strtotime($live->start_time).'000',strtotime($live->end_time).'000');
            if(isset($videos[0]['url'])){
                $live->video_url=$this->url_pre.$videos[0]['url'];
                $live->status=3;
                $live->save();
            }
        }
    }

}