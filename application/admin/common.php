<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2017-08-01
 * @Time: 18:05
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： common.php
 */
use Rych\Random\Random;
use app\common\model\Apply;


if (!function_exists('createApp')) {
    /**
     * createApp
     * 应用创建
     *
     * @author zhengkai
     * @date 2017-08-01
     *
     * @param string $appName 应用名称
     * @return false|int
     */
    function createApp($appName)
    {
        $random = new Random();
        $apply = new Apply();

        $data = [
            'app_name' => $appName, // 应用名称
            'app_secret' => md5($random->getRandomString(16)), // 应用api密钥
            'app_addtime' => time() // 应用添加时间
        ];

        return $apply->data($data)->save();
    }
}
