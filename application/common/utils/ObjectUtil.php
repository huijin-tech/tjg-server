<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2018/1/26
 * Time: 12:48
 */

namespace app\common\utils;

final class ObjectUtil
{
    private function __construct(){}

    /**
     * @param string $targetClass
     * @param object|array $sourceObject
     * @return object
     */
    public static function assign($targetClass, $sourceObject) {

        if (is_null($sourceObject)) {
            return null;
        }

        if (!is_object($sourceObject) && !is_array($sourceObject)) {
            throw new \RuntimeException('$sourceObject必须是object或array');
        }

        $object = new $targetClass();
        foreach ($sourceObject as $prop => $value) {
            $object->$prop = $value;
        }
        return $object;
    }

}