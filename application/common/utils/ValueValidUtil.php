<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2018/3/16
 * Time: 18:01
 */

namespace app\common\utils;


class ValueValidUtil
{
    const URL = '/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i';

    public static function url($val) {
        return preg_match(self::URL, $val);
    }
}