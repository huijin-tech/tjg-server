<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2018/3/22
 * Time: 14:12
 */

namespace app\common\lib\aop\sandbox;

/**
 * @property
 * @property string appId
 * @property string gatewayUrl
 * @property string rsaPrivateKeyFilePath
 * @property string alipayrsaPublicKey
 */
final
class AliPaySandbox
{
    private function __construct()
    {
        $this->appId = '2016081600255615';
        $this->gatewayUrl = 'https://openapi.alipaydev.com/gateway.do';
        $this->rsaPrivateKeyFilePath = __DIR__.'/rsa_private_key.pem';
        $this->alipayrsaPublicKey = 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAx+8ek9G0UVERY3SF1blCxKv4VoLrZbYC5cgN7QHPW29j/ZY6LrKE1Rc8WAlV5Er0vn3iHXbsIVcxPMIxZjBMPVXHobp83g6IUPMIWV4zgBoPicofcPyB+fSwE6kAsn4ZcqoxXkzxN/CCaUeb6REBRmOYRQ1xCPjwVzWx/BIsMTYV9e44Rce6mGeuHXgDvnxBKySQbvyPOlMQv0IWMtzKNh2zM0vjJyDSvz6PkGI0mX0RzjsLs2eEm2T/DOyn5VJiEm+rYcCkckDEwRLwppY2tuJu2q4qB92JDectYMsbTc3afurBdV0T/camaWbGbR5HkVS75oroykiLmjPRGsElGQIDAQAB';
    }

    private static $ins;
    public static function instance() : AliPaySandbox {
        if (empty(self::$ins)) self::$ins = new self();
        return self::$ins;
    }
}