<?php
namespace app\common\lib\rongcloud;

use app\common\lib\rongcloud\methods\{User,Message,Wordfilter,Group,Chatroom,Push,SMS};
use think\Env;

/**
 * 融云 Server API PHP 客户端
 * create by kitName
 * create datetime : 2016-09-05 
 * 
 * v2.0.1
 */


class RongCloud
{
    private static $instance = null;
    public static function getInstance() :RongCloud {
        if (empty(self::$instance)) {
            $appKey = config('rongYun.appkey');
            $appSecret = config('rongYun.appSecret');

            if (empty($appKey) || empty($appSecret)) {
                throw new \Exception('Please config RongCloud app key and secret!');
            }
            self::$instance = new static($appKey, $appSecret, 'json');
        }
        return self::$instance;
    }

    protected $container = [];
    
    /**
     * 参数初始化
     * @param $appKey
     * @param $appSecret
     * @param string $format
     */
    protected function __construct($appKey, $appSecret, $format = 'json') {
        $this->SendRequest = new SendRequest($appKey, $appSecret, $format);
    }

    /**
     * @param $className
     * @return mixed
     */
    protected function factory($className) {
        if (empty($this->container[$className])) {
            $this->container[$className] = new $className($this->SendRequest);
        }
        return $this->container[$className];
    }


    public function User() :User {
        return $this->factory(User::class);
    }
    
    public function Message() :Message {
        return $this->factory(Message::class);
    }
    
    public function Wordfilter() :Wordfilter {
        return $this->factory(Wordfilter::class);
    }
    
    public function Group() :Group {
        return $this->factory(Group::class);
    }
    
    public function Chatroom() :Chatroom {
        return $this->factory(Chatroom::class);
    }
    
    public function Push() :Push {
        return $this->factory(Push::class);
    }
    
    public function SMS() :SMS {
        return $this->factory(SMS::class);
    }
    
}