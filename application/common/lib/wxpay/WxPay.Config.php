<?php
/**
* 	配置账号信息
*/

class WxPayConfig
{
	//=======【基本信息设置】=====================================
	//
	/**
	 * TODO: 修改这里配置为您自己申请的商户信息
	 * 微信公众号信息配置
	 *
	 * APPID：绑定支付的APPID（必须配置，开户邮件中可查看）
	 *
	 * MCHID：商户号（必须配置，开户邮件中可查看）
	 *
	 * KEY：商户支付密钥，参考开户邮件设置（必须配置，登录商户平台自行设置）
	 * 设置地址：https://pay.weixin.qq.com/index.php/account/api_cert
	 *
	 * APPSECRET：公众帐号secert（仅JSAPI支付的时候需要配置， 登录公众平台，进入开发者中心可设置），
	 * 获取地址：https://mp.weixin.qq.com/advanced/advanced?action=dev&t=advanced/dev&token=2005451881&lang=zh_CN
	 * @var string
	 */
	private static $Mchs = [
	    // 新的商户号
	    'wx071715cfd51ed8cc' => [
	        'mchId' => self::MCHID,
            'key' => self::KEY,
        ],
        // 老的商户号
        'wxcba36d37fe539203' => [
            'mchId' => self::MCHID0,
            'key' => self::KEY0,
        ],
    ];
	public static function MchId($appId) {
	    return self::$Mchs[$appId]['mchId'] ?? null;
    }
    public static function Key($appId) {
	    return self::$Mchs[$appId]['key'] ?? null;
    }

	const MCHID0 = '1489745732';
	const KEY0 = 'd19408f928f966e4d9db543b1912cc33';

	const MCHID = '1488447642';
	const KEY = 'ee5370b569c366cee10a874485d9bbc5';

	const APPID = 'wx230e492bbfcf56b0';
	const APPSECRET = 'd4e01425c4942e3266aef7b32ed0bc40';

	//=======【证书路径设置】=====================================
	/**
	 * TODO：设置商户证书路径
	 * 证书路径,注意应该填写绝对路径（仅退款、撤销订单时需要，可登录商户平台下载，
	 * API证书下载地址：https://pay.weixin.qq.com/index.php/account/api_cert，下载之前需要安装商户操作证书）
	 * @var path
	 */
	private static $SSLCerts = [
	    // 新商户号证书
        'wx071715cfd51ed8cc' => self::SSLCERT_PATH,
        'wxcba36d37fe539203' => self::SSLCERT_PATH0,
    ];
	public static function SSLCertPath($appId) {
	    return self::$SSLCerts[$appId] ?? null;
    }
	const SSLCERT_PATH0 = __DIR__.'/cert0/apiclient_certs.pem';
	const SSLCERT_PATH = __DIR__.'/cert/apiclient_certs.pem';

    //=======【curl代理设置】===================================
    /**
     * TODO：这里设置代理机器，只有需要代理的时候才设置，不需要代理，请设置为0.0.0.0和0
     * 本例程通过curl使用HTTP POST方法，此处可修改代理服务器，
     * 默认CURL_PROXY_HOST=0.0.0.0和CURL_PROXY_PORT=0，此时不开启代理（如有需要才设置）
     * @var unknown_type
     */
    const CURL_PROXY_HOST = "0.0.0.0";//"10.152.18.220";
    const CURL_PROXY_PORT = 0;//8080;

	//=======【上报信息配置】===================================
	/**
	 * TODO：接口调用上报等级，默认紧错误上报（注意：上报超时间为【1s】，上报无论成败【永不抛出异常】，
	 * 不会影响接口调用流程），开启上报之后，方便微信监控请求调用的质量，建议至少
	 * 开启错误上报。
	 * 上报等级，0.关闭上报; 1.仅错误出错上报; 2.全量上报
	 * @var int
	 */
	const REPORT_LEVENL = 1;
}
