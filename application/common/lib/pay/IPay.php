<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/9/1
 * Time: 14:28
 */

namespace app\common\lib\pay;


interface IPay
{
    /**
     * 下订单
     *
     * @param $out_trade_no string 订单号
     * @param $total_fee float 支付总金额
     * @param $body string 商品描述
     * @param $client_type integer 支付类型，比如 1-APP、2-微信页面，3-PC支付
     * @param $client_ip string 支付的客户端实际IP
     * @param $detail string
     * @param null|mixed $ext
     * @return mixed
     */
    function unifiedOrder($out_trade_no, $total_fee, $body, $client_type, $client_ip, $detail = '', $ext = null);

    /**
     * 获取字段key，方便客户端调用
     *
     * @return string
     */
    function getKey();
}