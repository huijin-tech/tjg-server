<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/9/1
 * Time: 14:33
 */

namespace app\common\lib\pay;



class PayFactory
{
    const ALIPAY = 1;
    const WEIXINPAY = 2;

    const APPLEPAY = 4;

    /**
     * @param $pay_method
     * @return IPay|null
     */
    public static function getPayInstance($pay_method) :?IPay {
        if (1 == $pay_method) {
            return AliPay::getInstance();
        } elseif (2 == $pay_method) {
            return WeixinPay::getInstance();
        } elseif (4 == $pay_method) {
            return ApplePay::getInstance();
        } else {
            return null;
        }
    }

    public static function getPayName($pay_method) {
        if (1 == $pay_method) {
            return '支付宝';
        } elseif (2 == $pay_method) {
            return '微信支付';
        } elseif (4 == $pay_method) {
            return '苹果支付';
        } else {
            return null;
        }
    }

}