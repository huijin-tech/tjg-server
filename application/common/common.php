<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2017-08-03
 * @Time: 15:17
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： common.php
 */

if (!function_exists('passEncrypt')) {
    /**
     * passEncrypt
     * 密码加密处理
     *
     * @author zhengkai
     * @date 2017-08-02
     *
     * @param string $pass 需要加密的密码字符串
     * @return string
     */
    function passEncrypt($pass)
    {
        $pass = hash_hmac('sha256', ENCRYPT_KEY, $pass);
        return $pass;
    }
}


if (!function_exists('apiToken')) {
    /**
     * apiToken
     * api token生成
     *
     * @author zhengkai
     * @date 2017-08-01
     *
     * @param int $appId 应用id
     * @param string $appSecret 应用密钥
     * @return string
     */
    function apiToken($appId, $appSecret)
    {
        $token = md5(md5($appId.$appSecret));
        return $token;
    }
}