<?php
namespace app\common\controller;

use Curl\Curl;
use think\Controller;

class SinaHq extends Controller {
    private static $hq_api='http://hq.sinajs.cn/';

    // 新文件名
    public static function getStock(array $code)
    {
        $curl = new Curl();

        $curl->get(self::$hq_api.'list='.implode(',',$code));
        $result = [];
        if (!$curl->error) {
            $data=iconv("gb2312","utf-8//IGNORE", $curl->response);
            preg_match_all('/="(.*?)"/', $data,$temp);
            preg_match_all('/(?<=hq_str_).*?(?=\=)/', $data,$code);
            $stock_arr=$temp[1] ?? [];
            $stocks_code=$code[0] ?? [];
            foreach ($stock_arr as $k=>$v){
                if(!$v) continue;
                $info=explode(',',$v);
                array_push($info,$stocks_code[$k]);
                $result[]=$info;
            }
        }
        return $result;
    }

    public static function changeData(array $data){
        $stocks=[];
        foreach ($data as $key=>$value){
            if(!$value[0]) continue;
            $stocks[]=[
                'rlimit'=>$value[2]==0 ? '0.00' : sprintf("%.2f",round((($value[3]-$value[2])/$value[2])*100,2)),
                'sname'=>$value[0],
                'scode'=>$value[33]??'',
                'nowPri'=>sprintf("%.2f",$value[3]),
            ];
        }
        return $stocks;
    }

}