<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2018-05-07
 * @Time: 10:43
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： Files.php
 */
namespace app\common\controller;

use library\aliYunOss;
use think\Controller;
use think\Log;

class Files extends Controller {
    protected static $baseDir = ROOT_PATH.'public'.DS.'uploads'; // 保存目录
    protected static $urlPath = '/public/uploads'; // url路径

    // 新文件名
    protected static function newName()
    {
        return time().rand(100000, 999999);
    }


    /**
     * upload
     * 文件上传处理
     *
     * @author zhengkai
     * @date 2018-05-07
     *
     * @param string $fileType 允许上传的文件类型（后缀名）
     * @param int $fileSize 允许上传的文件大小，默认1MB 单位KB
     * @param string $form 文件表单名
     * @param string $type 上传文件类型：img=图片，file=文件
     * @return array
     */
    public static function upload($type='img', $fileType='jpg,jpeg,png,gif', $fileSize=1024, $form='file')
    {
        // 获取表单上传文件 例如上传了001.jpg
        $file = request()->file($form);

        $dir = '';
        if ($type=='img') $dir = 'image/'.date('Ymd', time()); // 图片存储目录
        if ($type=='file') $dir = 'file/'.date('Ymd', time()); // 文件存放目录

        $oldFileName = $file->getInfo('name');
        $savePath = self::$baseDir.DS.$dir;   // 存放目录
        $newFileName = self::newName();    // 新文件名
        // $webPath = DS.'upload'.DS.$dir;             // web目录
        $url = self::$urlPath.'/'.$dir;             // web目录

        // 移动到框架应用根目录/public/uploads/ 目录下
        if($file){
            $info = $file
                ->validate([
                    'size' => $fileSize * 1024, // 文件大小 单位：字节(Bytes)
                    'ext' => $fileType          // 允许上传的文件类型后缀名
                ])
                ->move($savePath, $newFileName);
            if($info){
                return array('code'=>1, 'msg'=>'上传成功', 'data'=>[
                    'fileName' => $oldFileName,
                    'filePath' => $savePath.DS.$info->getSaveName(),
                    // 'url' => $webPath.DS.$info->getSaveName()
                    'url' => $url.'/'.$info->getSaveName()
                ]);
            }else{
                // 上传失败获取错误信息
                Log::error("文件上传失败：{$file->getError()}");
                return array('code'=>0, 'msg'=>$file->getError(), 'data'=>null);
            }
        }
    }

    /**
     * imgSync
     * 自动下载内容中的远程图片并同步到阿里云OSS
     *
     * @author zhengkai
     * @date 2018-05-07
     *
     * @param $body
     * @return mixed|string
     */
    public static function imgSync($body)
    {
        $content = stripcslashes($body);
        $imgArr = [];
        $pattern = "/<img.*?src=[\'|\"](.*?(?:[\.gif|\.jpg|\.jpeg|\.png|\.bmp|\.webp]))[\'|\"].*?[\/]?>/";
        preg_match_all($pattern, $content,$imgArr);
        $imgArr = array_unique($imgArr[1]);

        // 文件保存目录
        $savePath = self::$baseDir.'/remote';
        Log::log($savePath);
        // 目录不存在就自动创建
        if (!is_dir($savePath)) mkdir($savePath, 0775, true);

        set_time_limit(0);
        foreach ($imgArr as $key=>&$val) {
            $strUrl = parse_url($val);
            if ($strUrl['host']==='image.cloud.51taojingu.com') break;

            $val = trim($val);
            $getImg = @file_get_contents($val);
            $imgName = self::newName().'.'.substr($val, -3, 3);
            $path = $savePath.'/'.$imgName;
            $url = self::$urlPath.'/remote/'.$imgName;

            if ($getImg) {
                $fp = @fopen ( $path, "w" );
                @fwrite ( $fp, $getImg );
                @fclose ( $fp );

                if (file_exists($path)) {
                    $syncAli = aliYunOss::getInstance()->uploadFile($imgName, $path, 'image/');

                    $imgUrl = $val;

                    if ($syncAli) {
                        $imgUrl = str_replace('tjg-customer-flow-division.oss-cn-shanghai.aliyuncs.com', 'image.cloud.51taojingu.com', $syncAli['info']['url']);

                        // 删除本地原图
                        @unlink($path);
                    }

                    // 替换原来的图片地址
                    $content = str_replace($val, $imgUrl, $content);
                }
            }
        }
        return $content;
    }
}