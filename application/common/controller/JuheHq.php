<?php
namespace app\common\controller;

use Curl\Curl;
use think\Controller;
use think\Env;

class JuheHq extends Controller {
    private static $hq_api='http://web.juhe.cn:8080/finance/stock/hs?';

    public static function getApi($stock_code=''){
        return self::$hq_api=self::$hq_api.'key='.Env::get('JUHE.JUHE_APP_KEY').'&gid='.$stock_code;
    }


    public static function getStock($stock_code)
    {
        $curl = new Curl();
        $curl->get(self::getApi($stock_code));
        $result = [];
        if (!$curl->error) {
            $result=json_decode($curl->response,true);
            $result=isset($result['resultcode']) ? $result : [];
        }
        return $result;
    }

    public static function changeData(array $data){
        $stocks=[];
        foreach ($data as $key=>$value){
            $stocks[]=[
                'rlimit'=>$value['increPer'],
                'sname'=>$value['name'],
                'scode'=>$value['gid'],
                'nowPri'=>$value['nowPri'],
            ];
        }
        return $stocks;
    }

}