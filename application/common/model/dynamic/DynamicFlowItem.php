<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2018/1/16
 * Time: 9:14
 */

namespace app\common\model\dynamic;
use app\common\model\Topic;


/**
 * Class DynamicFlowContent
 * @package app\common\model
 *
 * @property integer item_id
 * @property DynamicFlowItemTeacher item_teacher
 *
 * @property string item_title
 * @property string item_summary
 * @property string item_cover
 *
 * @property DynamicFlowItemColumn item_column
 *
 * @property integer item_filesize
 * @property integer item_duration
 *
 * @property mixed item_topic
 */
class DynamicFlowItem {
}
