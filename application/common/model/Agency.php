<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/8/22
 * Time: 14:49
 */

namespace app\common\model;


use think\Model;

/**
 * Class Agency
 * @package app\common\model
 *
 * @property integer id
 * @property string name
 * @property string logo
 * @property string intro
 * @property string business_license
 * @property string business_license_number
 * @property string corporate_address
 * @property string contact_person
 * @property string contact_number
 * @property integer contract_expired
 * @property string created
 * @property string updated
 * @property integer signed_time
 * @property float profit_rate
 * @property float salesman_profit_rate
 */
class Agency extends Model
{
    protected $table = 'agency';
    protected $pk = 'id';

    private static $SaveFields = [
        'name'  ,
        'logo'  ,
        'intro'  ,
        'business_license'  ,
        'business_license_number'  ,
        'corporate_address'  ,
        'contact_person'  ,
        'contact_number'  ,
        'contract_expired'  ,
        'profit_rate'  ,
        'salesman_profit_rate'  ,
    ];
    public static function getSaveFields(): array {
        return self::$SaveFields;
    }
}