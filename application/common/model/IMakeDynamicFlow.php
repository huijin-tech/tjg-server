<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2018/1/17
 * Time: 15:44
 */

namespace app\common\model;


use http\Exception;

interface IMakeDynamicFlow
{
    /**
     * 产生老师动态
     *
     * @param bool $isNew
     * @param integer $source
     * @return DynamicFlow
     */
    public function makeDynamicData(bool $isNew, int $source = DynamicFlow::SOURCE_SYSTEM): DynamicFlow;
}