<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/8/21
 * Time: 15:40
 */

namespace app\common\model\curriculum;


use app\admin\model\PageResponse;
use think\Db;
use think\Model;

class CurriculumPostBill extends Model
{
    protected $table = 'curriculum_post_bill';
    protected $pk = 'bill_id';

    public static function manageList(int $limit, int $offset, int $video_id = 0) :PageResponse {

        $filter = null;
        $params = ['limit'=>$limit, 'offset'=>$offset,];
        if ($video_id > 0) {
            $filter .= ' AND bill_video=:video_id';
            $params['video_id'] = $video_id;
        }

        $sql = <<<SQL
SELECT * FROM curriculum_post_bill
WHERE bill_user=0 $filter
LIMIT :limit OFFSET :offset
SQL;
        $list = Db::query($sql, $params);

        $sql = <<<SQL
SELECT count(*) AS num FROM curriculum_post_bill
WHERE bill_user=0 $filter
SQL;
        unset($params['limit'], $params['offset']);
        $re = Db::query($sql, $params);
        $total = intval($re[0]['num'] ?? 0);

        return PageResponse::success($list, $total);
    }

}