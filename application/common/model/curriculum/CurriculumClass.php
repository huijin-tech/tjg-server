<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/8/16
 * Time: 15:33
 */

namespace app\common\model\curriculum;


use app\admin\model\PageResponse;
use think\Db;
use think\Model;

class CurriculumClass extends Model
{
    protected $table = 'curriculum_class';
    protected $pk = 'class_id';

    private static $SaveFields = ['class_id','class_name','class_remark','class_sort','class_addtime','class_updatetime'];
    public static function getSaveFields(): array {
        return self::$SaveFields;
    }

    public static function manageList(int $limit, int $offset) :PageResponse {
        $list = self::limit($offset, $limit)->order(['class_id' => 'desc'])->select();
        $total = self::count();
        return PageResponse::success($list, $total);
    }
    
    public static function classNames() {
        $list = Db::query('select class_id,class_name from curriculum_class');
        return $list;
    }

    public static function update($data = [], $where = [], $field = null)
    {
        $model = new static();
        if (!empty($field)) {
            $model->allowField($field);
        }
        $model->isUpdate(true)->save($data, $where);
        return $model;
    }

    public static function create($data = [], $field = null)
    {
        $model = new static();
        if (!empty($field)) {
            $model->allowField($field);
        }
        $model->isUpdate(false)->save($data, []);
        return $model;
    }
}