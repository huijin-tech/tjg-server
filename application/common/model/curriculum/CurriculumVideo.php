<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/8/17
 * Time: 11:00
 */

namespace app\common\model\curriculum;


use app\admin\model\PageResponse;
use app\common\model\teacher\Teacher;
use think\Db;
use think\Model;

/**
 * Class CurriculumVideo
 * @package app\common\model\curriculum
 *
 * @property integer video_id
 * @property integer video_curriculum
 * @property integer video_level
 * @property integer video_user_level
 * @property integer video_teacher
 * @property integer video_title
 * @property integer video_url
 * @property integer video_cover
 * @property integer video_views
 * @property integer video_addtime
 * @property integer video_updatetime
 * @property integer video_description
 * @property integer video_isfree
 * @property integer video_free_start_time
 * @property integer video_free_end_time
 * @property integer video_sort
 * @property integer duration
 * @property integer statistics_play
 * @property integer statistics_play_finish
 * @property integer hide
 * @property integer preview_allowed
 * @property integer preview_url
 * @property integer vid
 */
class CurriculumVideo extends Model
{
    protected $table = 'curriculum_video';
    protected $pk = 'video_id';

    private static $SaveFields = [
        'video_curriculum',
        'video_level',
        'video_user_level',
        'video_teacher',
        'video_title',
        'video_url',
        'video_cover',
        'video_addtime',
        'video_updatetime',
        'video_description',
        'video_isfree',
        'video_free_start_time',
        'video_free_end_time',
        'video_sort',
        'duration',
        'hide',
        'preview_allowed',
        'preview_url',
        'views_total',
    ];
    public static function getSaveFields(): array {
        return self::$SaveFields;
    }

    /**
     * 管理端列表
     *
     * @param int $limit
     * @param int $offset
     * @param int $curriculum_id
     * @param int|null $hide
     * @return PageResponse
     */
    public static function manageList(int $limit, int $offset, int $curriculum_id = 0, $hide = null) :PageResponse {
        $filter = '';
        $params = ['limit' => $limit, 'offset' => $offset, ];

        if ($curriculum_id > 0) {
            $filter .= ' AND v.video_curriculum=:curriculum_id ';
            $params['curriculum_id'] = $curriculum_id;
        }
        if (null !== $hide) {
            $filter .= ' AND v.hide=:hide ';
            $params['hide'] = $hide;
        }

        $sql = <<<SQL
SELECT v.*,vs.netease_status vod_stat,
    (SELECT rec_id FROM recommend r WHERE v.video_id=r.rec_item_id AND r.rec_item_type=1 LIMIT 1)  
FROM curriculum_video v
LEFT JOIN video_storage vs ON v.vid=vs.id
WHERE TRUE {$filter}
ORDER BY v.video_curriculum DESC, v.video_sort ASC 
LIMIT :limit OFFSET :offset
SQL;
        $list = Db::query($sql, $params);

        $sql = <<<SQL
SELECT count(*) as num FROM curriculum_video v
WHERE TRUE {$filter}
SQL;
        unset($params['limit'], $params['offset']);
        $re = Db::query($sql, $params);
        $total = $re[0]['num'] ?? 0;

        return PageResponse::success($list, $total);
    }

    public function isFree() {
        $now = time();
        return (1 == $this->video_isfree) || (
                2 == $this->video_isfree && ($this->video_free_start_time < $now && $now < $this->video_free_end_time)
            );
    }

    public function setHide($hide) {
        $this->hide = $hide;
        $this->save();

        \app\api\model\curriculum\Curriculum::countVideos($this->video_curriculum);
        Teacher::countNumVideo($this->video_teacher);
    }
}