<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/8/14
 * Time: 14:52
 */

namespace app\common\model\teacher;


use think\Model;

class TeacherDegree extends Model
{
    protected $table = 'teacher_degree';
    protected $pk = 'id';

}