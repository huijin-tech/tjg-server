<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/9/8
 * Time: 15:32
 */

namespace app\common\model;


use think\Model;

class Recommend extends Model
{
    protected $table = 'recommend';
    protected $pk = 'rec_id';

}