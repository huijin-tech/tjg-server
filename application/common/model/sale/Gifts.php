<?php
namespace app\common\model\sale;

use app\common\model\BuyBill;

/**
 * Class Gifts
 * @package app\common\model\sales
 *
 * @property array|object curricula
 * @property array|object lives
 * @property array|object columns
 *
 */
class Gifts
{
    const FIELDS = [
        'curricula' => BuyBill::BUY_ITEM_CURRICULUM,
        'lives'     => BuyBill::BUY_ITEM_LIVE,
        'columns'   => BuyBill::BUY_ITEM_SPECIAL,
    ];

    public function __construct(array $data)
    {
        foreach (self::FIELDS as $field => $type) {
            $d = $data[$field] ?? null;
            if (!empty($d)) {
                $d = json_decode(json_encode($d,JSON_UNESCAPED_UNICODE));
                foreach ($d as & $v) {
                    $v = intval($v);
                }
                $this->$field = $d;
            }
        }

    }

    public static function getItemTypeId($field) {
        return self::FIELDS[$field];
    }
}