<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2017-08-01
 * @Time: 18:02
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： Apply.php
 */
namespace app\common\model;

use think\Model;

class Apply extends Model {
    protected $table = 'apply';
}