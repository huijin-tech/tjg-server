<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/9/16
 * Time: 10:45
 */

namespace app\common\model;


use app\api\model\user\Users;
use think\Cache;
use think\cache\Driver;
use think\Env;

class Chat
{
    /**
     * @return Driver
     */
    public static function getCache() :Driver {
        return Cache::tag('chat');
    }

    /**
     * @param $id
     * @param bool $val
     * @return mixed|null
     */
    public static function cache($id, $val = false) {

        $c = self::getCache();
        if (false === $val) {
            return $c->get($id);
        } else {
            $c->set($id, $val);
            return null;
        }
    }

    /**
     * @deprecated
     * @param int $liveChannelId
     * @return string
     */
    public static function getDefaultCharRoom($liveChannelId = 0) {
        return config('server.charroom') . '_' . $liveChannelId;
    }

    /**
     * @param $id
     * @return string
     */
    public static function getChatId($id) {
        return Env::get('chatIdPrefix','dev_').$id;
    }

    /**
     * 获取聊天用户信息
     * @param $id
     * @return array|bool
     */
    public static function getUserInfo($id) {
        $chat = self::getCache()->get($id);
        if (empty($chat)) return false;

        return [
            'id' => $chat['id'],
            'name' => $chat['name'],
            'avatar' => $chat['avatar'],
        ];
    }

}