<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2018-04-09
 * @Time: 23:39
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： Salesman.php
 */
namespace app\common\model\zds;

use think\Model;

/**
 * Class Salesman
 * @package app\common\model\zds
 *
 * @property mixed id
 * @property mixed agencyid
 * @property mixed username
 * @property mixed password
 * @property mixed realname
 * @property mixed sex
 * @property mixed mobile
 * @property mixed cooperate_start_time
 * @property mixed cooperate_end_time
 * @property mixed cooperate_status
 * @property mixed invite_code
 * @property mixed menu
 * @property mixed auth
 * @property mixed bonus
 * @property mixed create_time
 * @property mixed update_time
 * @property mixed remark
 * @property mixed status
 */
class Salesman extends Model {
    protected $table = 'zds_salesman';
    protected $pk = 'id';
}