<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2018-04-03
 * @Time: 16:32
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： Customer.php
 */
namespace app\common\model\zds;

use think\Model;

/**
 * Class Customer
 * @package app\common\model\zds
 *
 * @property mixed id
 * @property mixed userid
 * @property mixed agencyid
 * @property mixed salesmanid
 * @property mixed return_status
 * @property mixed return_remark
 * @property mixed remark
 */
class Customer extends Model {
    protected $table = 'zds_customer';
    protected $pk = 'id';

    /**
     * queryIsExist
     * 查询销售系统客户是否存在
     *
     * @author zhengkai
     * @date 2018-04-03
     *
     * @param int $userId 掌乾用户id
     * @return int|string
     */
    public static function queryIsExist($userId)
    {
        $data = self::where('userid', $userId)->count();

        return $data;
    }

    /**
     * is_vip
     * 标记掌乾用户是否为vip
     *
     * @author zhengkai
     * @date 2018-04-20
     *
     * @param int $userId 掌乾用户id
     * @return int
     */
    public static function is_vip($userId)
    {
        $result = self::queryIsExist($userId);
        if ($result) {
            return 1;
        } else {
            return 0;
        }
    }



    /**
     * getSingleCustomer
     * 获取单条销售系统客户信息
     *
     * @author zhengkai
     * @date 2018-04-09
     *
     * @param int $userId 掌乾用户id
     * @return null|static
     */
    public static function getSingleCustomer($userId)
    {
        $data = self::get(['userid'=>$userId]);

        return $data;
    }
}