<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2018-04-03
 * @Time: 18:51
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： Order.php
 */
namespace app\common\model\zds;

use app\common\model\BuyBill;
use think\Db;
use think\Log;
use think\Model;

/**
 * Class Order
 * @package app\common\model\zds
 *
 * @property mixed id
 * @property mixed billid
 * @property mixed billno
 * @property mixed userid
 * @property mixed customerid
 * @property mixed service_start_time
 * @property mixed service_end_time
 * @property mixed service_status
 * @property mixed audit_status
 * @property mixed remark
 * @property mixed create_time
 * @property mixed agencyid
 * @property mixed salesmanid
 * @property mixed service_period
 */
class Order extends Model {
    protected $table = 'zds_order';
    protected $pk = 'id';

    /**
     * queryBuyStatus
     * 商品是否存在已购买（已付款，订单审核通过）数据
     *
     * @param int $goodsType 商品类型：1=课程 2=直播 3=专栏
     * @param int $goodsId 商品id
     * @param int $userId 掌乾用户id
     * @return int|string
     */
    public static function queryBuyStatus($goodsType=1, $goodsId, $userId)
    {
        $data = self::alias('o')
            ->join('buy_bill bill', 'o.billid=bill_id', 'left')
            ->where('bill.bill_item_type', $goodsType)
            ->where('bill.bill_item_id', $goodsId)
            ->where('bill.bill_have_paid', 'true')
            ->where('o.userid', $userId)
            ->where('o.audit_status', 99)
            ->count();

        return $data;
    }

    /**
     * createOrder
     * 创建销售系统订单
     *
     * @author zhengkai
     * @date 2018-04-09
     *
     * @param int $billId
     * @return bool
     */
    public static function createOrder($billId)
    {
        $bill = BuyBill::get($billId);
        $customer = Customer::getSingleCustomer($bill->bill_user);

        // 服务结束时间
        // $serviceEndTime = strtotime("+1 months", $bill->bill_paytime);
        $serviceEndTime = strtotime($bill->bill_paytime);
        $serviceEndTime = date('Y-m-d', strtotime($serviceEndTime));
        // $serviceEndTime = date('Y-m-d 00:00:00', strtotime("+1 days", $serviceEndTime));
        // 服务状态
        // $serviceStatus = (time()>=$bill->bill_paytime)?1:0;
        $serviceStatus = 0;

        $data = [
            'billid' => $billId,
            'billno' => $bill->bill_no,
            'userid' => $bill->bill_user,
            'customerid' => $customer->id,
            // 'service_start_time' => date('Y-m-d H:i:s', $bill->bill_paytime),
            'service_start_time' => $serviceEndTime,
            'service_end_time' => $serviceEndTime,
            'service_status' => $serviceStatus,
            'audit_status' => 99,
            'remark' => 'VIP专区在线交易购买',
            'create_time' => date('Y-m-d H:i:s', time()),
            'agencyid' => $customer->agencyid,
            'salesmanid' => $customer->salesmanid,
            'service_period' => 0,
        ];

        Db::startTrans();
        try {
            $order = self::create($data);

            // 创建销售系统订单审核记录
            OrderAudit::createAudit($order->id, $order->salesmanid);

            // 创建销售系统订单提成记录
            OrderBonus::createBonus($order->id, $billId);

            Db::commit();
            return true;
        } catch (\Exception $e) {
            Db::rollback();
            Log::error("创建销售订单【error】：{$e->getMessage()}");
            return false;
        }
    }
}