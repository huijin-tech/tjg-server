<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2018-04-09
 * @Time: 23:42
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： OrderAudit.php
 */
namespace app\common\model\zds;

use think\Db;
use think\Log;
use think\Model;

/**
 * Class OrderAudit
 * @package app\common\model\zds
 *
 * @property mixed id
 * @property mixed orderid
 * @property mixed applicant_identity
 * @property mixed applicant
 * @property mixed apply_time
 * @property mixed auditor_identity
 * @property mixed auditor
 * @property mixed audit_phase
 * @property mixed audit_status
 * @property mixed audit_remark
 * @property mixed audit_time
 */
class OrderAudit extends Model {
    protected $table = 'zds_order_audit';
    protected $pk = 'id';

    /**
     * createAudit
     * 创建销售订单审核记录
     *
     * @author zhengkai
     * @date 2018-04-09
     *
     * @param int $orderId 销售订单id
     * @param int $applicant 审核申请人(销售系统业务员)id
     * @return bool
     */
    public static function createAudit($orderId, $applicant)
    {
        $data = [
            'orderid' => $orderId,
            'applicant_identity' => 2, // 业务员申请审核
            'applicant' => $applicant,
            'apply_time' => date('Y-m-d H:i:s', time()),
            'auditor_identity' => 1, // 系统管理员审核
            'auditor' => 1, // 系统超级管理员admin
            'audit_phase' => 2, // 阶段2
            'audit_status' => 99, // 审核通过
            'audit_remark' => '在线购买，系统自动审核',
            'audit_time' => date('Y-m-d H:i:s', time())
        ];

        Db::startTrans();
        try {
            self::create($data);
            Db::commit();
            return true;
        } catch (\Exception $e) {
            Db::rollback();
            Log::error("创建销售订单审核记录【error】：{$e->getMessage()}");
            return false;
        }
    }
}