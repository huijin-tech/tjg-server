<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2018-04-09
 * @Time: 23:41
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： Agency.php
 */
namespace app\common\model\zds;

use think\Model;

/**
 * Class Agency
 * @package app\common\model\zds
 *
 * @property mixed id
 * @property mixed name
 * @property mixed logo
 * @property mixed description
 * @property mixed contacts
 * @property mixed telephone
 * @property mixed cooperate_start_time
 * @property mixed cooperate_end_time
 * @property mixed cooperate_status
 * @property mixed bonus
 * @property mixed salesman_bonus
 * @property mixed create_time
 * @property mixed update_time
 * @property mixed remark
 * @property boolean status
 * @property mixed sys_userid
 */
class Agency extends Model {
    protected $table = 'zds_agency';
    protected $pk = 'id';
}