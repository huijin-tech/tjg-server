<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/10/31
 * Time: 18:13
 */

namespace app\common\model;


use think\Model;

/**
 * Class RealNameAuthentication
 * @package app\common\model
 *
 * @method static static get($data, $with = [], $cache = false)
 *
 * @property integer id
 * @property integer user_id
 * @property string realname
 * @property string identity_card_number
 * @property integer sex
 * @property integer birthday
 * @property string address
 * @property boolean auth_status
 * @property integer auth_time
 */
class RealNameAuthentication extends Model
{
    protected $table = 'real_name_authentication';
    protected $pk = 'id';
}