<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/9/26
 * Time: 18:17
 */

namespace app\common\model\app;


use think\Db;
use think\Model;

class AppStartPage extends Model
{
    protected $table = 'app_start_page';
    protected $pk = 'id';

    /**
     * api接口
     *
     * @return array|\stdClass
     */
    public static function getRand() {
        $sql = <<<SQL
SELECT id,image,has_link,link_url FROM app_start_page
WHERE hide=FALSE 
ORDER BY random()
LIMIT 1
SQL;
        $re = Db::query($sql);

        return $re[0] ?? new \stdClass();
    }

    public static function toggleShow(int $id, int $hide) {
        Db::execute('UPDATE app_start_page SET hide=:hide WHERE id=:id', ['id' => $id, 'hide' => $hide]);
    }
}