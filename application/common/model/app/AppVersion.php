<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/9/18
 * Time: 10:45
 */

namespace app\common\model\app;


use think\Db;
use think\Model;

/**
 * Class AppVersion
 * @package app\common\model\app
 *
 * @property string apk_url
 */
class AppVersion extends Model
{
    const PLATFORM_ANDROID = 1;
    const PLATFORM_IOS = 2;

    protected $table = 'app_version';
    protected $pk = 'id';

    public static function latestVersion($code, $platform) {
        $sql = <<<SQL
SELECT * FROM app_version 
WHERE platform=:platform
ORDER BY code DESC LIMIT 1
SQL;
        $re = Db::query($sql, ['platform' => $platform]);
        if (empty($re)) {
            return ['has_update' => false];
        }
        $ver = $re[0];
        $ver['created'] = substr($ver['created'],0,20);
        $ver['has_update'] = $ver['code'] > $code;

        return $ver;
    }


}