<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2018/3/1
 * Time: 16:19
 */

namespace app\common\model\app;

/**
 * 链接类型常量，用于（消息通知、Banner）
 *
 * Class ConstLinkType
 * @package app\common\model\app
 *
 */
final class ConstLinkType
{
    private function __construct() {}

    const LINK_TYPE_NULL            = 0;
    const LINK_TYPE_URL             = 1;
    const LINK_TYPE_LIVE            = 2;
    const LINK_TYPE_CURRICULUM      = 3;
    const LINK_TYPE_TEACHER         = 4;
    const LINK_TYPE_ESSAY           = 5;
    const LINK_TYPE_COLUMN          = 6;
    const LINK_TYPE_COLUMN_ESSAY    = 7;
    const LINK_TYPE_BILL            = 8;
    const LINK_TYPE_ASK             = 9;
    const LINK_TYPE_ASK_DETAIL      = 10;
    const LINK_TYPE_TOPIC           = 11;


    private static $Names = [
        self::LINK_TYPE_NULL            => '无操作',
        self::LINK_TYPE_URL             => 'URL地址',
        self::LINK_TYPE_LIVE            => '直播',
        self::LINK_TYPE_CURRICULUM      => '课程',
        self::LINK_TYPE_TEACHER         => '老师',
        self::LINK_TYPE_ESSAY           => '资讯文章',
        self::LINK_TYPE_COLUMN          => '专栏',
        self::LINK_TYPE_COLUMN_ESSAY    => '专栏文章',
        self::LINK_TYPE_BILL            => '订单',
        self::LINK_TYPE_ASK             => '问答首页',
        self::LINK_TYPE_ASK_DETAIL      => '问答详情',
        self::LINK_TYPE_TOPIC           => '图文直播',
    ];

    public static function getNames(): array {
        return self::$Names;
    }

    public static function getName($typeId): ?string {
        return self::$Names[$typeId] ?? null;
    }

}