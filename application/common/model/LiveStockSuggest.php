<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/11/7
 * Time: 9:56
 */

namespace app\common\model;


use think\Db;
use think\Model;

class LiveStockSuggest extends Model
{
    protected $table = 'live_stock_suggest';
    protected $pk = 'id';

    private static $SaveFields = [
        'id',
        'live_channel_id',
        'teacher_id',
        'content',
    ];
    public static function getSaveFields(): array {
        return self::$SaveFields;
    }

    /**
     * @param int $rows
     * @param int $page
     * @param int $cid
     * @param int $tid
     * @return mixed
     */
    public static function manageList(int $rows, int $page, int $cid = 0, int $tid = 0) {

        $query = Db::table('live_stock_suggest a');
        $query->join('teacher t','t.id=a.teacher_id');
        $query->join('live_channel c','c.id=a.live_channel_id');

        if ($cid > 0) $query->where('a.live_channel_id','=', $cid);
        if ($tid > 0) $query->where('a.teacher_id','=', $tid);

        $query->order('a.id','desc');

        $query->field('a.*, t.realname teacher_name, c.name channel_name');
        $result = $query->paginate($rows,false, ['page' => $page]);

        return $result->toArray();
    }

    /**
     * @param int $page
     * @param int $rows
     * @param int $cid
     * @return false|\PDOStatement|string|\think\Collection
     */
    public static function apiList($page, $rows, $cid) {

        $query = Db::table('live_stock_suggest a')
            ->join('teacher t','t.id=a.teacher_id')
            ->where('a.live_channel_id','=', $cid)
            ->field('a.id, a.content, a.created, t.realname teacher_name')
            ->order('a.id','desc')
            ->page($page, $rows);

        $list = $query->select();

        $today = date('Y-m-d');
        foreach ($list as & $item) {
            $created = $item['created'];
            if (empty($created)) continue;

            if (strpos($created, $today) === 0) {
                $item['created'] = substr($created,11,5);
            } else {
                $item['created'] = substr($created,0,10);
            }
        }

        return $list;
    }
}