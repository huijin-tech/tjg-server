<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/10/31
 * Time: 18:11
 */

namespace app\common\model;


use think\Db;
use think\Model;

/**
 * Class Contract
 * @package app\common\model
 *
 * @method static static get($data, $with = [], $cache = false)
 *
 * @property integer id
 * @property string contract_no
 * @property integer bill_id
 * @property string bill_trade_no
 * @property integer user_id
 * @property integer realname_auth_id
 * @property string contract_name
 * @property string contract_file
 * @property integer contract_time
 */
class Contract extends Model
{
    protected $table = 'contract';
    protected $pk = 'id';

    /**
     * 查询最后的编号
     *
     * @return string|null
     */
    public static function getLastNo() {
        $sql = <<<SQL
SELECT max(contract_no) contract_no FROM contract
SQL;
        $re = Db::query($sql);

        return $re[0]['contract_no'] ?? null;
    }

}