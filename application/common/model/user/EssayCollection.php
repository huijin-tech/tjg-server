<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2018-04-23
 * @Time: 16:31
 * @Author: wangxuejiao
 *
 */
namespace app\common\model\user;

use think\Model;


class EssayCollection extends Model
{
    protected $table = 'user_essay_collection';

    public static function takeCount($essay_id){
        return self::where('essay_id',$essay_id)->count();
    }

    public static function is_exist($essay_id,$user_id){
        return self::where('essay_id',$essay_id)->where('userid',$user_id)->find();
    }

    public static function addOne($essay_id,$user_id){
        $sql = <<<SQL
INSERT INTO user_essay_collection (userid, essay_id) VALUES (:uid, :eid)
ON CONFLICT DO NOTHING 
SQL;
        try{
            self::execute($sql, ['uid'=>intval($user_id),'eid'=>intval($essay_id)]);
            return 1;
        }catch (Exception $ex){
            return 0;
        }

    }

    public static function remove($essay_id,$user_id){
        return self::where(['essay_id'=>$essay_id,'userid'=>$user_id])->delete();
    }


}