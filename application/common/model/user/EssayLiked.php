<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2018-04-23
 * @Time: 16:31
 * @Author: wangxuejiao
 *
 */
namespace app\common\model\user;

use app\common\lib\BaseModel;
use think\Model;


class EssayLiked extends BaseModel
{
    protected $table = 'user_essay_liked';
    //protected $pk = 'id';

    public static function takeCount($essay_id){
        return self::where('essay_id',$essay_id)->count();
    }

    public static function is_exist($essay_id,$user_id){
        return self::where('essay_id',$essay_id)->where('userid',$user_id)->find();
    }


}