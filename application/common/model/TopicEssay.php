<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/10/23
 * Time: 18:15
 */

namespace app\common\model;


use app\common\model\dynamic\DynamicFlowItem;
use app\common\model\dynamic\DynamicFlowItemTeacher;
use app\common\model\teacher\Teacher;
use app\common\utils\DateTimeUtil;
use think\Exception;
use think\Model;

/**
 * Class TopicEssay
 * @package app\common\model
 *
 * @property integer id
 * @property string title
 * @property string content
 * @property integer topic_id
 * @property string created
 *
 */
class TopicEssay extends Model
{
    protected $table = 'topic_essay';
    protected $pk = 'id';

    private static $SaveFields = [
        'topic_id',
        'title',
        'content',
    ];
    public static function getSaveFields(): array {
        return self::$SaveFields;
    }

}