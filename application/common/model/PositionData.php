<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2018-04-18
 * @Time: 11:26
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： PositionData.php
 */
namespace app\common\model;

use think\Model;

class PositionData extends Model {
    protected $table = 'position_data';
    protected $pk = 'id';

    // 推荐位
    public static $position = [
        1 => 'PC首页头部',
        2 => 'PC首页中栏',
    ];

    // 推荐类型
    const POSITION_TYPE_CONTENT = 1; // 常规内容
    const POSITION_TYPE_AD = 2; // 广告
    public static $positionType = [
        self::POSITION_TYPE_CONTENT => '内容',
        self::POSITION_TYPE_AD => '广告',
    ];

    // 推荐内容类型
    const CONTENT_TYPE_NEWS = 1;    // 文章资讯
    const CONTENT_TYPE_COURSE = 2;  // 课程
    const CONTENT_TYPE_LIVE = 3;    // 直播
    const CONTENT_TYPE_SPECIAL = 4; // 专栏
    const CONTENT_TYPE_TEACHER = 5; // 老师
    const CONTENT_TYPE_AD = 6;      // 广告
    public static $contentType = [
        self::CONTENT_TYPE_NEWS => '文章资讯',
        self::CONTENT_TYPE_COURSE => '课程',
        self::CONTENT_TYPE_LIVE => '直播',
        self::CONTENT_TYPE_SPECIAL => '专栏',
        self::CONTENT_TYPE_TEACHER => '老师',
        self::CONTENT_TYPE_AD => '广告',
    ];

    /**
     * getAll
     * 获取全部推荐位内容数据
     *
     * @param int $page
     * @param int $limit
     * @return false|static[]
     * @throws \think\exception\DbException
     */
    public static function getAll($page=0, $limit=10)
    {
        $data = self::all(function ($query) use (&$page, &$limit) {
            $query->order('id desc')
                ->page($page, $limit);
        });

        return $data;
    }

    /**
     * getPosition
     * 根据推荐位获取推荐内容数据
     *
     * @author zhengkai
     * @date 2018-04-26
     *
     * @param $pid
     * @param int $num
     * @return false|static[]
     * @throws \think\exception\DbException
     */
    public static function getPosition($pid, $num=1)
    {
        $data = self::all(function ($query) use ($pid, $num) {
            $query->where('positions', $pid);
            $query->where('del', 'false');
            $query->field('content');
            $query->order('id desc');
            $query->limit($num);
        });

        foreach ($data as &$val) {
            $val = json_decode($val['content'], true);
        }

        return $data;
    }
}