<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2018/2/26
 * Time: 10:02
 */

namespace app\common\model;


use app\common\model\conf\ConfigUserRegisterGift;
use think\Config;
use think\Db;
use think\Exception;
use think\Model;

/**
 * Class SysConfiguration
 * @package app\common\model
 *
 * @property string name
 * @property string|object config
 * @property string created
 * @property string updated
 * @property string intro
 */
class SysConfiguration extends Model
{
    protected $table = 'sys_configuration';
    protected $pk = 'name';
    protected $field = [
        'name',
        'config',
        'intro',
    ];

    const NAME_USER_REGISTER_GIFT = 'USER_REGISTER_GIFT';
    public static $NameTitles = [
        self::NAME_USER_REGISTER_GIFT   => '注册赠送',
    ];
    public static $NameClasses = [
        self::NAME_USER_REGISTER_GIFT   => ConfigUserRegisterGift::class,
    ];
    public static function newConfig($name) {
        $class = self::$NameClasses[$name] ?? null;
        if ($class) {
            return new $class();
        }
        throw new Exception('无效的name参数');
    }

    public function encodeJSON() {
        if (isset($this->config) && (is_array($this->config) || is_object($this->config)))
            $this->config = json_encode($this->config,JSON_UNESCAPED_UNICODE);
    }
    public function decodeJSON() {
        if (isset($this->config) && is_string($this->config))
            $this->config = json_decode($this->config);
    }

    public function save($data = [], $where = [], $sequence = null)
    {
        $this->encodeJSON();
        $re = parent::save($data, $where, $sequence);
        $this->decodeJSON();
        return $re;
    }

    /**
     * @param mixed $data
     * @param array $with
     * @param bool $cache
     * @return null|static
     */
    public static function get($data, $with = [], $cache = false)
    {
        $model = parent::get($data, $with, $cache);
        if ($model && $model instanceof SysConfiguration) {
            $model->decodeJSON();
        }
        return $model;
    }

    public static function list() {
        $sql = 'select name,created,updated from sys_configuration';
        $re = Db::query($sql);
        $list = [];
        foreach (self::$NameTitles as $name => $title) {
            $data = ['name'=>$name, 'title'=>$title];
            foreach ($re as $item) {
                if ($item['name'] == $name) {
                    $data['title'] = $title;
                }
            }
            $list[] = $data;
        }
        return $list;
    }

    /**
     * @return ConfigUserRegisterGift
     */
    public static function userRegisterGift() {
        $model = self::get(self::NAME_USER_REGISTER_GIFT);
        if (!$model) return false;
        $model->config;
        return new ConfigUserRegisterGift($model->config);
    }

    private static $serviceTel = null;
    public static function getServiceTel() {
        if (empty(self::$serviceTel)) {
            self::$serviceTel = Config::get('serviceTel');
        }
        return self::$serviceTel;
    }
}