<?php
/**
 * Created by PhpStorm.
 * User: wangxuejiao
 * Date: 2018/6/12
 *
 */
namespace app\common\model;

use think\Model;

/**
 * Class UserStock
 * @package app\common\model
 *
 * @property integer id
 * @property string stock_code
 * @property integer user_id
 * @property integer sort
 * @property integer create_time
 */
class UserStock extends Model
{
    protected $table = 'user_stock';
    protected $pk = 'id';

    public static function findOne(array $condition){
        return self::where($condition)->find();
    }

    public static function maxSort(){
        return self::max('sort');
    }

    public static function remove(array $condition){
        return self::where($condition)->delete();
    }

    public static function getPaginate($user_id,$filed,$rows){
        return self::field($filed)->where('user_id',$user_id)->order('sort','desc')->order('create_time','desc')->paginate($rows)->toArray();
    }
}