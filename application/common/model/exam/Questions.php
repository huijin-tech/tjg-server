<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/9/18
 * Time: 15:03
 */

namespace app\common\model\exam;


use think\Model;

class Questions extends Model
{
    protected $table = 'questions';
    protected $pk = 'questions_id';
}