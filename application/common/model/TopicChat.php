<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/10/23
 * Time: 18:16
 */

namespace app\common\model;

use app\api\model\user\Users;
use app\common\model\teacher\Teacher;
use app\common\utils\DateTimeUtil;
use think\Db;
use think\Env;
use think\Exception;
use think\Model;

/**
 * Class TopicChat
 * @package app\common\model
 *
 * @property integer id
 * @property string content
 * @property integer topic_id
 * @property integer user_id
 * @property integer reply_target_id
 * @property string created
 *
 */
class TopicChat extends Model
{
    protected $table = 'topic_chat';
    protected $pk = 'id';
    protected $field = [

    ];

    /**
     * @param int $topic_id
     * @param int $rows
     * @param int $page
     * @param bool $paged
     * @return mixed
     */
    public static function list($topic_id, $rows, $page, $paged = false) {

        $query = Db::table('topic_chat a');
        $query->join('topic_chat b','a.reply_target_id=b.id','LEFT');
        $query->join('users au','au.id=a.user_id','LEFT');
        $query->join('users bu','bu.id=b.user_id','LEFT');
        $query->where('a.topic_id','=', $topic_id);
        $query->order('a.id','desc');
        $query->field('
        a.id,
        a.content,
        a.created,
        au.id uid,
        au.mobile,
        au.nickname,
        au.avatar,
        b.id target_id,
        b.content target_content,
        b.created target_created,
        bu.id target_uid,
        bu.mobile target_mobile,
        bu.nickname target_nickname
        ');
        $query->order('a.id','desc');

        if ($paged) {
            $result = $query->paginate($rows,false,['page'=>$page])->toArray();
            self::formatAuthor($result['data'], $topic_id);
        } else {
            $result = $query->page($page,$rows)->select();
            self::formatAuthor($result, $topic_id);
        }

        return $result;
    }

    /**
     * @param $list
     * @param $topic_id
     */
    private static function formatAuthor(& $list, $topic_id) {

        $topic = Topic::get($topic_id);
        $teacher = Teacher::get($topic->teacher_id);

        foreach ($list as & $item) {

            $item['created'] = DateTimeUtil::format($item['created']);
            if ($item['target_created']) $item['target_created'] = DateTimeUtil::format($item['target_created']);

            if (empty($item['uid'])) {
                if (empty($item['nickname'])) $item['nickname'] = $teacher->realname.'老师';
                if (empty($item['avatar'])) $item['avatar'] = $teacher->avatar ? $teacher->avatar : Env::get('server.defaultAvatar');
                $item['is_host'] = true;
                $item['uid'] = $teacher->id;
            } else {
                $item['is_host'] = false;
                if (empty($item['nickname'])) $item['nickname'] = mobile_hidden($item['mobile']);
                if (empty($item['avatar'])) $item['avatar'] = Users::defaultAvatar();
            }
            unset($item['mobile']);

            if (empty($item['target_id'])) continue;

            if (empty($item['target_uid'])) {
                if (empty($item['target_nickname'])) $item['target_nickname'] = $teacher->realname.'老师';
            } else {
                if (empty($item['target_nickname'])) $item['target_nickname'] = mobile_hidden($item['target_mobile']);
            }
            unset($item['target_mobile']);

        }
    }

    /**
     * 检查用户是否参与
     *
     * @param $topic_id
     * @param $uid
     * @return bool
     */
    public static function checkUser($topic_id, $uid) {
        $sql = <<<SQL
SELECT exists(SELECT 1 FROM topic_chat WHERE user_id=:uid LIMIT 1) e
SQL;
        $re = Db::query($sql, ['uid' => $uid]);

        return $re[0]['e'] ?? false;
    }

}