<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2018/2/26
 * Time: 14:22
 */

namespace app\common\model\conf;

/**
 * Class ConfigUserRegisterGift
 * @package app\common\model\conf
 *
 * @property int type
 * @property int id
 * @property bool enable
 */
class ConfigUserRegisterGift
{
    public function __construct($config = null)
    {
        if (is_array($config)) $config = (object) $config;
        $this->type = $config->type ?? 0;
        $this->id = $config->id ?? 0;
        $this->enable = $config->enable ?? true;
    }
}