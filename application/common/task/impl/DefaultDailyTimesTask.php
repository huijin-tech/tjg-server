<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/12/20
 * Time: 17:42
 */

namespace app\common\task\impl;


use app\common\model\UserCreditRecord;
use app\common\task\AbstractCreditTask;
use think\Exception;

class DefaultDailyTimesTask extends AbstractCreditTask
{

    public function finished()
    {
        if (isset($this['maxTimes']) == false) {
            throw new Exception('Missing configuration "maxTimes"!');
        }

        $maxTimes = $this['maxTimes'];
        if (is_integer($maxTimes) == false) {
            throw new Exception('Configuration "maxTimes" type error, expect integer.');
        }

        $times = UserCreditRecord::countByDate($this['type'], $this['uid'], date('Y-m-d'));

        $this['times'] = $times;
        return $times >= $maxTimes;
    }

    public function tips()
    {
        return '您今天的任务'.$this['name'].'已完成';
    }

}