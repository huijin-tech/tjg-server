<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/12/21
 * Time: 15:43
 */

namespace app\common\task\impl;


use think\Exception;

class DefaultRebateTask extends InfiniteTask
{
    public function execute()
    {
        if ($this->params) {
            $credit = intval($this['percent'] * $this->params['bill_pay_amount']);
            if ($credit > 0) {
                $this['amount'] = $credit;
                parent::execute();
            }
        }
    }
}