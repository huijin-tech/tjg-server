<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/12/20
 * Time: 18:51
 */

namespace app\common\task\impl;


use app\common\task\AbstractCreditTask;

class InfiniteTask extends AbstractCreditTask
{
    public function finished()
    {
        return false;
    }

}