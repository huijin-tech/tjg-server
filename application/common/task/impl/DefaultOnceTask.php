<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/12/20
 * Time: 18:19
 */

namespace app\common\task\impl;


use app\common\model\UserCreditRecord;
use app\common\task\AbstractCreditTask;


class DefaultOnceTask extends AbstractCreditTask
{
    public function finished()
    {
        return UserCreditRecord::checkOnce($this['type'], $this['uid']);
    }
}