<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/12/18
 * Time: 20:31
 */

namespace app\common\task\impl;



use app\common\model\UserCreditRecord;
use app\common\task\AbstractCreditTask;

class DefaultDailyTask extends AbstractCreditTask
{

    public function finished()
    {
        return UserCreditRecord::checkByDate($this['type'], $this['uid'], date('Y-m-d'));
    }

    public function tips()
    {
        return '您今日已经'.$this['name'];
    }

}