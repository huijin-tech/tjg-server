<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/12/18
 * Time: 21:27
 */

namespace app\common\task\impl;


use app\api\model\user\Users;

class AttendanceTask extends DefaultDailyTask
{
    public function finished()
    {
        return Users::checkAttendance($this['uid'], date('Y-m-d'));
    }

    public function execute()
    {
        parent::execute();
        Users::attend($this['uid']);
    }

}