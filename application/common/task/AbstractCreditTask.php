<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/12/20
 * Time: 14:13
 */

namespace app\common\task;


use app\common\model\UserCreditRecord;
use think\Exception;

class AbstractCreditTask implements ICreditTask
{
    protected $config;
    protected $params = [];

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function finished()
    {
        throw new Exception('Method did not implemented!');
    }

    public function execute()
    {
        UserCreditRecord::record(intval($this['uid']), intval($this['type']), intval($this['amount']), strval($this['name']), $this->params);
    }

    public function tips()
    {
        return '您已经'.$this['name'];
    }

    public function setParams($params)
    {
        $this->params = $params;
    }

    public function getParams()
    {
        return $this->params;
    }

    public function offsetExists($offset)
    {
        return isset($this->config[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->config[$offset] ?? null;
    }

    public function offsetSet($offset, $value)
    {
        $this->config[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        unset($this->config[$offset]);
    }

}