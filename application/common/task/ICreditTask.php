<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/12/18
 * Time: 20:25
 */

namespace app\common\task;


interface ICreditTask extends \ArrayAccess
{

    public function finished();

    public function execute();

    public function tips();

    public function setParams($params);

    public function getParams();

}