<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

use think\Env;

return [
    // +----------------------------------------------------------------------
    // | 应用设置
    // +----------------------------------------------------------------------

    // 应用命名空间
    'app_namespace'          => 'app',
    // 应用调试模式
    'app_debug'              => Env::get('app_debug', false),
    // 应用Trace
    'app_trace'              => Env::get('app_trace', false),
    // 应用模式状态
    'app_status'             => '',
    // 是否支持多模块
    'app_multi_module'       => true,
    // 入口自动绑定模块
    'auto_bind_module'       => false,
    // 注册的根命名空间
    'root_namespace'         => [],
    // 扩展函数文件
    'extra_file_list'        => [THINK_PATH . 'helper' . EXT],
    // 默认输出类型
    'default_return_type'    => 'json',
    // 默认AJAX 数据返回格式,可选json xml ...
    'default_ajax_return'    => 'json',
    // 默认JSONP格式返回的处理方法
    'default_jsonp_handler'  => 'jsonpReturn',
    // 默认JSONP处理方法
    'var_jsonp_handler'      => 'callback',
    // 默认时区
    'default_timezone'       => 'PRC',
    // 是否开启多语言
    'lang_switch_on'         => false,
    // 默认全局过滤方法 用逗号分隔多个
    'default_filter'         => '',
    // 默认语言
    'default_lang'           => 'zh-cn',
    // 应用类库后缀
    'class_suffix'           => false,
    // 控制器类后缀
    'controller_suffix'      => false,

    // +----------------------------------------------------------------------
    // | 模块设置
    // +----------------------------------------------------------------------

    // 默认模块名
    'default_module'         => 'index',
    // 禁止访问模块
    'deny_module_list'       => ['common'],
    // 默认控制器名
    'default_controller'     => 'Index',
    // 默认操作名
    'default_action'         => 'index',
    // 默认验证器
    'default_validate'       => '',
    // 默认的空控制器名
    'empty_controller'       => 'Error',
    // 操作方法后缀
    'action_suffix'          => '',
    // 自动搜索控制器
    'controller_auto_search' => false,

    // +----------------------------------------------------------------------
    // | URL设置
    // +----------------------------------------------------------------------

    // PATHINFO变量名 用于兼容模式
    'var_pathinfo'           => 's',
    // 兼容PATH_INFO获取
    'pathinfo_fetch'         => ['ORIG_PATH_INFO', 'REDIRECT_PATH_INFO', 'REDIRECT_URL'],
    // pathinfo分隔符
    'pathinfo_depr'          => '/',
    // URL伪静态后缀
    'url_html_suffix'        => 'html',
    // URL普通方式参数 用于自动生成
    'url_common_param'       => false,
    // URL参数方式 0 按名称成对解析 1 按顺序解析
    'url_param_type'         => 0,
    // 是否开启路由
    'url_route_on'           => true,
    // 路由使用完整匹配
    'route_complete_match'   => false,
    // 路由配置文件（支持配置多个）
    'route_config_file'      => ['route'],
    // 是否强制使用路由
    'url_route_must'         => false,
    // 域名部署
    'url_domain_deploy'      => false,
    // 域名根，如thinkphp.cn
    'url_domain_root'        => '',
    // 是否自动转换URL中的控制器和操作名
    'url_convert'            => true,
    // 默认的访问控制器层
    'url_controller_layer'   => 'controller',
    // 表单请求类型伪装变量
    'var_method'             => '_method',
    // 表单ajax伪装变量
    'var_ajax'               => '_ajax',
    // 表单pjax伪装变量
    'var_pjax'               => '_pjax',
    // 是否开启请求缓存 true自动缓存 支持设置请求缓存规则
    'request_cache'          => false,
    // 请求缓存有效期
    'request_cache_expire'   => null,
    // 全局请求缓存排除规则
    'request_cache_except'   => [],

    // +----------------------------------------------------------------------
    // | 模板设置
    // +----------------------------------------------------------------------

    'template'               => [
        // 模板引擎类型 支持 php think 支持扩展
        'type'         => 'php',
        // 模板路径
        'view_path'    => '',
        // 模板后缀
        'view_suffix'  => 'php',
        // 模板文件名分隔符
        'view_depr'    => DS,
        // 模板引擎普通标签开始标记
        'tpl_begin'    => '{',
        // 模板引擎普通标签结束标记
        'tpl_end'      => '}',
        // 标签库标签开始标记
        'taglib_begin' => '{',
        // 标签库标签结束标记
        'taglib_end'   => '}',
    ],

    // 视图输出字符串内容替换
    'view_replace_str'       => [],
    // 默认跳转页面对应的模板文件
    'dispatch_success_tmpl'  => THINK_PATH . 'tpl' . DS . 'dispatch_jump.tpl',
    'dispatch_error_tmpl'    => THINK_PATH . 'tpl' . DS . 'dispatch_jump.tpl',

    // +----------------------------------------------------------------------
    // | 异常及错误设置
    // +----------------------------------------------------------------------

    // 异常页面的模板文件
    'exception_tmpl'         => THINK_PATH . 'tpl' . DS . 'think_exception.tpl',

    // 错误显示信息,非调试模式有效
    'error_message'          => '页面错误！请稍后再试～',
    // 显示错误信息
    'show_error_msg'         => false,
    // 异常处理handle类 留空使用 \think\exception\Handle
    'exception_handle'       => '',

    // +----------------------------------------------------------------------
    // | 日志设置
    // +----------------------------------------------------------------------

    'log'                    => [
        // 日志记录方式，内置 file socket 支持扩展
        'type'  => 'File',
        // 日志保存目录
        'path'  => LOG_PATH,
        // 日志记录级别
        'level' => [],
    ],

    // +----------------------------------------------------------------------
    // | Trace设置 开启 app_trace 后 有效
    // +----------------------------------------------------------------------
    'trace'                  => [
        // 内置Html Console 支持扩展
        'type' => 'Html',
    ],

    // +----------------------------------------------------------------------
    // | 缓存设置
    // +----------------------------------------------------------------------

    'cache'                  => [
        // 驱动方式
        'type'   => Env::get('cache.type','File'),
        // 缓存保存目录
        'path'   => CACHE_PATH,
        // 缓存前缀
        'prefix' => '',
        // redis地址
        'host' => Env::get('cache.host', '127.0.0.1'),
        // redis密码，如果有
        'password' => Env::get('cache.password'),
        // 缓存有效期 0表示永久缓存
        'expire' => 0,
    ],


    // +----------------------------------------------------------------------
    // | 会话设置
    // +----------------------------------------------------------------------

    'session'                => [
        'id'             => '',
        // SESSION_ID的提交变量,解决flash上传跨域
        'var_session_id' => '',
        // SESSION 前缀
        'prefix'         => 'think',
        // 驱动方式 支持redis memcache memcached
        'type'           => '',
        // 是否自动开启 SESSION
        'auto_start'     => true,
    ],

    // +----------------------------------------------------------------------
    // | Cookie设置
    // +----------------------------------------------------------------------
    'cookie'                 => [
        // cookie 名称前缀
        'prefix'    => '',
        // cookie 保存时间
        'expire'    => 0,
        // cookie 保存路径
        'path'      => '/',
        // cookie 有效域名
        'domain'    => '',
        //  cookie 启用安全传输
        'secure'    => false,
        // httponly设置
        'httponly'  => '',
        // 是否使用 setcookie
        'setcookie' => true,
    ],

    //分页配置
    'paginate' => [
        'type'      => 'bootstrap',
        'var_page'  => 'page',
        'list_rows' => 10,
    ],

    // 服务器配置
    'server' => [
        'host' => Env::get('server.host','http://localhost'),
        'charroom' => Env::get('server.defaultCharRoom','cossbow'),
        'liveNamePrefix' => Env::get('server.liveNamePrefix','test'),
        'wapHost' => Env::get('server.wapHost', 'http://wap.zqcj.net.cn'),
        'iosVipCourse' => Env::get('server.iosVipCourse', 1), // ios vip 课程列表控制
    ],
    // 网易云
    'v163' => [
        'appKey' => Env::get('v163.appKey'),
        'appSecret' => Env::get('v163.appSecret'),
        'videoTypeId' => Env::get('v163.videoTypeId'),
        'videoPresetId' => Env::get('v163.videoPresetId'),
        'accidPrefix' => Env::get('v163.accidPrefix', 'test'),
        'encryptExpired' => Env::get('v163.encryptExpired', 600),
    ],
    'v163SMS' => [
        'appKey' => Env::get('v163SMS.appKey', '842159fd7b78d86d0b80c529868a2f93'),
        'appSecret' => Env::get('v163SMS.appSecret', 'e979dc73df11'),
    ],
    // 融云
    'rongYun' => [
        'appkey' => Env::get('rongcloud.appkey', 'pwe86ga5phdt6'),
        'appSecret' => Env::get('rongcloud.appSecret', 'ZIt9AeCM2FMA6u'),
    ],
    // 阿里云
    'aliYunOSS' => [
        'AccessKeyId' => Env::get('aliyun.AccessKeyId', 'LTAIBJ8y6KtA9fbV'),
        'AccessKeySecret' => Env::get('aliyun.AccessKeySecret', 'AKvSlH63CT8bDhkL5I76I2KMdle4P7'),
        'Bucket' => Env::get('aliyun.bucket', 'tjg-customer-flow-division'),
        'EndPoint' => Env::get('aliyun.EndPoint', 'oss-cn-shanghai.aliyuncs.com'),
        'EndPointInternal' => Env::get('aliyun.EndPointInternal', 'oss-cn-shanghai-internal.aliyuncs.com'),
        'EnableDelete' => (bool) Env::get('aliyun.EnableDelete',false),
        'ServerOut' => (bool) Env::get('aliyun.ServerOut',true),
    ],
    // 极光推送
    'jiguang' => [
        'appKey' => Env::get('jiguang.appKey','f5df83defbea8d27c8732322'),
        'masterSecret' => Env::get('jiguang.masterSecret','63e65e01b8f5397a93b3263c'),
        'production' => (bool) Env::get('jiguang.production',false),
    ],

    'wordfilter' => [
        'host' => Env::get('wordfilter.host', 'http://192.168.3.10:8111')
    ],

    // 客服电话
    'serviceTel' => '18011393532',

    // 支付功能控制开关（主要针对IOS版）
    'isPay' => 0,

    'aliPay' => [
        'isSandbox' => Env::get('aliPay.isSandbox',false),
    ],

    // 通联支付
    'allinpay' => [
        'merchantId' => Env::get('allinpay.merchantId'),
        'md5Key' => Env::get('allinpay.md5Key'),
        'payServer' => Env::get('allinpay.payServer'),
        'cert' => Env::get('allinpay.certFile'),
    ],

    'proportion' =>'0.35',//推广提成比例

    //课程难度等级
    'curriculum_level' => [
        1 =>'初级',
        2 =>'中级',
        3 =>'高级'
    ],

    // 资讯配置
    'news_col' => [
        'news' => Env::get('news_col.news', 0), // 资讯
        'school' => Env::get('news_col.school', 0), // 学堂
        'teacher' => Env::get('news_col.teacher', 0), // 观点
        'defaultPic' => Env::get('news_col.defaultPic', ''),
    ],

    // 课程服务醘
    'course_serivce' => [
        'internalReferenceId' => Env::get('course_serivce.internalReferenceId', 0), // 内参(文章)栏目ID
        'researchReportId' => Env::get('course_serivce.researchReportId', 0), // 研报(文章)栏目ID
    ],

    'company_list'=>[
        "15928921315"=>"江川",
        "18683566358"=>"韦压英",
        "13678009997"=>"陈亮",
        "18781965917"=>"陈琴",
        "17602829701"=>"夏志成",
        "15708424885"=>"罗小文",
        "13540474716"=>"孙波",
        "15008416423"=>"蒲利君",
        "15692819906"=>"曾著玺",
        "17612856534"=>"马东骄",
        "18106634529"=>"赵远兴",
        "13540311659"=>"王为",
        "13540635745"=>"周梦霞",
        "13096321571"=>"奚显贵",
        "13548196106"=>"刘红英",
        "15719455912"=>"王瑞",
        "18108090966"=>"陈明",
        "18780195237"=>"雷力",
        "13540275296"=>"张运",
        "13551290216"=>"王东",
        "18728501324"=>"郭登辉",
        "13811102007"=>"张玮",
        "17628092659"=>"王雪娇",
        "15882352901"=>"宋燕",
        "18328507854"=>"何丽",
        "15001811037"=>"冯国磊",
        "13880852151"=>"李国峰",
        "18108273725"=>"张筱",
        "15882327233"=>"许甜",
        "13668255505"=>"龚玉佳",
        "15181214314"=>"陈超",
    ],
];
