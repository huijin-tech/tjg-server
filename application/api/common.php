<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2017-08-01
 * @Time: 17:56
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： common.php
 */

if (!function_exists('userToken')) {
    /**
     * userToken
     * user token生成
     *
     * @author zhengkai
     * @date 2017-08-02
     *
     * @param int $userId 用户id
     * @param string $timestamp 时间戳
     * @return string
     */
    function userToken($userId, $timestamp)
    {
        $token = md5(md5($userId.$timestamp));
        return $token;
    }
}

if (!function_exists('saveUserToken')) {
    /**
     * saveUserToken
     * 保存 user token 到缓存
     *
     * @author zhengkai
     * @date 2017-08-02
     *
     * @param $userId
     * @param $token
     * @return bool
     */
    function saveUserToken($userId, $token)
    {
        return \think\Cache::tag('user_token')->set('user_'.$userId, $token);
    }
}

