<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2017-08-11
 * @Time: 14:20
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： Curriculum.php
 */
namespace app\api\controller;

use app\admin\model\SysAdmin;
use app\common\model\ApiResponse;
use app\common\controller\Base;
use app\common\model\curriculum\CurriculumVideo;
use app\api\model\curriculum\Curriculum as CurriculumModel;
use app\common\model\NeteaseUser;
use app\common\model\VideoStorage;
use app\common\model\zds\Customer;
use app\common\model\zds\Goods;
use app\common\model\zds\Order;
use library\v163Class;
use think\Db;
use think\Log;
use think\Validate;

class Curriculum extends Base {
    protected $curriculum;

    public function __construct()
    {
        parent::__construct();

        $this->curriculum = new \app\api\model\curriculum\Curriculum();
    }

    /**
     * curriculum_system
     * 课程体系数据接口
     * @api http://xxx/api/curriculum/system
     *      page  选填 当前页数
     *      rows  选填 每页加载数据条数
     *
     * @author zhengkai
     * @date 2017-09-08
     * @todo v1.4.1废弃
     *
     * @return \think\response\Json
     */
    public function curriculum_system()
    {
        $uid = checkUserToken(true);

        // 分页参数
        $form = input('post.');
        if (!array_key_exists('page', $form) || empty($form['page'])) $form['page']=1;
        if (!array_key_exists('rows', $form) || empty($form['rows'])) $form['rows']=10;


        return $this->curriculum->get_all_curriculum($uid, $form['page'], $form['rows']);
    }

    /**
     * curriculum_system_detail
     * 课程体系详情数据接口
     * @api http://xxx/api/curriculum/system_detail
     *      curriculum_id 必填 课程id
     *
     * @author zhengkai
     * @date 2017-09-08
     * @todo v1.4.1废弃
     *
     * @return \think\response\Json
     */
    public function curriculum_system_detail()
    {
        $uid = checkUserToken(true);

        $form = input('post.');

        // 表单验证
        $validate = new Validate([
            ['curriculum_id', 'require|^\\d+$', '课程ID不能为空|参数类型不正确']
        ]);
        if(!$validate->check($form)) return json(array('code'=>1010, 'msg'=>$validate->getError(), 'data'=>null));

        return $this->curriculum->get_curriculum_detail($uid, $form['curriculum_id']);
    }

    /**
     * share_curriculum_system_detail
     * 分享页面专用（不需要登录）课程体系详情数据接口
     * @api http://xxx/api/curriculum/share_system_detail
     *      curriculum_id 必填 课程id
     * @todo v1.4.1废弃
     *
     * @author zhengkai
     * @date 2017-10-11
     *
     * @return \think\response\Json
     */
    public function share_curriculum_system_detail()
    {
        $uid = checkUserToken(false);

        $form = input('post.');

        // 表单验证
        $validate = new Validate([
            ['curriculum_id', 'require|^\\d+$', '课程ID不能为空|参数类型不正确']
        ]);
        if(!$validate->check($form)) return json(array('code'=>1010, 'msg'=>$validate->getError(), 'data'=>null));

        return $this->curriculum->get_curriculum_detail($uid, $form['curriculum_id']);
    }

    /**
     * study_progress
     * 学习进阶数据接口
     * @api http://xxx/api/curriculum/system_detail
     *      page 选填 页数
     *      rows 选填 每页加载数据条数
     * @todo v1.4.1废弃
     *
     * @return \think\response\Json
     */
    public function study_progress()
    {
        $uid = checkUserToken(true);

        // 分页参数
        $form = input('post.');
        if (!array_key_exists('page', $form) || empty($form['page'])) $form['page']=1;
        if (!array_key_exists('rows', $form) || empty($form['rows'])) $form['rows']=10;

        return $this->curriculum->get_buy_video($uid, $form['page'], $form['rows']);
    }

    /**
     * curriculum_detail_data
     * 课程首页详细数据 数据接口
     * @api http://xxx/api/curriculum/detail_data
     *
     * @return \think\response\Json
     */
    public function curriculum_detail_data()
    {
        $uid = checkUserToken(true);

        return $this->curriculum->get_buy_curriculum($uid);
    }

    /**
     * curriculum_detail_data_list
     * 课程首页详细数据详情 数据接口
     * @api http://xxx/api/curriculum/detail_data_list
     *      curriculum_id 必填 课程id
     *
     * @todo v1.4.1废弃
     *
     * @return \think\response\Json
     */
    public function curriculum_detail_data_list()
    {
        $uid = checkUserToken(true);

        $form = input('post.');

        // 表单验证
        $validate = new Validate([
            ['curriculum_id', 'require|^\\d+$', '课程ID不能为空|参数类型不正确']
        ]);
        if(!$validate->check($form)) return json(array('code'=>1010, 'msg'=>$validate->getError(), 'data'=>null));

        return $this->curriculum->get_buy_curriculum_detail($uid, $form['curriculum_id']);
    }

    /**
     * curriculum_video_detail
     * 课程视频详情数据接口
     * @api http://xxx/api/curriculum/video_detail
     *      video_id 必填 视频id
     *
     * @todo v1.4.1废弃
     *
     * @return \think\response\Json
     */
    public function curriculum_video_detail()
    {
        $uid = checkUserToken(false);

        $form = input('post.');

        // 表单验证
        $validate = new Validate([
            ['video_id', 'require|^\\d+$', '视频ID不能为空|参数类型不正确']
        ]);
        if(!$validate->check($form)) return json(array('code'=>1010, 'msg'=>$validate->getError(), 'data'=>null));

        return $this->curriculum->get_video_detail($form['video_id'], $uid);
    }

    /**
     * curriculum_related_video
     * 相关课程视频数据接口
     * @api http://xxx/api/curriculum/related_video
     *      video_id 必填 视频id
     *
     * @todo v1.4.1废弃
     *
     * @author zhengkai
     * @date 2017-09-19
     *
     * @return array|\think\response\Json
     */

    public function curriculum_related_video()
    {
        $form = input('post.');

        // 表单验证
        $validate = new Validate([
            ['video_id', 'require|^\\d+$', '视频ID不能为空|参数类型不正确']
        ]);
        if(!$validate->check($form)) return json(array('code'=>1010, 'msg'=>$validate->getError(), 'data'=>null));

        // 分页参数
        if (!array_key_exists('page', $form) || empty($form['page'])) $form['page']=1;
        if (!array_key_exists('rows', $form) || empty($form['rows'])) $form['rows']=10;

        return $this->curriculum->get_related_video($form['video_id'], $form['page'], $form['rows']);
    }

    /**
     * curriculum_free_video
     * 免费课程视频数据接口
     * @api http://xxx/api/curriculum/free_video
     *      page 选填 当前页数
     *      rows 选填 每页加载数据条数
     *
     * @todo v1.4.1废弃
     *
     * @author zhengkai
     * @date 2017-09-08
     *
     * @return \think\response\Json
     */
    public function curriculum_free_video()
    {
        // 分页参数
        $form = input('post.');
        if (!array_key_exists('page', $form) || empty($form['page'])) $form['page']=1;
        if (!array_key_exists('rows', $form) || empty($form['rows'])) $form['rows']=10;

        return $this->curriculum->get_free_video($form['page'], $form['rows']);
    }

    /**
     * curriculum_class
     * 课程分类数据接口
     * @api http://xxx/api/curriculum/class
     *
     * @author zhengkai
     * @date 2017-09-08
     *
     * @todo v1.4.1废弃
     *
     * @return \think\response\Json
     */
    public function curriculum_class()
    {
        return $this->curriculum->get_curriculum_class();
    }

    /**
     * wechat_recommend_video
     * 微信精选课程视频(首页大咖学)数据接口
     * @api http://xxx/api/wechat/recommend_video
     *      num 选填 需要获取数量的条数，默认为2
     *
     * @author zhengkai
     * @date 2017-08-18
     *
     * @return mixed
     */
    public function recommend_video()
    {
        $videos = $this->curriculum->get_recommend_video();

        return ApiResponse::success($videos);
    }

    /**
     * save_comment
     * 课程评价提交接口
     * @api http://xxx/api/curriculum/save_comment
     *      curriculum_id 必填 被评价课程ID
     *      video_id 必填 被评价课程视频ID // TODO 在v1.1中取消，仅在v1.0中支持
     *      comment_content 必填 评价内容
     *
     * @author zhengkai
     * @date 2017-09-08
     * @update zhengkai 2017-11-08
     *      升级适用于v1.1
     *
     * @return \think\response\Json
     */
    public function save_comment()
    {
        $uid = checkUserToken(true);

        $form = input('post.');

        $form['video_id']=0; // 此参数v1.1废弃，为考虑v1.0的兼容问题临时设置调用

        // 表单验证
        $validate = new Validate([
            ['curriculum_id', 'require|^\\d+$', '课程id不能为空|参数类型不正确'],
            ['video_id', '^\\d+$', '参数类型不正确'], // TODO 仅在v1.0中支持
            ['comment_content', 'require|max:200', '评论内容不能为空|评论内容最长不能超过200个字'],
            ['comment_score', '^\\d+$', '参数类型不正确'],
        ]);
        $validate->scene('form', ['comment_curriculum', 'comment_video', 'comment_content']);
        if(!$validate->check($form)) return json(array('code'=>1010, 'msg'=>$validate->getError(), 'data'=>null));

        if (!array_key_exists('video_id', $form) || empty($form['video_id'])) $form['video_id']=0;

        return $this->curriculum->insert_comment($uid, $form['curriculum_id'], $form['video_id'], $form['comment_content'], $form['comment_score']);
    }


    /**
     * comment_list
     * 课程评价数据接口
     * @api http://xxx/api/curriculum/comment_list
     *      curriculum_id 必填 课程id
     *      video_id 必填 课程视频id // TODO 在v1.1中取消，仅在v1.0中支持
     *      page  选填 当前页数 默认1
     *      rows  选填 每页加载数据条数 默认10
     *
     * @author zhengkai
     * @date 2017-08-14
     *
     * @return \think\response\Json
     */
    public function comment_list()
    {
        $form = input('post.');

        // 表单验证
        $validate = new Validate([
            ['curriculum_id', 'require|^\\d+$', '课程id不能为空|参数类型不正确'],
            ['video_id', '^\\d+$', '参数类型不正确'], // TODO 仅在v1.0中支持
        ]);
        if(!$validate->check($form)) return json(array('code'=>1010, 'msg'=>$validate->getError(), 'data'=>null));

        if (!array_key_exists('video_id', $form) || empty($form['video_id'])) $form['video_id']=0;
        if (!array_key_exists('page', $form) || empty($form['page'])) $form['page']=1;
        if (!array_key_exists('rows', $form) || empty($form['rows'])) $form['rows']=10;

        return $this->curriculum->get_comment($form['curriculum_id'], $form['video_id'], $form['page'], $form['rows']);
    }

    /**
     * post_bill
     * 课程晒单数据接口
     * @api http://xxx/api/curriculum/post_bill
     *      curriculum_id 必填 课程id
     *      page  选填 当前页数
     *      rows  选填 每页加载数据条数
     *
     * @author zhengkai
     * @date 2017-08-21
     *
     * @return array|\think\response\Json
     */
    public function post_bill()
    {
        $form = input('post.');

        // 表单验证
        $validate = new Validate([
            ['curriculum_id', 'require|^\\d+$', '课程id不能为空|参数类型不正确'],
            ['video_id', 'require|^\\d+$', '课程视频id不能为空|参数类型不正确'],
        ]);
        if(!$validate->check($form)) return json(array('code'=>1010, 'msg'=>$validate->getError(), 'data'=>null));

        if (!array_key_exists('page', $form) || empty($form['page'])) $form['page']=1;
        if (!array_key_exists('rows', $form) || empty($form['rows'])) $form['rows']=10;

        return $this->curriculum->get_post_bill($form['curriculum_id'], $form['video_id'], $form['page'], $form['rows']);
    }

    /**
     * curriculum_unlock
     * 已购课程解锁数据接口
     * @api http://xxx/api/curriculum/unlock
     *      curriculum_id 必填 课程ID
     *      curriculum_level 必填 需要解锁的课程视频难度等级
     *
     * @author zhengkai
     * @date 2017-09-11
     *
     * @return \think\response\Json
     */
    public function curriculum_unlock()
    {
        $uid = checkUserToken(true);

        $form = input('post.');

        // 表单验证
        $validate = new Validate([
            ['curriculum_id', 'require|^\\d+$', '课程ID不能为空|参数类型不正确'],
            ['curriculum_level', 'require|^\\d+$', '课程难度等级不能为空|参数类型不正确']
        ]);
        if(!$validate->check($form)) return json(array('code'=>1010, 'msg'=>$validate->getError(), 'data'=>null));

        return $this->curriculum->update_buy_curriculum_unlock($uid, $form['curriculum_id'], $form['curriculum_level']);

    }

    /**
     * update_study_status
     * 用户已购买课程视频学习状态更新接口
     * @api http://xxx/api/curriculum/update_study
     *      curriculum_id 必填 课程id
     *      video_id    必填 视频id
     *      curriculum_level  必填  课程视频难度等级
     *
     * @author zhengkai
     * @date 2017-09-11
     *
     * @return \think\response\Json
     */
    public function update_study_status()
    {
        $uid = checkUserToken(true);

        $form = input('post.');

        // 表单验证
        $validate = new Validate([
            ['curriculum_id', 'require|^\\d+$', '课程ID不能为空|参数类型不正确'],
            ['video_id', 'require|^\\d+$', '视频ID不能为空|参数类型不正确'],
            ['curriculum_level', 'require|^\\d+$', '课程难度等级不能为空|参数类型不正确']
        ]);
        if(!$validate->check($form)) return json(array('code'=>1010, 'msg'=>$validate->getError(), 'data'=>null));

        return $this->curriculum->update_buy_curriculum_video_study_status($uid, $form['curriculum_id'], $form['video_id'], $form['curriculum_level']);
    }

    /**
     * 统计视频的点击和播放
     *
     * @return ApiResponse
     */
    public function statistics_play() {
        $form = $this->getAndCheckForm([
            ['video_id', 'require|integer',],
            ['finish', 'integer',],
        ]);

        $finishField = empty($form['finish']) ? 'statistics_play' : 'statistics_play_finish';
        $sql = <<<SQL
UPDATE curriculum_video SET {$finishField}={$finishField}+1 
WHERE video_id=:vid
SQL;
        Db::query($sql, ['vid' => $form['video_id']]);

        return ApiResponse::success();
    }

    /**
     * curriculum_new
     * 最新课程数据接口
     * @api http://xxx/api/curriculum/new
     *      num 选填 加载数据条数，默认6
     *
     * @author zhengkai
     * @date 2017-11-07
     * @version 1.1
     *
     * @return \think\response\Json
     */
    public function curriculum_new()
    {
        $form = input('post.');
        if (!array_key_exists('num', $form) || empty($form['num'])) $form['num']=6;

        return $this->curriculum->queryNewCurriculum($form['num']);
    }

    /**
     * filter_field
     * 课程筛选条件数据接口
     * @api http://xxx/api/curriculum/filter
     *
     * @author zhengkai
     * @date 2017-11-07
     * @version 1.1
     *
     * @return \think\response\Json
     */
    public function filter_field()
    {
        return $this->curriculum->filterCurriculumCondition();
    }

    /**
     * curriculum
     * 课程列表数据接口
     * @api http://xxx/api/curriculum
     *      GET|POST
     *          rows    选填  每页显示数据条数，默认10
     *          page    选填  当前第x页，默认1
     *
     *          key     选填  搜索关键字
     *          type    选填  课程类型
     *          class   选填  课程分类
     *          teacher 选填  老师
     *          sort    排序
     *
     * @author zhengkai
     * @date 2017-11-07
     * @version 1.1
     *
     * @return \think\response\Json
     */
    public function curriculum()
    {
        $uid = checkUserToken(false);

        // 分页参数
        $form = input('');
        if (!array_key_exists('page', $form) || empty($form['page'])) $form['page']=1;
        if (!array_key_exists('rows', $form) || empty($form['rows'])) $form['rows']=10;

        return $this->curriculum->queryCurriculum($uid, $form);
    }

    /**
     * curriculum
     * 课程列表数据接口
     * @api http://xxx/api/curriculum
     *      GET|POST
     *          rows    选填  每页显示数据条数，默认10
     *          page    选填  当前第x页，默认1
     *
     *          key     选填  搜索关键字
     *          type    选填  课程类型
     *          class   选填  课程分类
     *          teacher 选填  老师
     *          sort    排序
     *
     * @author wangxuejiao
     * @date 2018-04-16
     * @version pc
     *
     * @return \think\response\Json
     */
    public function curriculumWeb()
    {
        $uid = checkUserToken(false);
        // 分页参数
        $form = input();
        $form['page']=$form['page']??1;
        $form['page']=$form['rows']??10;
        return ApiResponse::success($this->curriculum->queryCurriculumNew($uid, $form['rows']));
    }

    /**
     * curriculum_detail
     * 课程详情数据接口
     * @api http://xxx/api/curriculum/detail
     *      GET|POST
     *          curriculum_id 必填 课程id
     *
     * @author zhengkai
     * @date 2017-11-08
     * @version 1.1
     *
     * @return \think\response\Json
     */
    public function curriculum_detail()
    {
        $uid = checkUserToken(false);

        $form = input();

        // 表单验证
        $validate = new Validate([
            ['curriculum_id', 'require|^\\d+$', '课程ID不能为空|参数类型不正确']
        ]);
        if(!$validate->check($form)) return json(array('code'=>1010, 'msg'=>$validate->getError(), 'data'=>null));

        return $this->curriculum->getCurriculumDetail($uid, $form['curriculum_id']);
    }

    /**
     * curriculum_video
     * 课程视频(列表)数据接口
     * @api http://xxx/api/curriculum/video
     *      curriculum_id 必填 课程id
     *
     * @author zhengkai
     * @date 2017-11-08
     * @version 1.1
     *
     * @return \think\response\Json
     */
    public function curriculum_video()
    {
        $uid = checkUserToken(false);

        $form = input('post.');

        // 表单验证
        $validate = new Validate([
            ['curriculum_id', 'require|^\\d+$', '课程ID不能为空|参数类型不正确']
        ]);
        if(!$validate->check($form)) return json(array('code'=>1010, 'msg'=>$validate->getError(), 'data'=>null));

        return $this->curriculum->getCurriculumVideo($uid, $form['curriculum_id']);
    }

    /**
     * video_player
     * 单个课程视频(播放)数据接口
     * @api http://xxx/api/curriculum/video/player
     *      video_id 必填 视频id
     *
     * @author zhengkai
     * @date 2017-11-08
     * @version 1.1
     *
     * @return \think\response\Json
     */
    public function video_player()
    {
        $uid = checkUserToken(true);

        $form = input('post.');

        // 表单验证
        $validate = new Validate([
            ['video_id', 'require|^\\d+$', '视频ID不能为空|参数类型不正确']
        ]);
        if(!$validate->check($form)) return json(array('code'=>1010, 'msg'=>$validate->getError(), 'data'=>null));

        return $this->curriculum->getSingleVideo($uid, $form['video_id']);
    }

    /**
     * curriculum_recommend
     * 推荐课程数据接口
     * @api http://xxx/api/curriculum/recommend
     *      num 选填 需要获取的数据条数，默认4
     *
     * @author zhengkai
     * @date 2017-11-06
     * @version v1.1
     *
     * @return \think\response\Json
     */
    public function curriculum_recommend()
    {
        $form = input('post.');
        if (!array_key_exists('num', $form) || empty($form['num'])) $form['num']=4;

        return $this->curriculum->queryRecommendCurriculum($form['num']);
    }

    /**
     * curriculum_teacher
     * 老师课程数据接口
     * @api http://xxx/api/curriculum/teacher
     *      GET|POST
     *          teacher_id  必填  老师id
     *          page        选填  当前第x页，默认1
     *          rows        选填  每页加载数据条数，默认10
     *
     * @author zhengkai
     * @date 2017-11-09
     * @version 1.1
     *
     * @return \think\response\Json
     */
    public function curriculum_teacher()
    {
        $uid = checkUserToken(false);
        $form = input();

        // 表单验证
        $validate = new Validate([
            ['teacher_id', 'require|^\\d+$', '老师id不能为空|参数类型不正确']
        ]);
        if(!$validate->check($form)) return json(array('code'=>1010, 'msg'=>$validate->getError(), 'data'=>null));

        if (!array_key_exists('curriculum_type', $form) || empty($form['curriculum_type'])) $form['curriculum_type']=0;
        if (!array_key_exists('page', $form) || empty($form['page'])) $form['page']=1;
        if (!array_key_exists('rows', $form) || empty($form['rows'])) $form['rows']=10;

        return $this->curriculum->getTeacherCurriculum($uid, $form['teacher_id'], $form['curriculum_type'], $form['page'], $form['rows']);
    }

    /**
     * web_curriculum_post_bill
     * web课程首页学员晒单数据接口
     * @api http://xxx/api/web/curriculum/post_bill
     *      POST
     *          num 选填 获取数据条数，默认4
     *
     * @author zhengkai
     * @date 2017-11-15
     * @version 1.1
     *
     * @return \think\response\Json
     * TODO 晒单人
     */
    public function web_curriculum_post_bill()
    {
        $form = input('post.');
        if (!array_key_exists('num', $form) || empty($form['num'])) $form['num']=4;

        return $this->curriculum->getNewPostBill($form['num']);
    }

    /**
     * web_curriculum_comment
     * web课程首页课程学员评价数据接口
     * @api http://xxx/api/web/curriculum/comment
     *      POST
     *          num 选填 获取数据条数，默认4
     *
     * @author zhengkai
     * @date 2017-11-16
     * @version 1.1
     *
     * @return \think\response\Json
     */
    public function web_curriculum_comment()
    {
        $form = input('post.');
        if (!array_key_exists('num', $form) || empty($form['num'])) $form['num']=4;

        return $this->curriculum->getNewComment($form['num']);
    }

    /**
     * web_curriculum_player
     * web课程视频播放数据接口
     * @api http://xxx/api/web/curriculum/palyer
     *      POST
     *          video_id 必填 课程视频id
     *
     * @author zhengkai
     * @date 2017-11-16
     * @version 1.1
     *
     * @return \think\response\Json
     */
    public function web_curriculum_player()
    {
        $uid = checkUserToken(true);

        $form = input('post.');

        // 表单验证
        $validate = new Validate([
            ['video_id', 'require|^\\d+$', '视频ID不能为空|参数类型不正确']
        ]);
        if(!$validate->check($form)) return json(array('code'=>1010, 'msg'=>$validate->getError(), 'data'=>null));

        return $this->curriculum->getSingleVideoAndList($uid, $form['video_id']);
    }

    /**
     * web_curriculum_player_count
     * web课程视频播放学习记录数据处理与视频播放统计
     * @api http://xxx/api/web/curriculum/count
     *      POST
     *          video_id 必填 视频id
     *
     * @author zhengkai
     * @date 2017-11-16
     * @version 1.1
     *
     * @return \think\response\Json
     */
    public function web_curriculum_player_count()
    {
        $uid = checkUserToken(false);

        $form = input('post.');

        // 表单验证
        $validate = new Validate([
            ['video_id', 'require|^\\d+$', '视频ID不能为空|参数类型不正确']
        ]);
        if(!$validate->check($form)) return json(array('code'=>1010, 'msg'=>$validate->getError(), 'data'=>null));

        return $this->curriculum->studyLogAndPlayerCount($uid, $form['video_id']);
    }

    public function credit_curriculum() {

        $uid = checkUserToken();

        $form = $this->getAndCheckForm([
            ['rows', 'integer',],
            ['page', 'integer',],
        ]);

        $rows = intval($form['rows'] ?? 10);
        $result = CurriculumModel::queryCreditEnable($form['page']??1,$rows,$this->isPC(),$uid);
        return ApiResponse::success($result['list'], $result['total']??null,$rows);
    }

    /**
     * 获取视频播放信息，地址和解密token
     *
     * @return ApiResponse
     */
    public function video_play_src() {

        $uid = checkUserToken(true);

        $form = $this->getAndCheckForm([
            ['video_id', 'require|integer',],
            ['video_style', 'integer',],
        ]);
        $video_style = intval($form['video_style']??1);
        $style = v163Class::getStyle($video_style,'flv');
        if (0 == $style) ApiResponse::error(ApiResponse::ERR_FORM_INVALID,'video_style取值不正确');

        $video = CurriculumVideo::get($form['video_id']);
        if (empty($video)) ApiResponse::error(ApiResponse::ERR_OPERATE_FAILED,'课程视频不存在');
        // todo 判断是否解锁、购买或免费
        if (SysAdmin::checkAdminUser($uid)) {
        } else {
            if (CurriculumModel::isOpen($video->video_curriculum) || $video->isFree() || $video->preview_allowed) {
            } else {
                if (CurriculumModel::haveBought($uid, $video->video_curriculum) == false) {
                    ApiResponse::error(ApiResponse::ERR_NO_PURCHASE,'您还未购买该视频');
                }
            }
        }

        $encrypt = VideoStorage::getEncryptUrl($video->vid, $style);
        if (empty($encrypt)) ApiResponse::error(ApiResponse::ERR_OPERATE_FAILED,'视频未准备好');
        $v163 = v163Class::getInstance();
        try{
            $re = $v163->vod_encrypt_token($encrypt['encrypt_id']);
        }catch (\Exception $ex){
            Log::error('获取视频解密token失败：vid='.$video->vid.', '.$ex->getMessage());
            return ApiResponse::error(ApiResponse::ERR_OPERATE_FAILED,'视频未准备好');
        }

        if ($uid > 0) $nu = NeteaseUser::getUser($uid);
        else $nu = NeteaseUser::defaultUser();

        $data = [
            'type' => 'video/x-flv',
            'src' => $encrypt['play_url'],
            'decryptInfo' => [
                'transferToken' => $re['transferToken'] ?? null,
                'accid' => $nu['accid'],
                'token' => $nu['token'],
                'appKey' => $v163->getAppKey(),
            ],
            'headers' => $re['headers'],
            'body' => $re['body'],
        ];

        return ApiResponse::success($data);
    }

    /**
     * vip_course
     * 当前登录用户的vip课程列表
     * @api POST http://xxx/api/curriculum/vip
     *      page    选填 当前页数，默认1
     *      limit   选填  每页加载数据，默认10
     *
     * @author zhengkai
     * @version 1.4.1
     * @date 2018-04-03
     * @update 2018-04-10 wangxuejiao
     *      增加数据筛选条件
     *
     * @return ApiResponse
     */
    public function vip_course()
    {
        $uid = checkUserToken(true);

        // 检查当前登录用户是否为vip(销售系统客户)
        $vipUser = Customer::queryIsExist($uid);
        if (!$vipUser) return ApiResponse::error(ApiResponse::ERR_NOt_VIP, '抱歉！您还不是VIP客户，请与客户专员联系。');

        $form = $this->getAndCheckForm([
            ['page', 'integer', '参数类型不正确'],
            ['limit', 'integer', '参数类型不正确'],
        ]);

        $form['page']=isset($form['page']) && $form['page']>0 ? intval($form['page']):1;
        $form['limit']=isset($form['limit']) && $form['limit']>0 ? intval($form['limit']):10;
        $result =  Goods::queryVipCurriculum($uid, $form['limit'], $form['page']);

        return ApiResponse::success($result);
    }

    /**
     * iosVIPList
     * 课程列表数据接口
     * IOS专用
     * @api http://xxx/api/curriculum/vlists
     *      GET|POST
     *          rows    选填  每页显示数据条数，默认10
     *          page    选填  当前第x页，默认1
     *
     *          key     选填  搜索关键字
     *          type    选填  课程类型
     *          class   选填  课程分类
     *          teacher 选填  老师
     *          sort    排序
     *
     * @author zhengkai
     * @date 2018-04-19
     * @version 1.4.1
     *
     * @return \think\response\Json
     */
    public function iosVIPList()
    {
        $uid = checkUserToken(true);

        $mode = config('server.iosVipCourse');
        switch ($mode) {
            case 1:
                $_GET['type']=2;
                return $this->curriculum();
                break;
            case 2:
                return $this->vip_course();
                break;
        }
    }

    /**
     * pc_index
     * pc首页课程
     *
     * @author zhengkai
     * @date 2018-04-23
     *
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function pc_index()
    {
        $free = \app\api\model\curriculum\Curriculum::getHotCourse(2);
        //modify cc 2018-10-15 修改获取vip课程type=3(原来是高级课程 type=1)
        $premium = \app\api\model\curriculum\Curriculum::getHotCourse(3);

        $result = [
            'free' => $free,
            'premium' => $premium
        ];

        return $result;
    }
}
