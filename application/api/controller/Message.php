<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2018/2/27
 * Time: 14:51
 */

namespace app\api\controller;


use app\common\controller\Base;
use app\common\model\ApiResponse;
use app\common\model\UserNoticeMessage;

class Message extends Base
{
    private $uid;
    protected function _initialize()
    {
        $this->uid = checkUserToken(true);
    }

    public function main() {
        $totals = UserNoticeMessage::myTotal($this->uid);
        return ApiResponse::success(['totals'=>$totals]);
    }

    public function my_list() {

        $form = $this->getAndCheckForm([
            ['rows', 'integer',],
            ['page', 'integer',],
            ['type', '^[1-2]$',],
        ]);

        $p = self::pageFormat($form);
        $list = UserNoticeMessage::myList($this->uid, $form['type']??null, $p['limit'], $p['offset']);

        return ApiResponse::success($list);
    }

    public function set_read() {
        $form = $this->getAndCheckForm([
            ['id','integer'],
        ]);
        if (empty($form['id'])) UserNoticeMessage::setRead($this->uid);
        else UserNoticeMessage::read($this->uid, $form['id']);
        return ApiResponse::success();
    }

}