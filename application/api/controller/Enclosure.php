<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2018/8/16
 * Time: 17:22
 */

namespace app\api\controller;


use app\admin\model\CourseEnclosure;
use app\common\controller\Base;
use app\common\model\ApiResponse;

class Enclosure extends Base
{
    public function list(){
        $form = $this->getAndCheckForm([
            ['page', 'integer',],
            ['rows', 'integer',],
            ['curriculum_id', 'require|integer',],
            ['course_service_id', 'require|integer',],
        ]);
        $form['page']=$form['page']??1;
        $form['rows']=$form['rows']??10;
        if($form['curriculum_id']>0){
            $condition['curriculum_id']=$form['curriculum_id'];
        }
        if($form['course_service_id']>0){
            $condition['course_service_id']=$form['course_service_id'];
        }
        $res=CourseEnclosure::getApiPage($form['rows'],$condition);
        $oneM=1048576;
        foreach($res['data'] as &$item){
            $item['create_time']=date('Y-m-d',strtotime($item['create_time']));
            $item['size']=$item['size']>$oneM ? sprintf("%.2f", $item['size']/$oneM).'M' :sprintf("%.2f", $item['size']/1024).'K';
        }
        return ApiResponse::success($res);
    }

}