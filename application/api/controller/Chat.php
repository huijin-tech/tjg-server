<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/8/2
 * Time: 10:24
 */

namespace app\api\controller;


use app\api\model\user\Users;
use app\common\controller\Base;
use app\common\model\ApiResponse;
use app\common\model\teacher\Teacher;
use think\Db;
use think\Request;

class Chat extends Base
{
    /**
     * 获取聊天7的登录token，需要登录
     *
     * @param Request $request
     * @return ApiResponse
     */
    public function token(Request $request) {

        $uid = checkUserToken();

        $user = Users::get($uid);
        if (empty($user)) $userData = ['id' => md5($request->ip()), 'nickname' => '来宾', 'avatar' => Users::defaultAvatar()];
        else $userData = $user->toArray();

        $chat = Users::chatToken($userData, true);

        return ApiResponse::success($chat);
    }

    /**
     * 聊天窗口获取用户信息接口
     *
     * @return ApiResponse
     */
    public function get_chat_user_info() {

        $id = input('post.id');

        $info = \app\common\model\Chat::getUserInfo($id);
        if ($info) return ApiResponse::success($info);
        else return ApiResponse::error(ApiResponse::ERR_OPERATE_FAILED,'用户不存在');
    }


}