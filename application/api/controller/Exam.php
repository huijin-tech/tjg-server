<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2017-09-22
 * @Time: 15:38
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： Exam.php
 */
namespace app\api\controller;

use app\common\controller\Base;
use think\Validate;

class Exam extends Base {
    protected $exam;

    public function __construct()
    {
        parent::__construct();

        $this->exam = new \app\api\model\curriculum\Exam();
    }

    /**
     * video_exam
     * 课程视频测试题数据接口
     * @api http://xxx/api/exam/questions
     *      curriculum_id 必填 课程id
     *      video_id 必填 视频id
     *
     * @author zhengkai
     * @date 2017-09-22
     *
     * @return \think\response\Json
     */
    public function video_exam()
    {
        $uid = checkUserToken(true);

        $form = input('post.');

        // 表单验证
        $validate = new Validate([
            ['curriculum_id', 'require|^\\d+$', '课程ID不能为空|参数类型不正确'],
            ['video_id', 'require|^\\d+$', '视频ID不能为空|参数类型不正确']
        ]);
        if(!$validate->check($form)) return json(array('code'=>1010, 'msg'=>$validate->getError(), 'data'=>null));

        return $this->exam->get_video_questions($uid, $form['curriculum_id'], $form['video_id']);
    }

    /**
     * action_answer
     * 课程视频测试提交请求
     * @api http://xxx/api/exam/submit
     *      answer_data 必填 json字符串
     *      {
     *          "exam_id": 3,
     *          "exam_num": 7,
     *          "curriculum_id": 4,
     *          "video_id": 20,
     *          "questions": [
     *              {
     *              "questions_id": 2,
     *              "questions_answer": "A"
     *              },
     *              ... ...
     *          ]
     *       }
     *
     * @return \think\response\Json
     */
    public function action_answer()
    {
        $uid = checkUserToken(true);

        $form = input('post.');

        // 表单验证
        $validate = new Validate([
            ['answer_data', 'require', '提交参数不能为空'],
        ]);
        if(!$validate->check($form)) return json(array('code'=>1010, 'msg'=>$validate->getError(), 'data'=>null));

        return $this->exam->insert_questions_answer($uid, $form['answer_data']);
    }
}