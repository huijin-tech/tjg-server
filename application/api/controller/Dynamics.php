<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2018-05-10
 * @Time: 15:40
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： Dynamic.php
 */
namespace app\api\controller;

use app\api\model\dynamic\DynamicComment;
use app\api\model\dynamic\DynamicLike;
use app\api\model\dynamic\DynamicM;
use app\common\controller\Base;
use app\common\model\ApiResponse;

class Dynamics extends Base {
    /**
     * lists
     * 动态(掌乾看盘)列表
     * @api http://xxx/api/dynamics/lists
     *      page 选填 当前页数，默认1
     *      limit 选填 每页加载条数，默认10
     *
     * @author zhengkai
     * @date 2018-05-10
     * @version 1.5
     *
     * @return ApiResponse
     * @throws \think\exception\DbException
     */
    public function lists()
    {
        $uid = checkUserToken(false);

        $form = $this->getAndCheckForm([
            ['page', 'integer', '参数类型不正确'],
            ['limit', 'integer', '参数类型不正确'],
        ]);

        if (!isset($form['page'])) $form['page'] = 1;
        if (!isset($form['limit'])) $form['limit'] = 10;

        $result = DynamicM::getAllDynamic(DynamicM::TYPE_KANPAN, $uid, intval($form['page']), intval($form['limit']));

        return ApiResponse::success($result);
    }

    /**
     * comment_list
     * 动态留言列表
     * @api http://xxx/api/dynamics/comment_list
     *      id 必填 动态id
     *      page 选填 当前页数，默认1
     *      limit 选填 每页加载条数，默认10
     *
     * @author zhengkai
     * @date 2018-05-10
     * @version 1.5
     *
     * @return ApiResponse
     */
    public function comment_list()
    {
        $form = $this->getAndCheckForm([
            ['id', 'require|integer', '动态id不能为空|参数类型不正确'],
            ['page', 'integer', '参数类型不正确'],
            ['limit', 'integer', '参数类型不正确'],
        ]);

        if (!isset($form['page'])) $form['page'] = 1;
        if (!isset($form['limit'])) $form['limit'] = 10;

        $result = DynamicComment::getAllComment(intval($form['id']), (intval($form['page'])), intval($form['limit']));

        return ApiResponse::success($result);
    }

    /**
     * article_message_commit
     * 掌乾动态评论提交接口
     * @api http://xxx/api/dynamics/comment_commit
 *          id 必填 动态id
 *          content 必填 评论内容
     *
     * @author zhengkai
     * @date 2018-05-10
     * @version 1.5
     *
     * @return ApiResponse
     */
    public function comment_commit()
    {
        $uid = checkUserToken(true);

        $form = $this->getAndCheckForm([
            ['id', 'require|integer', '动态id不能为空|参数类型不正确'],
            ['content', 'require|max:200', '评论内容不能为空|评论内容最多允许200个字符'],
        ]);

        $data = [
            'dynamic_id' => $form['id'],
            'user_id' => $uid,
            'content' => $form['content'],
            'times' => time()
        ];
        $result = DynamicComment::createComment($data);

        if ($result) {
            return ApiResponse::success();
        } else {
            return ApiResponse::error(ApiResponse::ERR_OPERATE_FAILED, 'fail');
        }
    }

    /**
     * detail
     * 动态详情
     * @api http://xxx/api/dynamics/detail
     *      id 必填 动态id
     *
     * @author zhengkai
     * @date 2018-05-10
     * @version 1.5
     *
     * @return ApiResponse
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function detail()
    {
        $uid = checkUserToken(false);

        $form = $this->getAndCheckForm([
            ['id', 'require|integer', '动态id不能为空|参数类型不正确'],
        ]);

        $result = DynamicM::getSingleDynamic(intval($form['id']), $uid);

        return ApiResponse::success($result);
    }

    /**
     * liked
     * 动态点赞
     * @api http://xxx/api/dynamics/liked
     *
     * @author zhengkai
     * @date 2018-05-10
     * @version 1.5
     *
     * @return ApiResponse
     */
    public function liked()
    {
        $uid = checkUserToken(true);

        $form = $this->getAndCheckForm([
            ['id', 'require|integer', '动态id不能为空|参数类型不正确'],
        ]);

        $result = DynamicLike::createLiked(intval($form['id']), $uid);

        if ($result) {
            return ApiResponse::success();
        } else {
            return ApiResponse::error(ApiResponse::ERR_OPERATE_FAILED, 'fail');
        }
    }
}