<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2017-08-08
 * @Time: 11:19
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： Sms.php
 */
namespace app\api\controller;

use app\common\controller\Base;
use library\ServerAPI;
use think\Validate;

class Sms extends Base {
    protected $sms;

    public function __construct()
    {
        parent::__construct();

        $this->sms = ServerAPI::getInstance();
    }

    /**
     * send_code
     * 发送短信验证码
     * @api http://xxx/api/sms/send_code
     *      mobile 必填 手机号码
     *
     * @author zhengkai
     * @date 2017-08-08
     *
     * @return \think\response\Json
     */
    public function send_code()
    {
        $form = input('post.');

        // 表单验证
        $validate = new Validate([
            ['mobile', 'require|^1[3-9]\d{9}$', '手机号码不能为空|手机号码格式不正确'], // 手机号码
        ]);
        if(!$validate->check($form)) return json(array('code'=>1010, 'msg'=>$validate->getError(), 'data'=>null));

        $result = $this->sms->sendCode($form['mobile']);

        if ($result['code']==200) {
            return json(array('code'=>200, 'msg'=>'验证码已发送，请注意查收。', 'data'=>null));
        }elseif (416 == $result['code']) {
            return json(array('code'=>1012, 'msg'=>'发送太过频繁，请稍后再试', 'data'=>null));
        } else {
            return json(array('code'=>1012, 'msg'=>'fail', 'data'=>null));
        }
    }


}