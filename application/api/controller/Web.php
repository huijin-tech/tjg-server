<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2018-04-23
 * @Time: 20:23
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： Web.php
 */
namespace app\api\controller;

use app\admin\model\ApiResponse;
use app\api\model\TeacherSpeaking;
use app\common\controller\Base;
use app\common\model\PositionData;
use app\common\model\teacher\Teacher;
use app\common\model\teacher\TeacherEssay;
use app\common\model\teacher\TeacherEssayCatalog;
use think\Db;

class Web extends Base {

    /**
     * index_voice
     * pc首页名家股说(老师语音)
     *
     * @return ApiResponse
     * @throws \think\exception\DbException
     */
    public function index_voice()
    {
        $result = TeacherSpeaking::getLimitVoice(5);

        return ApiResponse::success($result);
    }

    /**
     * index_live
     * pc首页直播数据接口
     * @api POST http://xxx/api/web/index_live
     *
     * @author zhengkai
     * @date 2018-04-23
     * @version version 1.4.1
     *
     * @return ApiResponse
     * @throws \think\exception\DbException
     */
    public function index_live()
    {
        $result = Live::pc_index();

        return ApiResponse::success($result);
    }

    /**
     * index_topic
     * pc首页图文直播数据接口
     * @api POST http://xxx/api/web/index_topic
     *
     * @author zhengkai
     * @date 2018-04-23
     * @version version 1.4.1
     *
     * @return ApiResponse
     * @throws \think\exception\DbException
     */
    public function index_topic()
    {
        $form = $this->getAndCheckForm([
            ['teacher_id', 'integer',],
        ]);

        $teacher_id=$form['teacher_id']??'';
        $result = Topics::pc_index($teacher_id);

        return ApiResponse::success($result);
    }

    /**
     * index_topic
     * pc首页文章资讯数据接口
     * @api POST http://xxx/api/web/index_article
     *
     * @author zhengkai
     * @date 2018-04-23
     * @version version 1.4.1
     *
     * @return ApiResponse
     * @throws \think\exception\DbException
     *
     * 'zq' => News::pc_index_teacher_article(),   // 掌乾文章
    'hot' => News::pc_index_news(),             // 实时热点
     */
    public function index_article()
    {
        $catalogs_temp=TeacherEssayCatalog::getCatalog();
        $catalogs=[];
        foreach ($catalogs_temp as $k=>$v){
            if($v['id']>0) $catalogs[$v['parent_desc']][]=$v['id'];
        }
        $row=4;
        $field='id,cover,title,created,summary,special_column_id';
        // 掌乾文章
        // $zq=TeacherEssay::getLimit(['catalog_id'=>isset($catalogs['branch_1']) ? ['in',$catalogs['branch_1']] : -1],$row,$field,'created desc',1);
        $result = [
            /*'zq' =>[
                'top'=>array_shift($zq)??[],
                'list'=>$zq??[]
            ],*/
            'zq' => News::pc_index_teacher_article(),
            // 'hot' => TeacherEssay::getLimit(['catalog_id'=>isset($catalogs['branch_2']) ? ['in',$catalogs['branch_2']] : -1],$row,$field,'created desc',2),   // 实时热点、观点
            'hot' => News::pc_index_news(),   // 实时热点
            'school' => TeacherEssay::getLimit(['catalog_id'=>isset($catalogs['branch_3']) ? ['in',$catalogs['branch_3']] : -1 ],$row,$field,'created desc',3), // 掌乾学堂
        ];

        return ApiResponse::success($result);
    }

    /**
     * index_topic
     * pc首页问答数据接口
     * @api POST http://xxx/api/web/index_ask
     *
     * @author zhengkai
     * @date 2018-04-23
     * @version version 1.4.1
     *
     * @return ApiResponse
     * @throws \think\exception\DbException
     */
    public function index_ask()
    {
        $result = Ask::pc_index();

        return ApiResponse::success($result);
    }

    /**
     * index_topic
     * pc首页专栏数据接口
     * @api POST http://xxx/api/web/index_special
     *
     * @author zhengkai
     * @date 2018-04-23
     * @version version 1.4.1
     *
     * @return ApiResponse
     * @throws \think\exception\DbException
     */
    public function index_special()
    {
        $result = Special::pc_index();

        return ApiResponse::success($result);
    }

    /**
     * index_topic
     * pc首页老师数据接口
     * @api POST http://xxx/api/web/index_teacher
     *      limit 选填 获取数据条数，默认3
     *
     * @author zhengkai
     * @date 2018-04-23
     * @version version 1.4.1
     *
     * @return ApiResponse
     * @throws \think\exception\DbException
     */
    public function index_teacher()
    {
        $form = $this->getAndCheckForm([
            ['limit', 'integer', 'limit参数类型不正确']
        ]);
        if (!isset($form['limit'])) $form['limit']=3;

        $result = Teacher::recommendeds($form['limit']);

        return ApiResponse::success($result);
    }

    /**
     * index_topic
     * pc首页课程数据接口
     * @api POST http://xxx/api/web/index_course
     *
     * @author zhengkai
     * @date 2018-04-23
     * @version version 1.4.1
     *
     * @return ApiResponse
     * @throws \think\exception\DbException
     */
    public function index_course()
    {
        $result = Curriculum::pc_index();

        return ApiResponse::success($result);
    }

    /**
     * position
     * 推荐位内容数据接口
     * @api POST http://xxx/api/web/position
     *      position 必填 推荐位标识id
     *      num 选填 获取数据条数，默认1
     *
     * @author zhengkai
     * @date 2018-04-26
     * @version 1.4.1
     *
     * @return ApiResponse
     * @throws \think\exception\DbException
     */
    public function position()
    {
        $form = $this->getAndCheckForm([
            ['position', 'require|integer', '推荐位标识不能为空|参数类型不正确'],
            ['num', 'integer', '参数类型不正确'],
        ]);

        $form['num'] = isset($form['num'])?$form['num']:1;

        $result = PositionData::getPosition($form['position'], $form['num']);

        return ApiResponse::success($result);
    }


    /**
     * query_agency
     * 分销机构(经销商)查询接口
     *
     * @author zhengkai
     * @date 20180626
     *
     * @return ApiResponse
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function query_agency()
    {
        $form = $this->getAndCheckForm([
            ['name', 'require', '机构名称不能为空'],
        ]);

        $map = [];
        //$map['name'] = ['like', "%{$form['name']}%"];
        $data = Db::table('zds_agency')
            ->where('name',$form['name'])
            ->where('is_query', 'true')
            ->field('name, logo, description, telephone')
            ->select();

        foreach ($data as &$val) {
            $val['level'] = '金牌';
        }

        return ApiResponse::success($data);
    }
}