<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2017/8/9
 * Time: 12:59
 */

namespace app\api\controller;


use app\common\controller\Base;
use app\common\model\ApiResponse;

class Error extends Base
{
    public function index() {
        ApiResponse::error(ApiResponse::ERR_INVALID_URI, '访问资源不存在');
    }
}