<?php
/**
 * Created by PhpStorm.
 * User: winnie
 * Date: 2018/7/18
 * Time: 10:23
 */

namespace app\api\controller;


use app\api\model\topic\TopicEssay;
use app\common\controller\Base;
use app\teacher\lib\ApiResponse;

class TopicsEssay extends Base
{
    public function list(){
        $form = $this->getAndCheckForm([
            ['teacher_id', 'integer',],
            ['page','integer',],
            ['rows','integer',],
        ]);
        $form['page']=$form['teacher_id']??1;
        $form['rows']=$form['rows']??10;
        $condition=[];
        if(isset($form['teacher_id']) && $form['teacher_id']>0){
            $condition['teacher_id']=$form['teacher_id'];
        }
        $data=TopicEssay::getList($form['page'],$form['rows'],$condition);
        return ApiResponse::success($data);
    }

}