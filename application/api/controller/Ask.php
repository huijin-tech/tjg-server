<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2018-03-01
 * @Time: 10:36
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： Ask.php
 */
namespace app\api\controller;

use app\api\model\ask\AskLiked;
use app\api\model\ask\AskMessage;
use app\api\model\ask\AskView;
use app\api\model\Teacher;
use app\common\controller\Base;
use app\common\model\ApiResponse;
use think\Cache;
use think\Db;

class Ask extends Base {
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * submit
     * 提交用户提问
     * @api POST http://xxx/api/ask/submit
     *      teacher_id  必填  老师id
     *      content     必填  问题内容
     *      img         选填  图片，每个图片地址之间使用英文逗号分隔
     *
     * @author zhengkai
     * @date 2018-03-02
     * @version 1.4
     *
     * @return \think\response\Json
     */
    public function submit()
    {
        $uid = checkUserToken(true);
        $form = $this->getAndCheckForm([
            ['teacher_id', 'require|integer', '老师ID不能为空'],
            ['content', 'require|max:200', '提问内容不能为空'],
        ]);
        if (!isset($form['img'])) $form['img'] = null;
        $form['img'] = $form['img']?json_encode(explode(',', $form['img'])):null;

        $result = \app\api\model\ask\Ask::askPost($uid, $form['teacher_id'], $form['content'], $form['img']);

        return $result;
    }

    /**
     * lists
     * 用户提问列表
     * @api POST http://xxx/api/ask/lists
     *      page 选填 当前页数，默认1
     *      limit   选填  每页加载条数，默认10
     *
     * @author zhengkai
     * @date 2018-03-05
     * @version 1.4
     *
     *
     * @return ApiResponse
     */
    public function lists()
    {
        $uid = checkUserToken(false);
        $form = $this->getAndCheckForm([
            ['page', 'integer',],
            ['limit', 'integer',],
        ]);

        if (!isset($form['page'])) $form['page'] = 1;
        if (!isset($form['limit'])) $form['limit'] = 10;

        $result = \app\api\model\ask\Ask::queryAllAsk($uid, $form['page'], $form['limit']);

        $result = ApiResponse::success($result);

        // 总页数统计 zk 20180424 新增
        $total = Db::query("select count(*) as total from ask");
        $page = $total[0]['total'] / $form['limit'];
        $result->page_total = ceil($page);

        return $result;
    }

    /**
     * teacher
     * 找人提问老师列表
     * @api POST http://xxx/api/ask/teacher
     *      order 必填 排序方式，'answer'=回答最多、'liked'=点赞最多
     *      page  选填 当前页数，默认1
     *      limit 选填 每页加载数，默认10
     *
     * @author zhengkai
     * @date 2018-03-05
     * @version 1.4
     *
     * @return ApiResponse
     */
    public function teacher()
    {
        $form = $this->getAndCheckForm([
            ['order', 'require', '排序方式不能为空'],
            ['page', 'integer',],
            ['limit', 'integer',],
        ]);
        if (!isset($form['page'])) $form['page'] = 1;
        if (!isset($form['limit'])) $form['limit'] = 10;

        $result = Teacher::askTeacher($form['order'], $form['page'], $form['limit']);

        return ApiResponse::success($result);
    }

    /**
     * like
     * 问答点赞
     * @api POST http://xxx/api/ask/like
     *      ask_id 必填 问答id
     *
     * @author zhengkai
     * @date 2018-03-05
     * @version 1.4
     *
     * @return ApiResponse
     */
    public function like()
    {
        $uid = checkUserToken(true);
        $form = $this->getAndCheckForm([
            ['ask_id', 'require|integer', '问答id不能为空'],
        ]);

        $result = AskLiked::liked($uid, $form['ask_id']);

        if ($result) {
            return ApiResponse::success();
        } else {
            return ApiResponse::error(ApiResponse::ERR_OPERATE_FAILED,'请求失败');
        }
    }

    /**
     * message
     * 问答留言提交
     * @api POST http://xxx/api/ask/message
     *      ask_id  必填  问答id
     *      content 必填  留言内容，最多不能超过200个字符
     *
     * @author zhengkai
     * @date 2018-03-06
     * @version 1.4
     *
     * @return ApiResponse
     */
    public function message()
    {
        $uid = checkUserToken(true);
        $form = $this->getAndCheckForm([
            ['ask_id', 'require|integer', '问答id不能为空'],
            ['content', 'require|max:200', '留言内容不能为空'],
        ]);

        $result = AskMessage::messageSave($uid, $form['ask_id'], $form['content']);

        if ($result) {
            return ApiResponse::success();
        } else {
            return ApiResponse::error(ApiResponse::ERR_OPERATE_FAILED,'留言失败');
        }
    }

    /**
     * message_list
     * 问答留言列表
     * @api POST http://xxx/api/ask/message_list
     *      ask_id  必填  问答id
     *
     * @author zhengkai
     * @date 2018-03-06
     * @version 1.4
     *
     * @return ApiResponse
     */
    public function message_list()
    {
        $form = $this->getAndCheckForm([
            ['ask_id', 'require|integer', '问答id不能为空'],
            ['page', 'integer',],
            ['limit', 'integer',],
        ]);

        if (!isset($form['page'])) $form['page'] = 1;
        if (!isset($form['limit'])) $form['limit'] = 10;

        $result = AskMessage::getAskMessage($form['ask_id'], $form['page'], $form['limit']);

        return ApiResponse::success($result);
    }

    /**
     * detail
     * 问答详情
     * @api POST http://xxx/api/ask/detail
     *      ask_id  必填  问答id
     *
     * @author zhengkai
     * @date 2018-03-07
     * @version 1.4
     *
     * @return ApiResponse
     */
    public function detail()
    {
        $uid = checkUserToken(false);

        $form = $this->getAndCheckForm([
            ['ask_id', 'require|integer', '问答id不能为空'],
        ]);

        if (\app\api\model\ask\Ask::$viewCredit===0 && $uid>0) {
            AskView::createView($uid, $form['ask_id']);
        }

        $result = \app\api\model\ask\Ask::getSingleAsk($uid, $form['ask_id']);

        return ApiResponse::success($result);
    }

    /**
     * check_credit
     * 提问或偷看答案用户积分检测
     * @api POST http://xxx/api/ask/check_credit
     *      type        必填  业务类型：1=问答提问，2=偷看问题答案
     *      teacher_id  选填  老师id，type=2时可留空
     *
     * @author zhengkai
     * @date 2018-03-07
     * @version 1.4
     *
     * @return \think\response\Json
     */
    public function check_credit()
    {
        $uid = checkUserToken(true);
        $form = $this->getAndCheckForm([
            ['type', 'require|integer', '业务类型不能为空'],
            // ['ask_id', 'require|integer', '问答id不能为空'],
            ['teacher_id', 'integer',],
        ]);

        if (!isset($form['teacher_id'])) $form['teacher_id'] = 0;

        $result = \app\api\model\ask\Ask::checkCredit($form['type'], $uid, $form['teacher_id']);

        return $result;
    }

    /**
     * peep
     * 问题答案偷看
     * @api POST http://xxx/api/ask/peep
     *      ask_id  必填  问答id
     *
     * @author zhengkai
     * @date 2018-03-07
     * @version 1.4
     *
     * @return ApiResponse
     */
    public function peep()
    {
        $uid = checkUserToken(true);

        $form = $this->getAndCheckForm([
            ['ask_id', 'require|integer', '问答id不能为空'],
        ]);

        $result = AskView::createView($uid, $form['ask_id']);

        if ($result) {
            return ApiResponse::success();
        } else {
            return ApiResponse::error(ApiResponse::ERR_OPERATE_FAILED,'请求失败');
        }
    }

    /**
     * del
     * 过期问答删除
     * @api POST http://xxx/api/ask/del
     *      ask_id  必填  问答id
     *
     * @author zhengkai
     * @date 2018-03-08
     * @version 1.4
     *
     * @return ApiResponse
     */
    public function del()
    {
        $uid = checkUserToken(true);
        $form = $this->getAndCheckForm([
            ['ask_id', 'require|integer', '问答id不能为空'],
        ]);

        $result = \app\api\model\ask\Ask::delAsk($uid, $form['ask_id']);

        if ($result) {
            return ApiResponse::success();
        } else {
            return ApiResponse::error(ApiResponse::ERR_OPERATE_FAILED,'删除失败');
        }
    }

    /**
     * pc_index
     * pc首页问答
     *
     * @author zhengkai
     * @date 2018-04-23
     *
     * @return false|static[]
     * @throws \think\exception\DbException
     */
    public static function pc_index()
    {
        $result = \app\api\model\ask\Ask::getHotAsk();

        return $result;
    }
}