<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2017-10-24
 * @Time: 16:27
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： Stock.php
 */
namespace app\api\controller;

use app\admin\model\ApiResponse;
use app\common\controller\Base;
use app\common\controller\SinaHq;
use Curl\Curl;
use think\Log;
use think\Request;

class Stock extends Base {

    protected $stockNew;

    public function __construct()
    {
        parent::__construct();

        $this->stockNew = new \app\api\model\Stock();
    }

    /**
     * new_stock
     * 新股日历数据接口
     * @api http://xxx/api/web/other/new_stock
     *
     * @author zhengkai
     * @date 2017-10-24
     *
     * @return \think\response\Json
     */
    public function new_stock()
    {
        return $this->stockNew->getNewStock();
    }

    /**
     * 股票涨幅
     * @return ApiResponse
     */
    public function detail()
    {
        $form = $this->getAndCheckForm([
            ['code', 'require','股票代码不能为空'],
        ]);
        $data=SinaHq::getStock(explode(',',$form['code']));
        if(!$data) return ApiResponse::error(ApiResponse::ERR_INVALID_REQUEST,'服务器错误');
        $result=SinaHq::changeData($data);
        if(!$data) return ApiResponse::error(ApiResponse::ERR_INVALID_REQUEST,'股票不存在');
        return ApiResponse::success(count($result)==1 ? $result[0] : $result);
    }

    /**
     * stockData
     * 获取股票数据
     *
     * @author zhengkai
     * @date 2018-05-11
     *
     * @param $code
     * @return mixed
     */
    public static function stockData($code)
    {
        try {
            $data=SinaHq::getStock(explode(',', $code));
            if(!$data) return [];
            $result=SinaHq::changeData($data);
            return $result[0]??[];
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return false;
        }
    }
}