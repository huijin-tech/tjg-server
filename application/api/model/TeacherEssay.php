<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2018-04-23
 * @Time: 16:44
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： TeacherEssay.php
 */
namespace app\api\model;

use app\api\model\curriculum\Curriculum;
use think\Db;
use think\Model;

class TeacherEssay extends Model {
    protected $table = 'teacher_essay';
    protected $pk = 'id';

    /**
     * getNewTeacherArticle
     * 获取指定条数的最新老师文章
     * 老师原创文章或老师专栏文章
     *
     * @author zhengkai
     * @date 2018-04-23
     *
     * @param int $offset
     * @param int $limit
     * @return false|static[]
     * @throws \think\exception\DbException
     */
    public static function getNewTeacherArticle($offset=0, $limit=10)
    {
        $data = self::all(function ($query) use ($offset, $limit) {
            $query->where('hide', 'false');
            $query->where('teacher_id', '>', 0);
            $query->field('id, title, summary, cover, created, special_column_id');
            $query->order('created desc');
            $query->limit($offset, $limit);
        });

        foreach ($data as &$val) {
            $val['created'] = date('Y-m-d H:i:s', strtotime($val['created']));
            $val['is_special'] = $val['special_column_id']?1:0;

            unset($val['special_column_id']);
        }

        return $data;
    }


    /**
     * getNewsArticle
     * 获取指定条数的最新资讯文章
     *
     * @author zhengkai
     * @date 2018-04-23
     *
     * @param int $offset
     * @param int $limit
     * @return false|static[]
     * @throws \think\exception\DbException
     */
    public static function getNewsArticle($offset=0, $limit=10)
    {
        $data = self::all(function ($query) use ($offset, $limit) {
            $query->where('hide', 'false');
            $query->where('teacher_id', 0);
            $query->where('special_column_id', 0);
            $query->field('id, title, summary, cover, created');
            $query->order('created desc');
            $query->limit($offset, $limit);
        });

        foreach ($data as &$val) {
            $val['summary'] = $val['summary'] ?? '';
            $val['title'] = $val['title'] ?? '';
            $val['cover'] = $val['cover'] ?? '';
            $val['created'] = date('Y-m-d H:i:s', strtotime($val['created']));
            $val['summary'] = $val['summary']??'';
            unset($val['special_column_id']);
        }

        return $data;
    }

    /**
     * getBindCourseArticle
     * 获取课程服务的文章
     *
     * @author zhengkai
     * @date 2018-05-25
     *
     * @param int $catalogId 文章栏目id
     * @param int $courseId 绑定的课程id
     * @return mixed
     */
    public static function getBindCourseArticle($catalogId, $courseId, $limit=5)
    {
        $data = Db::query("select id,title,created  from teacher_essay where catalog_id=:catalogId and bind_course @> '[{$courseId}]' and hide=false order by created desc limit {$limit}", ['catalogId'=>$catalogId]);

        foreach ($data as &$val) {
            $val['created'] = tranTime(strtotime($val['created']));
        }

        return $data;
    }

    /**
     * checkBingCourseArticleIsBuy
     * 查询文章是否绑定课程，所绑定的课程是否被购买
     *
     * @author zhengkai
     * @date 2018-05-28
     *
     * @param $uid
     * @param $articleId
     * @param $courseId
     * @return bool
     * @throws \think\exception\DbException
     */
    public static function checkBingCourseArticleIsBuy($uid, $articleId, $courseId=0)
    {
        $article = self::get(function ($query) use ($articleId, $courseId) {
            $query->where('id', $articleId)
                ->where(function ($where){
                    $where->where('catalog_id', config('course_serivce.internalReferenceId'))
                        ->whereOr('catalog_id', config('course_serivce.researchReportId'));
                })
                ->where("bind_course @> '[{$courseId}]'");
        });

        if ($article) {
            $is_buy = Curriculum::haveBought($uid, $courseId);
            if ($is_buy) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}