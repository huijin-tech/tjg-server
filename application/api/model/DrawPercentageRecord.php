<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2018-04-17
 * @Time: 16:31
 * @Author: wangxuejiao
 *
 */
namespace app\api\model;

use think\Model;

class DrawPercentageRecord extends Model {
    protected $table = 'draw_percentage_record';
    protected $pk = 'id';

    /**
     * priceSum
     * 获取用户提成总额
     *
     * @author wangxuejiao
     * @date 2018-04-12
     *
     * @param $promoter_id 邀请用户id
     * @return float|int
     */
    public static function priceSum($promoter_id){
        return self::where('promoter_id',$promoter_id)->sum('draw_percentage_price');
    }

    public static function addRecord(array $data){
        return self::create($data);
    }

    public static function getRecordList(array $condition,$page=1,$limit=10,$order='bill_paytime desc'){
        $data = self::alias('d')
            ->join('users u', 'd.bill_user=u.id', 'left')
            ->join('buy_bill b', 'b.bill_id=d.bill_id and b.bill_no=d.bill_no', 'left')
            ->field('
                u.id,
                u.mobile,
                d.draw_percentage_price,
                b.bill_pay_amount as price,
                b.bill_item_title,
                b.bill_paytime
            ')
            ->where($condition)
            ->order($order)
            ->page($page, $limit)
            ->select();
        if(!$data) return $data;
        foreach ($data as &$item){
            $item->mobile= mobile_hidden($item->mobile);
            $item->bill_paydate = date('Y-m-d',$item->bill_paytime);
        }
        return $data;
    }

    public static function getRecordListCount(array $condition){
        return  self::alias('d')
            ->join('users u', 'd.bill_user=u.id', 'left')
            ->join('buy_bill b', 'b.bill_id=d.bill_id and b.bill_no=d.bill_no', 'left')
            ->field('
                u.id,
                u.mobile,
                d.draw_percentage_price,
                b.bill_pay_amount as price,
                b.bill_item_title,
                b.bill_paytime
            ')
            ->where($condition)
            ->count();
    }

    public static function getTotalPrice(array $condition){
        $data=self::alias('d')
            ->join('buy_bill b', 'b.bill_id=d.bill_id and b.bill_no=d.bill_no', 'left')
            ->where($condition)
            ->field('b.bill_pay_amount AS price,d.draw_percentage_price as percentage')
            ->select();
        $result=[
            'price'=>'0.00',
            'percentage'=>'0.00',
        ];
        if($data){
            foreach ($data as $item){
                $result['price']+=$item['price'];
                $result['percentage']+=$item['percentage'];
            }
            $result['price']=sprintf("%.2f",$result['price']);
            $result['percentage']=sprintf("%.2f",$result['percentage']);
        }
        return $result;
    }

    public static function getRecordListPage(array $condition,$page=1,$limit=10,$order='bill_paytime desc'){
        $data = self::alias('d')
            ->join('users u', 'd.bill_user=u.id', 'left')
            ->join('buy_bill b', 'b.bill_id=d.bill_id and b.bill_no=d.bill_no', 'left')
            ->field('
                u.id,
                u.mobile,
                d.draw_percentage_price,
                b.bill_pay_amount as price,
                b.bill_item_title,
                b.bill_paytime
            ')
            ->where($condition)
            ->order($order)
            ->paginate($limit)->toArray();
        if($data['total']<1) return $data;
        foreach ($data['data'] as &$item){
            $item['mobile'] = mobile_hidden($item['mobile']);
            $item['bill_paydate'] = date('Y-m-d H:i',$item['bill_paytime']);
        }
        return $data;
    }

}