<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2018-01-12
 * @Time: 16:31
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： DynamicFlow.php
 */
namespace app\api\model;

use app\api\model\curriculum\Curriculum;
use app\api\model\live\Live;
use app\api\model\special\UserSubscribeSpecial;
use app\common\model\teacher\TeacherEssay;
use app\common\model\Topic;
use think\Db;
use think\db\Query;
use think\Model;

class DynamicFlow extends Model {
    protected $table = 'dynamic_flow';
    protected $pk = 'df_id';

    public static function manageList($rows, $page) {
        $p = self::order('df_id','desc')
            ->paginate($rows,false, ['page'=>$page]);
        return $p;
    }

    public static function del($id) {
        $sql = <<<SQL
DELETE FROM dynamic_flow WHERE df_id=:id RETURNING *
SQL;
        $re = Db::execute($sql, ['id' => intval($id)]);
        return $re[0] ?? null;
    }

    /**
     * getDynamic
     * 获取名师动态数据（信息流）
     *
     * @author zhengkai
     * @date 2018-01-12
     *
     * @param int $uid 用户id
     * @param int $limit 获取数据条数，默认10条
     * @return array
     */
    public function getDynamic($uid, $page=1, $limit=10)
    {
        $data = $this::all(function (Query $query) use (&$page, &$limit) {
            $query->where('df_delete=false')
                ->order('df_time desc')
                ->page($page, $limit);
        });

        $dataArr = [];
        foreach ($data as &$val) {
            $val['df_time'] =tranTime($val['df_time']);
            $val['df_value'] = json_decode($val['df_value'], true);
            $val['df_isnew'] = $val['df_isnew']?1:0;

            $dataArr[] = $val->toArray();
        }

        $list = [];
        foreach ($dataArr as $key=>$val) {
            // 获取老师ID
            $teacherId = $val['df_value']['item_teacher']['teacher_id'];
            // 获取课程id
            $curriculumId = $val['df_value']['item_id'];

            // 老师被关注状态
            $val['df_value']['item_teacher']['is_follow'] = Teacher::getFollowStatus($uid, $teacherId);

            // 动态分类
            switch ($val['df_class']) {
                case 1: // 专栏
                    if (empty($val['df_value']['item_cover'])) $val['df_value']['item_cover'] = TeacherEssay::$DefaultCover;
                    // 专栏栏目ID
                    $scid = $val['df_value']['item_column']['column_id'];

                    // 专栏被登陆用户订阅状态
                    $val['df_value']['item_column']['column_subscribe_status'] = UserSubscribeSpecial::isBuy($uid, $scid);
                    break;
                case 2: // 文章
                    if (empty($val['df_value']['item_cover'])) $val['df_value']['item_cover'] = TeacherEssay::$DefaultCover;
                    break;
                case 3: // 视频直播
                    $live = Live::getSimple($val['df_value']['item_id']);
                    $val['df_value']['item_live_status'] = $live->status;
                    if (Live::STATUS_START == $live->status) $val['df_value']['item_id'] = $live->live_channel_id;
                    break;
                case 4: // 课程
                    // 课程价格
                    $price = Curriculum::getCurriculumPrice($curriculumId);
                    $val['df_value']['item_sale_price'] = $price['sale_price']?:0;
                    $val['df_value']['item_market_price'] = $price['market_price']?:0;
                    break;
                case 5: // 图文直播
                    if (isset($val['df_value']['item_topic'])) {
                        $topicId = $val['df_value']['item_topic']['id'];
                        $val['df_value']['item_topic'] = Topic::apiInfo($topicId, $uid);
                    }
                    break;
            }

            // 动态类型
            switch ($val['df_type']) {
                case 1: // 图文
                    break;
                case 2: // 语音
                    // 媒体文件时长
                    $item_duration = $val['df_value']['item_duration'];
                    $val['df_value']['item_duration'] = secToTime(intval($item_duration));
                    break;
                case 3: // 视频
                    // 媒体文件时长
                    $item_duration = $val['df_value']['item_duration'];
                    $val['df_value']['item_duration'] = secToTime(intval($item_duration));
                    break;
                case 4: // 直播视频
                    break;
                case 5: // 课程
                    break;
                case 6: // 图文直播
                    break;
            }

            // 内容收费标识
            $val['df_isfee'] = $val['df_isfee']?1:0;
            unset($val['df_delete']);
            unset($val['df_source']);
            $list[] = $val;
        }

        return $list;
    }
}