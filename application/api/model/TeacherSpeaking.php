<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2018-04-24
 * @Time: 09:24
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： TeacherSpeaking.php
 */
namespace app\api\model;

use think\Model;

class TeacherSpeaking extends Model {
    protected $table = 'teacher_speaking';
    protected $pk = 'id';

    /**
     * getLimitVoice
     * 获取指定数量的老师语音数据
     *
     * @author zhengkai
     * @date 2018-04-24
     *
     * @param int $limit
     * @return false|static[]
     * @throws \think\exception\DbException
     */
    public static function getLimitVoice($limit=10)
    {
        $data = self::all(function ($query) use ($limit) {
            $query->field('id, title, url');
            $query->order('created desc');
            $query->limit($limit);
        });

        foreach ($data as &$val) {
            // $val['created'] = date('Y-m-d H:i:s', $val['created']);
        }

        return $data;
    }
}