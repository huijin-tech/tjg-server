<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2018/3/6
 * Time: 10:41
 */

namespace app\api\model\ask;


use think\Model;

/**
 * Class AskAnswer
 * @package app\api\model\ask
 *
 * @property mixed id
 * @property mixed ask_id
 * @property mixed teacher_id
 * @property mixed content
 * @property mixed img
 * @property mixed times
 */
class AskAnswer extends Model
{
    protected $table = 'ask_answer';
    protected $pk = 'id';
    protected $field = [
        'id',
        'ask_id',
        'teacher_id',
        'content',
        'img',
    ];

    public static function get($data, $with = [], $cache = false)
    {
        $model = parent::get($data, $with, $cache);
        if ($model) $model->img = json_decode($model->img);
        return $model;
    }

    public function save($data = [], $where = [], $sequence = null)
    {
        $this->img = json_encode($this->img,JSON_UNESCAPED_UNICODE);
        $result = parent::save($data, $where, $sequence);
        $this->img = json_decode($this->img);
        return $result;
    }
}