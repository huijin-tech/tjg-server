<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2018-04-23
 * @Time: 14:42
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： TopicM.php
 */
namespace app\api\model\topic;

use think\Model;

class TopicM extends Model {
    protected $table = 'topic';
    protected $pk = 'id';
}