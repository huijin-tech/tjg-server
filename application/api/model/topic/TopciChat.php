<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2018-04-23
 * @Time: 14:43
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： TopciChat.php
 */
namespace app\api\model\topic;

use think\Model;

class TopciChat extends Model {
    protected $table = 'topic_chat';
    protected $pk = 'id';
}