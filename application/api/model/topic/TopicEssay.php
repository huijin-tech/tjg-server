<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2018-04-23
 * @Time: 14:45
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： TopicEssay.php
 */
namespace app\api\model\topic;

use app\common\model\teacher\Teacher;
use think\Model;

class TopicEssay extends Model {
    protected $table = 'topic_essay';
    protected $pk = 'id';

    /**
     * getWeekList
     * 获取七天内的所有图片直播数据
     *
     * @author zhengkai
     * @date 2018-04-23
     *
     * @return false|static[]
     * @throws \think\exception\DbException
     */
    public static function getWeekList($teacher_id='')
    {
        $now = time();
        $startTime = date('Y-m-d 00:00:00', strtotime('-7 days'));
        $endTime = date('Y-m-d 23:59:59', $now);
        $data = self::all(function ($query) use ($startTime, $endTime,$teacher_id) {
            $query->alias('tope');
            $query->join('topic top', 'tope.topic_id=top.id', 'left');
            $query->join('teacher t', 'top.teacher_id=t.id', 'left');
            $query->where('tope.created', 'BETWEEN', [$startTime, $endTime]);
            if($teacher_id>0){
                $query->where('t.id', $teacher_id);
            }
            $query->field('top.id as topic_id, tope.content, tope.created, top.title, t.realname as teacher_name, t.avatar as teacher_avatar');
            $query->order('tope.created desc');
        });
        foreach ($data as &$val) {
            $val['img'] = getImages($val['content'])?:[];
            $audio = getAudio($val['content']);
            $val['audio'] = (is_array($audio) && count($audio)>0)?$audio[0]:'';
            $video = getVideo($val['content']);
            $val['video'] = (is_array($video) && count($video)>0)?$video[0]:'';
            $val['teacher_avatar'] = $val['teacher_avatar']?:Teacher::DEFAULT_AVATAR;
            $val['content'] = stripHtml($val['content']);
            $val['created'] = tranTime(strtotime($val['created']));
        }

        return $data;
    }

    public static function getList($page,$rows,array $condition=[])
    {

        $data=self::alias('tope')
            ->join('topic top', 'tope.topic_id=top.id', 'left')
            ->join('teacher t', 'top.teacher_id=t.id', 'left')
            ->field('top.id as topic_id, tope.content, tope.created, top.title, t.realname as teacher_name, t.avatar as teacher_avatar,top.num_liked,top.participants_count')
            ->where($condition)
            ->order('tope.created desc')
            ->paginate($rows)
            ->toArray();
        if($data['total']>0){
            foreach ($data['data'] as &$val) {
                $val['img'] = getImages($val['content'])?:[];
                $audio = getAudio($val['content']);
                $val['audio'] = (is_array($audio) && count($audio)>0)?$audio[0]:'';
                $video = getVideo($val['content']);
                $val['video'] = (is_array($video) && count($video)>0)?$video[0]:'';
                $val['teacher_avatar'] = $val['teacher_avatar']?:Teacher::DEFAULT_AVATAR;
                $val['content'] = stripHtml($val['content']);
                $val['created'] = tranTime(strtotime($val['created']));
            }
        }
        return $data;
    }
}