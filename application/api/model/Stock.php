<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2017-10-24
 * @Time: 16:29
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： Stock.php
 */
namespace app\api\model;

use think\Db;
use think\Model;

class Stock extends Model {
    /**
     * getNewStock
     * 获取新股日历数据
     *
     * @author zhengkai
     * @date 2017-10-24
     *
     * @return \think\response\Json
     */
    public function getNewStock()
    {
        $sql = <<<SQL
select zjjc,sgdm,swfxrq,fxjg from stock_new order by swfxrq desc;
SQL;
        $data = Db::query($sql);

        // 获取所有发行（申购）日期
        $releaseDate = [];
        foreach ($data as $val) array_push($releaseDate, $val['swfxrq']);
        $releaseDate = array_unique($releaseDate);

        // 星期转换
        $week = ['日', '一', '二', '三', '四', '五', '六'];

        // 按照发行（申购）日期分组获取新股数据
        $arr = [];
        foreach ($releaseDate as $key=>$val) {
            $arr2 = [];
            foreach ($data as $key2=>$val2) {
                if ($val==$val2['swfxrq']) $arr2[] = $val2;
            }

            $arr[] = [
                'releaseDate' => date('Y/m/d', $val),
                'releaseWeek' => '星期'.$week[date('w', $val)],
                'list' => $arr2
            ];
        }

        $result = $arr;

        return json(array('code'=>200, 'msg'=>'success', 'data'=>$result));
    }
}