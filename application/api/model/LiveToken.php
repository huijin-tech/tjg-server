<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2017-10-24
 * @Time: 16:29
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： Stock.php
 */
namespace app\api\model;

use think\Model;

class LiveToken extends Model {
    public static function getOne($token){
        return self::where('token',$token)->find();
    }
}