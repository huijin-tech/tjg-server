<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2017-09-07
 * @Time: 15:48
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： Teacher.php
 */
namespace app\api\model;

use app\api\model\ask\Ask;
use think\Db;
use think\Model;

class Teacher extends Model {
    protected $table = 'teacher';
    protected $pk = 'id';
    /**
     * getFollowStatus
     * 获取老师的关注状态（当前登录用户）
     *
     * @author zhengkai
     * @date 2017-09-07
     *
     * @param int $uid 用户id
     * @param int $teacher_id 老师id
     * @return int
     */
    public static function getFollowStatus($uid, $teacher_id)
    {/*
        $result = Db::table('user_follow_teacher')
            ->cache(60)
            ->where('userid', $uid)
            ->where('teacher_id', $teacher_id)
            ->count();*/
        $sql = <<<SQL
select count(teacher_id) as follow_num from user_follow_teacher where userid={$uid} and teacher_id={$teacher_id}
SQL;
        $result = Db::query($sql);
        $result = $result[0]['follow_num'];

        if ($result) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * get_single_teacher
     * 获取指定id的单条老师数据
     *
     * @author zhengkai
     * @date 2017-09-07
     *
     * @param int $teacher_id
     * @return array|false|\PDOStatement|string|Model
     */
    public function get_single_teacher($teacher_id)
    {
        /*$result = $this
            ->alias('t')
            ->join('teacher_degree td', 't.degree_id=td.id', 'LEFT')
            ->cache(60)
            ->where('t.id', $teacher_id)
            ->field('t.id as teacher_id,td.name as degree_name, *')
            ->find();*/
        $sql = <<<SQL
  select t.id as teacher_id,td.name as degree_name, * from teacher t 
  LEFT JOIN teacher_degree td ON (t.degree_id=td.id)
  where t.id={$teacher_id}
SQL;

        $result = Db::query($sql);

        return $result[0];
    }

    /**
     * getBaseTeacher
     * 获取单个老师的基本信息数据
     *
     * @author zhengkai
     * @date 2018-01-16
     *
     * @param int $id 老师id
     * @return mixed
     */
    public static function getBaseTeacher($id)
    {
        $sql = <<<SQL
  select t.id as teacher_id, t.realname as teacher_name, t.avatar as teacher_avatar, td.name as teacher_degree, t.num_essay as teacher_article_total, t.num_followed as teacher_followed_total, t.followed, t.intro as teacher_info from teacher t 
  LEFT JOIN teacher_degree td ON (t.degree_id=td.id)
  where t.id={$id}
SQL;
        $result = Db::query($sql);
        $result = $result[0];

        // $result['teacher_article_total'] = num2tring($result['teacher_article_total']);
        $result['teacher_followed_total'] = countDataNum($result['teacher_followed_total'], $result['followed'], true);

        return $result;
    }

    /**
     * getConfigs
     * 获取指定的老师配置信息
     *
     * @author zhengkai
     * @date 2018-03-01
     *
     * @param $teacherId
     * @param $configItem
     * @return mixed
     */
    public static function getConfigs($teacherId, $configItem) {
        $data = self::get(function ($query) use (&$teacherId) {
            $query->where('id', $teacherId);
        });

        $teacherConfig = $data->config;

        $teacherConfig = json_decode($teacherConfig, true);

        return $teacherConfig[$configItem];
    }

    /**
     * getDegree
     * 获取老师头衔
     *
     * @author zhengkai
     * @date 2018-03-05
     *
     * @param int $id 头衔id
     * @return array|false|\PDOStatement|string|Model
     */
    public static function getDegree($id)
    {
        $data = Db::table('teacher_degree')
            ->where('id', $id)
            ->field('name')
            ->find();

        return $data['name'];
    }

    /**
     * askTeacher
     * 查询可进行付费问答的老师数据
     *
     * @author zhengkai
     * @date 2018-03-05
     *
     * @param string $order 排序方式：'answer'=按照回答最多问题排序，'liked'=按照点赞最多排序
     * @param int $page 当前页数，默认1
     * @param int $limit 每页加载数，默认10
     * @return array|false|string|static[]
     */
    public static function askTeacher($order='answer', $page=1, $limit=10)
    {
        $data = self::all(function ($query) {
            $query->field('
            id as teacher_id,
            realname as teacher_name,
            avatar as teacher_avatar,
            degree_id as teacher_degree,
            config->>\'ask_credit\' as ask_answer_credit
            ')
                ->where('hide', 'false');
        });

        foreach ($data as &$val) {
            $val['teacher_degree'] = self::getDegree($val['teacher_degree']);
            $val['ask_answer_credit'] = (int)$val['ask_answer_credit'];
            $val['ask_answer_num'] = Ask::teacherAnswerAskTotal($val['teacher_id']);
            $val['ask_like_num'] = Ask::teacherLikeAskTotal($val['teacher_id']);
        }

        // 数组排序处理
        if ($order=='answer') {
            $data = array_sort($data, 'ask_answer_num', 'desc');
            foreach ($data as &$val) $val['ask_answer_num'] = (int)$val['ask_answer_num'];
        } else if ($order=='liked') {
            $data = array_sort($data, 'ask_like_num', 'desc');
            foreach ($data as &$val) $val['ask_like_num'] = (int)$val['ask_like_num'];
        }

        // 数组分页处理
        $data = page_array($limit, $page, $data, 0);

        return $data;
    }

    /**
     * getTeacherSimpleList
     * 获取老师简单的数据
     *
     * @author zhengkai
     * @date 2018-04-24
     *
     * @param int $page
     * @param int $limit
     * @return false|static[]
     * @throws \think\exception\DbException
     */
    public static function getTeacherSimpleList($page=1, $limit=10)
    {
        $data = self::all(function ($query) use ($limit, $page) {
            $query->field('id, realname, intro, list_img');
            $query->where('hide','false');
            $query->order('rank desc, id desc');
            $query->page($page, $limit);
        });

        return $data;
    }

    public static function getRealName($curriculum_id){
        $result = self::alias('t')
            ->join('curriculum_video c', 'c.video_teacher=t.id', 'left')
            ->where('c.video_curriculum',$curriculum_id)
            ->field('t.realname')
            ->distinct('t.realname')
            ->column('t.realname');
        return $result;
    }

    public static function getRealNameById($id){
        return self::where('id',$id)->column('realname');
    }
}