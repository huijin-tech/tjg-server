<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2018-02-27
 * @Time: 09:42
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： Sales.php
 */
namespace app\api\model;

use think\Db;
use think\Model;

class Sales extends Model {
    protected $table = 'sales';
    protected $pk = 'sales_id';

    /**
     * gift
     * 优惠促销赠品数据处理
     *
     * @author zhengkai
     * @date 2018-02-27
     *
     * @param int $salesId 优惠活动id
     * @return array
     */
    public static function gift($salesId)
    {
        $data = self::get(function ($query) use (&$salesId) {
            $query->where('sales_id', $salesId);
        });
        $giveGoods = json_decode($data['gifts'], true);
        if (count($giveGoods)) {
            $giveArr = [];
            $courseArr = [];
            $columnArr = [];
            foreach ($giveGoods as $key=>$val) {
                switch ($key) {
                    case 'curricula': // 赠送课程
                        // 获取赠送课程数据
                        $courseId = implode(',', $val);
                        $giveCourse = Db::query("select 
c.curriculum_id as item_id, 
c.curriculum_name as item_title,
cc.class_name as item_class,
c.curriculum_type as item_course_type,
c.curriculum_level as item_course_level,
c.curriculum_suit_user as item_suit_user,
c.curriculum_market_price as item_market_price,
c.curriculum_sale_price as item_sale_price,
c.curriculum_pic as item_cover,
c.curriculum_episode_number as item_video_total
from curriculum as c
left join curriculum_class cc on c.curriculum_class=cc.class_id
where c.curriculum_id in ({$courseId})");
                        foreach ($giveCourse as $course_key=>$course_val) {
                            // $course_val['item_type'] = 1;
                            switch ($course_val['item_course_level']) {
                                case 1:
                                    $course_val['item_course_level'] = '初级';
                                    break;
                                case 2:
                                    $course_val['item_course_level'] = '中级';
                                    break;
                                case 3:
                                    $course_val['item_course_level'] = '高级';
                                    break;
                            }
                            $courseArr[$course_key]['item_type'] = 1;
                            $courseArr[$course_key]['goods_course'] = $course_val;
                            $courseArr[$course_key]['goods_column'] = (object)[];
                        }
                        break;
                    case 'columns': // 赠送专栏
                        // 获取赠送专栏数据
                        $columnId = implode(',', $val);
                        $giveCourse = Db::query("select 
column_id as item_id, 
column_title as item_title,
column_price as item_sale_price,
column_expiry as item_expiry_day,
covers->>'list' as item_cover
from special_column where column_id in ({$columnId})");
                        foreach ($giveCourse as $column_key=>$column_val) {
                            // $cloumn_val['item_type'] = 3;
                            $columnArr[$column_key]['item_type'] = 3;
                            $columnArr[$column_key]['goods_course'] = (object)[];
                            $columnArr[$column_key]['goods_column'] = $column_val;
                        }
                        break;
                }
                $giveArr = array_merge($courseArr, $columnArr);
            }
        } else {
            $giveArr = [];
        }

        return $giveArr;
    }
}