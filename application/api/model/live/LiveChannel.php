<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2018-04-23
 * @Time: 09:42
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： LiveChannel.php
 */
namespace app\api\model\live;

use think\Model;

class LiveChannel extends Model {
    protected $table = 'live_channel';
    protected $pk = 'id';
}