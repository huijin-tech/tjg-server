<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2018/1/4
 * Time: 17:57
 */

namespace app\teacher\lib;

use think\Config;
use think\exception\HttpResponseException;
use think\Response;


/**
 * Class ResponseEntity
 * @package app\teacher\lib
 *
 * @property int code
 * @property mixed data
 * @property string msg
 */
final class ApiResponse
{
    private function __construct(int $code, $data = null, string $msg = null) {
        $this->code = $code;
        $this->data = $data;
        $this->msg = $msg;
    }

    public static function success($data = null) {
        return new static(self::SUCCESS, $data, 'success');
    }

    public static function error(int $code, string $msg = null, $throwOut = true) {
        $type = Config::get('default_return_type');
        $response = Response::create(new static($code, null, $msg), $type);
        if ($throwOut) throw new HttpResponseException($response);
        else return $response;
    }

    const SUCCESS = 200;

    const ERR_AUTH_FAILED = 1010;
    const ERR_FORBIDDEN_SOURCE = 1012;

    const ERR_FORM_INVALID = 1020;

    const ERR_OPERATE_FAILED = 1030;


}