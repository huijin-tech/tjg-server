<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2018/1/9
 * Time: 11:03
 */

namespace app\teacher\lib;


use app\common\model\teacher\Teacher;
use think\exception\HttpResponseException;
use think\Request;
use think\Validate;

/**
 * Class TeacherController
 * @package app\teacher\lib
 *
 * @property integer limit
 * @property integer offset
 * @property integer page
 * @property integer rows
 * @property integer teacherId
 *
 */
abstract class TeacherBaseController
{
    public function __construct(Request $request)
    {
        $this->rows = $this->limit = intval($request->param('limit', 10));
        $page = $request->param('page');
        if (is_numeric($page) && $page > 1) {
            $this->offset = intval(($page - 1) * $this->limit);
        } else {
            $this->offset = intval($request->param('offset', 0));
        }
        $this->page = intval($this->offset / $this->limit) + 1;

        $this->teacherId = $request->teacherId;
    }

    protected function getLogin(): ?Teacher {
        return Teacher::get(Request::instance()->teacherId);
    }

    public function _empty() {
        return json(['msg' => '找不到接口'], 404);
    }

    /**
     * 处理post表单验证，
     * @param $validateRules
     * @return mixed
     * @throws HttpResponseException
     */
    protected function validFormData($validateRules) {
        $form = input('param.');
        $validate = new Validate($validateRules);
        if(!$validate->check($form)) {
            ApiResponse::error(ApiResponse::ERR_FORM_INVALID, $validate->getError());
        }
        return $form;
    }
}