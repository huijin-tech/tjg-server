<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2018-05-28
 * @Time: 11:13
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： CurriculumM.php
 */
namespace app\teacher\model;

use think\Db;
use think\Model;

class CurriculumM extends Model {
    protected $table = 'curriculum';
    protected $pk = 'curriculum_id';

    const LEVEL_PRIMARY = 1;
    const LEVEL_INTERMEDIATE = 2;
    const LEVEL_SENIOR = 3;
    public static $LevelNames = [
        self::LEVEL_PRIMARY         => '初级',
        self::LEVEL_INTERMEDIATE    => '中级',
        self::LEVEL_SENIOR          => '高级',
    ];

    const TYPE_ADVANCED = 1;
    const TYPE_OPEN     = 2;
    const TYPE_VIP      = 3;
    public static $TypeNames = [
        self::TYPE_ADVANCED => '高级课',
        self::TYPE_OPEN     => '公开课',
        self::TYPE_VIP      => 'VIP课程',
    ];

    /**
     * getBindArticleCourse
     * 获取绑定文章的课程数据
     *
     * @author zhengkai
     * @date 20180528
     *
     * @param int $bindArticleColId
     * @return mixed
     */
    public static function getBindArticleCourse($bindArticleColId=0) {
        $courseType = self::TYPE_ADVANCED;

        // zk 20180524 获取有课程服务且可绑定文章的课程查询条件
        switch ($bindArticleColId) {
            case config('course_serivce.internalReferenceId'):
                $where = "and cbs.article_reference=true";
                break;
            case config('course_serivce.researchReportId'):
                $where = "and cbs.article_report=true";
                break;
            default :
                $where = " and c.curriculum_type={$courseType} AND c.curriculum_sale_price>0";
        }

        $sql = <<<SQL
SELECT c.curriculum_id AS id ,c.curriculum_name AS name, c.curriculum_sale_price as price , c.curriculum_type
FROM curriculum as c
left join course_bind_service cbs on c.curriculum_id = cbs.curriculum_id
WHERE hide=false {$where}
order by c.curriculum_id desc
SQL;
        $list = Db::query($sql);

        foreach ($list as &$val) {
            $val['curriculum_type_name'] = self::$TypeNames[$val['curriculum_type']];
        }

        return $list;
    }
}