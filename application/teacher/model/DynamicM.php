<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2018-05-10
 * @Time: 09:18
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： DynamicM.php
 */
namespace app\teacher\model;

use think\Db;
use think\Log;
use think\Model;

class DynamicM extends Model {
    protected $table = 'dynamic';
    protected $pk = 'id';


    const TYPE_KANPAN = 1;
    public static $dynamicType = [
        self::TYPE_KANPAN => '掌乾看盘',
    ];

    /**
     * createDynamic
     * 创建动态
     *
     * @param array $param
     * @return bool
     */
    public static function createDynamic($param=[])
    {
        Db::startTrans();
        try {
            $res = self::create($param);
            Db::commit();
            return true;
        } catch (\Exception $e) {
            Db::rollback();
            Log::error("老师后台创建掌乾动态【error】：{$e->getMessage()}");
            return false;
        }
    }

    /**
     * updateDynamic
     * 更新动态
     *
     * @param $id
     * @param array $param
     * @return bool
     */
    public static function updateDynamic($id, $param=[])
    {
        Db::startTrans();
        try {
            self::where('id', $id)->update($param);
            Db::commit();
            return true;
        } catch (\Exception $e) {
            Log::error('老师后台更新掌乾动态【error】：'.$e->getMessage());
            Db::rollback();
            return false;
        }
    }

    /**
     * delDynamic
     * 删除动态
     *
     * @param $id
     * @return bool
     */
    public static function delDynamic($id)
    {
        Db::startTrans();
        try {
            self::destroy($id);
            Db::commit();
            return true;
        } catch (\Exception $e) {
            Log::error('老师后台删除掌乾动态【error】：'.$e->getMessage());
            Db::rollback();
            return false;
        }
    }

    /**
     * getAllDynamic
     * 获取全部动态数据
     *
     * @param int $teacherId
     * @param int $page
     * @param int $limit
     * @return DynamicM[]|false
     * @throws \think\exception\DbException
     */
    public static function getAllDynamic($teacherId, $page=1, $limit=10)
    {
        $data = self::all(function ($query) use ($teacherId, $page, $limit) {
            $query->where('author_type', 2)
                ->where('author', $teacherId)
                ->order('times desc')
                ->page($page, $limit);
        });

        foreach ($data as &$val) {
            $val['type'] = self::$dynamicType[$val['type']];

            $attachment = json_decode($val['attachment'], true);
            $val['images'] = $attachment['images']??[];
            $val['video'] = $attachment['video']??[];
            $val['audio'] = $attachment['audio']??[];

            $val['stock'] = isset($val['stock'])?json_decode($val['stock'], true):[];

            $val['times'] = date('Y-m-d H:i:s', $val['times']);

            unset($val['attachment']);
        }

        return $data;
    }

    /**
     * getSingleDynamic
     * 获取单条动态数据
     *
     * @param $id
     * @return DynamicM|object|null
     * @throws \think\exception\DbException
     */
    public static function getSingleDynamic($id)
    {
        $data = self::get($id);

        if (!$data) return (object)[];

        $attachment = json_decode($data['attachment'], true);
        $data['images'] = $attachment['images']??[];
        $data['video'] = $attachment['video']??[];
        $data['audio'] = $attachment['audio']??[];

        $data['stock'] = isset($data['stock'])?json_decode($data['stock'], true):[];

        unset($data->attachment);
        return $data;
    }
}