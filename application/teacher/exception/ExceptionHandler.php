<?php
namespace app\teacher\exception;


use app\teacher\lib\ApiResponse;
use Exception;
use think\exception\Handle;

class ExceptionHandler extends Handle
{
    public function render(Exception $e)
    {
        if ($e instanceof \InvalidArgumentException) {
            return ApiResponse::error(ApiResponse::ERR_FORM_INVALID,'参数错误: '.$e->getMessage(),false);
        }

        return parent::render($e);
    }
}