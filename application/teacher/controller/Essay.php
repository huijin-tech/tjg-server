<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2018/1/8
 * Time: 16:09
 */

namespace app\teacher\controller;


use app\common\model\DynamicFlow;
use app\common\model\SpecialColumn;
use app\common\model\teacher\TeacherEssay;
use app\common\model\teacher\TeacherEssayCatalog;
use app\teacher\lib\ApiResponse;
use app\teacher\lib\TeacherBaseController;
use think\Db;
use think\Request;

class Essay extends TeacherBaseController
{
    public function catalog() {
        $list = TeacherEssayCatalog::apiList();
        return ApiResponse::success($list);
    }

    public function list() {
        $form = $this->validFormData([
            ['catalog_id', 'integer',],
            ['column_id', 'integer',],
        ]);

        $catalog_id = intval($form['catalog_id'] ?? 0);
        if (!isset($form['column_id']) || ''===$form['column_id']) $column_id = -1;
        else $column_id = intval($form['column_id']);
        $keywords = strval($form['keywords'] ?? null);
        $page = TeacherEssay::manageList($this->limit, $this->offset, $catalog_id, $this->teacherId, $keywords, $column_id);

        return $page;
    }

    public function save() {
        $data = $this->validFormData([
            ['title','require','标题不能为空'],
            ['cover','require','封面不能为空'],
            ['summary','require','摘要不能为空'],
            ['content','require','内容不能为空'],
        ]);

        Db::transaction(function () use ($data) {
            $isNew = false;
            if (isset($data['id'])) {
                $model = TeacherEssay::get($data['id']);
            }
            if (empty($model)) {
                $isNew = true;
                $model = new TeacherEssay();
            }

            // 自动下载内容中的远程图片并同步到oss
            if (isset($data['content'])) $data['content'] = Files::imgSync($data['content']);

            // 绑定课程 zk20180528
            if (isset($data['bind_course'])) {
                // 转换json中有string类型数字为int类型数字
                $bind_course = json_decode(json_encode($data['bind_course'], JSON_UNESCAPED_UNICODE));
                foreach ($bind_course as &$bindCourseVal) {
                    if (is_numeric($bindCourseVal)) $bindCourseVal = intval($bindCourseVal);
                }
                $data['bind_course'] = json_encode($bind_course, JSON_UNESCAPED_UNICODE);
            }

            // 随机点赞数
            $data['num_liked'] = rand(19, 69);

            $model->data($data);
            $model->teacher_id = $this->teacherId;
            $model->save();

            if ($model->special_column_id > 0) {
                SpecialColumn::update(['column_updatetime' => time()], ['column_id' => $model->special_column_id]);
            }

            if ($isNew) $model->makeDynamicData($isNew,DynamicFlow::SOURCE_TEACHER);

        });

        return ApiResponse::success();
    }

    public function hide(int $id, string $hide = 'false') {
        TeacherEssay::setHide($id, $hide=='true', $this->teacherId);
        return ApiResponse::success();
    }

    public function del($id) {
        TeacherEssay::del($id, $this->teacherId);
        return ApiResponse::success();
    }

}