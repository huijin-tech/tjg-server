<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2018-05-10
 * @Time: 09:28
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： Dynamic.php
 */
namespace app\teacher\controller;

use app\teacher\lib\ApiResponse;
use app\teacher\lib\TeacherBaseController;
use app\teacher\model\DynamicM;
use think\Log;

class Dynamic extends TeacherBaseController {
    /**
     * lists
     * 动态列表
     * @api http://xxx/api/teacher/dynamic/lists
     *
     * @return ApiResponse
     * @throws \think\exception\DbException
     */
    public function lists()
    {
        $form = $this->validFormData([
            ['page', 'integer', '参数类型不正确'],
            ['limit', 'integer', '参数类型不正确'],
        ]);

        if (!isset($form['page'])) $form['page'] = 1;
        if (!isset($form['limit'])) $form['limit'] = 10;

        $result = DynamicM::getAllDynamic($this->teacherId, intval($form['page']), intval($form['limit']));

        $data = ApiResponse::success($result);

        // 分页总数计算
        $numTotal = DynamicM::where([
            'author_type' => 2,
            'author' => $this->teacherId
        ])->count('id');
        $pageTotal = ceil($numTotal/intval($form['limit']));
        $data->pageTotal = $pageTotal;

        return $data;
    }

    /**
     * detail
     * 动态详情
     *
     * @return ApiResponse
     * @throws \think\exception\DbException
     */
    public function detail()
    {
        $form = $this->validFormData([
            ['id', 'require|integer', '动态id不能为空|参数类型不正确'],
        ]);

        $result = DynamicM::getSingleDynamic(intval($form['id']));

        return ApiResponse::success($result);
    }

    /**
     * save
     * 保存动态数据
     *
     * @return ApiResponse|\think\Response|\think\response\Json|\think\response\Jsonp|\think\response\Redirect|\think\response\View|\think\response\Xml
     */
    public function save()
    {
        $form = $this->validFormData([
            ['type', 'require|integer', '动态类型不能为空|参数类型不正确'],
            ['content', 'require', '动态内容不能为空'],
        ]);

        $form['author_type'] = 2;
        $form['author'] = $this->teacherId;

        if (isset($form['id']) && $form['id']) {

            $form['attachment'] = [
                'images' => isset($form['images'])?$form['images']:[],
                'video' => isset($form['video'])?$form['video']:[],
                'audio' => isset($form['audio'])?$form['audio']:[],
            ];
            $form['attachment'] = json_encode($form['attachment']);
            unset($form['images'],$form['video'],$form['audio']);

            $result = DynamicM::updateDynamic(intval($form['id']), $form);
        } else {
            $form['times'] = time();

            $form['stock'] = $form['stock']??[];
            $form['stock'] = json_encode($form['stock']);

            $form['attachment'] = [
                'images' => $form['images']??[],
                'video' => $form['video']??[],
                'audio' => $form['audio']??[],
            ];
            $form['attachment'] = json_encode($form['attachment']);
            unset($form['images'],$form['video'],$form['audio']);

            // 随机点赞数
            $form['like_count_set'] = rand(19, 69);

            $result = DynamicM::createDynamic($form);
        }

        if ($result) return ApiResponse::success();
        else return ApiResponse::error(ApiResponse::ERR_OPERATE_FAILED);
    }

    /**
     * del
     * 删除动态数据
     *
     * @return ApiResponse|\think\Response|\think\response\Json|\think\response\Jsonp|\think\response\Redirect|\think\response\View|\think\response\Xml
     */
    public function del()
    {
        $form = $this->validFormData([
            ['id', 'require|integer', '动态id不能为空|参数类型不正确'],
        ]);

        $result = DynamicM::delDynamic(intval($form['id']));

        if ($result) return ApiResponse::success();
        else return ApiResponse::error(ApiResponse::ERR_OPERATE_FAILED);
    }
}