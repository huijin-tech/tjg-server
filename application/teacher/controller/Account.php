<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2018/1/4
 * Time: 14:08
 */

namespace app\teacher\controller;

use app\common\model\teacher\Teacher;
use app\teacher\behaviour\Auth;
use app\teacher\lib\ApiResponse;
use think\Request;

class Account
{

    public function login(Request $request) {

        // 检查空值
        $username = $request->param('username');
        $password = $request->param('password');
        if (!($username && $password)) {
            ApiResponse::error(ApiResponse::ERR_FORM_INVALID,'用户名和密码不能为空');
        }

        // 验证用户名和密码
        $teacher = Teacher::getByUserName($username);
        if (empty($teacher) || Teacher::passwordVerify($password, $teacher->password) == false) {
            ApiResponse::error(ApiResponse::ERR_OPERATE_FAILED,'用户名或密码错误');
        }

        // 保存登录状态
        $info = $teacher->info();
        $info['token'] = Auth::saveLogin($teacher);

        return ApiResponse::success($info);
    }

    public function logout() {
        Auth::clearLogin();
        return ApiResponse::success();
    }

}