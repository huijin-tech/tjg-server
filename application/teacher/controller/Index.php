<?php
/**
 * Created by PhpStorm.
 * User: jjj
 * Date: 2018/1/16
 * Time: 16:41
 */

namespace app\teacher\controller;


use app\common\model\Sales;
use app\teacher\lib\ApiResponse;

class Index
{
    public function sales() {
        $list = Sales::listAvailable();
        return ApiResponse::success($list);
    }

}