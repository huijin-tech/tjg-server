<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2018-05-28
 * @Time: 11:26
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： Course.php
 */
namespace app\teacher\controller;

use app\teacher\lib\ApiResponse;
use app\teacher\lib\TeacherBaseController;
use app\teacher\model\CurriculumM;

class Course extends TeacherBaseController {
    public function bind_article_course()
    {
        // zk 20180524 获取可绑定文章的课程的文章栏目id
        $form = $this->validFormData([]);
        $form['bindArticleColId'] = $form['bindArticleColId']??0;

        $result = CurriculumM::getBindArticleCourse($form['bindArticleColId']);

        return ApiResponse::success($result);
    }
}