<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件

use think\Response;
const ENCRYPT_KEY = 'zhangqian'; // 用户密码加密的key


// 公共函数
if (!function_exists('getPostJson')) {
    /**
     * 如果客户端上传JSON，此函数可以解析成array并返回
     *
     * @return array
     */
    function getPostJson() {
        $post = file_get_contents('php://input');
        $data = json_decode($post, true);
        return $data;
    }
}

if (!function_exists('passEncrypt')) {
    /**
     * passEncrypt
     * 密码加密处理
     *
     * @author zhengkai
     * @date 2017-08-02
     *
     * @param string $pass 需要加密的密码字符串
     * @return string
     */
    function passEncrypt($pass)
    {
        $pass = hash_hmac('sha256', $pass, ENCRYPT_KEY);
        return $pass;
    }
}


if (!function_exists('apiToken')) {
    /**
     * apiToken
     * api token生成
     *
     * @author zhengkai
     * @date 2017-08-01
     *
     * @param int $appId 应用id
     * @param string $appSecret 应用密钥
     * @return string
     */
    function apiToken($appId, $appSecret)
    {
        $token = md5(md5($appId.$appSecret));
        return $token;
    }
}

if (!function_exists('fetchTextFromHtml')) {
    /**
     * 过滤字符串里的html和空格
     *
     * @param $html
     * @return string
     */
    function fetchTextFromHtml($html) {
        return strtr(strip_tags($html), [' '=>'', '　'=>'']);
    }
}

if (!function_exists('stripHtml')) {
    /**
     * stripHtml
     * 过滤html与所有空格
     *
     * @param $str
     * @return mixed
     */
    function stripHtml($str)
    {
        return preg_replace('/\s|　|\&\w+;/', '', (strip_tags($str)));
    }
}

if (!function_exists('mobile_hidden')) {
    /*
     * mobile_hidden
     * 隐藏手机号码中间四位以*号代替
     *
     * @author zhengkai
     * @date 2017-08-24
     *
     * @param string/int $mobile 手机号码
     * */
    function mobile_hidden($mobile){
        $str=$mobile;
        $resstr=substr_replace($str,'****',4,4);
        return $resstr;
    }
}

if (!function_exists('time_greet')) {
    /**
     * time_greet
     * 根据时间显示问候语
     *
     * @return null|string
     */
    function time_greet(){
        $hour = date('H', time());
        $str = null;
        if ($hour < 6) {$str = "又是一个不眠夜！";}
        else if ($hour < 9){$str = "HI~ 早上好！";}
        else if ($hour < 12){$str = "上午工作顺利吗？";}
        else if ($hour < 14){$str = "中午好，吃了吗？";}
        else if ($hour < 17){$str = "下午好，别打盹哦！";}
        else if ($hour < 19){$str = "最美的夕阳总是傍晚出现！";}
        else if ($hour < 22){$str = "夜晚的景色总是让人着迷！";}
        else {$str = "夜深了，还不休息吗？";}

        return $str;
    }
}

if (!function_exists('array_sort')) {
    /**
     * array_sort
     * 数组指定键值排序
     *
     * @param array $array 需要排序的数组
     * @param string $keys 需要排序键值的键名
     * @param string $type 排序类型 asc desc
     * @return array|string
     */
    function array_sort($array,$keys,$type='asc'){
        if(!isset($array) || !is_array($array) || empty($array)){
            return [];
        }
        if(!isset($keys) || trim($keys)==''){
            return '';
        }
        if(!isset($type) || $type=='' || !in_array(strtolower($type),array('asc','desc'))){
            return '';
        }

        $keysvalue=[];
        foreach($array as $key=>$val){
            $val[$keys] = str_replace('-','',$val[$keys]);
            $val[$keys] = str_replace(' ','',$val[$keys]);
            $val[$keys] = str_replace(':','',$val[$keys]);
            $keysvalue[] =$val[$keys];
        }

        asort($keysvalue); //key值排序
        reset($keysvalue); //指针重新指向数组第一个

        $keysort = [];
        foreach($keysvalue as $key=>$vals) {
            $keysort[] = $key;
        }

        $keysvalue = [];
        $count=count($keysort);
        if(strtolower($type) != 'asc'){
            for($i=$count-1; $i>=0; $i--) {
                $keysvalue[] = $array[$keysort[$i]];
            }
        }else{
            for($i=0; $i<$count; $i++){
                $keysvalue[] = $array[$keysort[$i]];
            }
        }
        return $keysvalue?$keysvalue:[];
    }
}

if (!function_exists('page_array')) {
    /**
     * 数组分页函数  核心函数  array_slice
     * 用此函数之前要先将数据库里面的所有数据按一定的顺序查询出来存入数组中
     * $count   每页多少条数据
     * $page   当前第几页
     * $array   查询出来的所有数组
     * order 0 - 不变     1- 反序
     */
    function page_array($count,$page,$array,$order){
        $countpage = 0;
        $page = (empty($page))?'1':$page; // 判断当前页面是否为空 如果为空就表示为第一页面
        $start=($page-1)*$count;  // 计算每次分页的开始位置
        if($order==1){
            $array=array_reverse($array);
        }
        $totals = count($array);
        $countpage = ceil($totals/$count); // 计算总页面数
        $pagedata = array();
        $pagedata = array_slice($array,$start,$count);
        return $pagedata; // 返回查询数据
    }
}

if (!function_exists('tranTime')) {
    /**
     * 友好的时间显示
     *
     * @param int    $sTime 待显示的时间
     * @param string $type  类型. normal | mohu | full | ymd | other
     * @param string $alt   已失效
     * @return string
     */
    function tranTime($sTime,$type = 'normal', $alt = 'false') {
        if (!$sTime)
            return '';
        //sTime=源时间，cTime=当前时间，dTime=时间差
        $cTime      =   time();
        $dTime      =   $cTime - $sTime;
        $dDay       =   intval(date("z",$cTime)) - intval(date("z",$sTime));
        //$dDay     =   intval($dTime/3600/24);
        $dYear      =   intval(date("Y",$cTime)) - intval(date("Y",$sTime));
        //normal：n秒前，n分钟前，n小时前，日期
        if($type=='normal'){
            if( $dTime < 60 ){
                if($dTime < 10){
                    return '刚刚';    //by yangjs
                }else{
                    return intval(floor($dTime / 10) * 10)."秒前";
                }
            }elseif( $dTime < 3600 ){
                return intval($dTime/60)."分钟前";
                //今天的数据.年份相同.日期相同.
            }elseif( $dYear==0 && $dDay == 0  ){
                //return intval($dTime/3600)."小时前";
                return '今天'.date('H:i',$sTime);
            }elseif($dYear==0){
                return date("m月d日 H:i",$sTime);
            }else{
                return date("Y-m-d H:i",$sTime);
            }
        }elseif($type=='mohu'){
            if( $dTime < 60 ){
                return $dTime."秒前";
            }elseif( $dTime < 3600 ){
                return intval($dTime/60)."分钟前";
            }elseif( $dTime >= 3600 && $dDay == 0  ){
                return intval($dTime/3600)."小时前";
            }elseif( $dDay > 0 && $dDay<=7 ){
                return intval($dDay)."天前";
            }elseif( $dDay > 7 &&  $dDay <= 30 ){
                return intval($dDay/7) . '周前';
            }elseif( $dDay > 30 ){
                return intval($dDay/30) . '个月前';
            }
            //full: Y-m-d , H:i:s
        }elseif($type=='full'){
            return date("Y-m-d , H:i:s",$sTime);
        }elseif($type=='ymd'){
            return date("Y-m-d",$sTime);
        }else{
            if( $dTime < 60 ){
                return $dTime."秒前";
            }elseif( $dTime < 3600 ){
                return intval($dTime/60)."分钟前";
            }elseif( $dTime >= 3600 && $dDay == 0  ){
                return intval($dTime/3600)."小时前";
            }elseif($dYear==0){
                return date("Y-m-d H:i:s",$sTime);
            }else{
                return date("Y-m-d H:i:s",$sTime);
            }
        }
    }
}

if (!function_exists('array_to_object')) {
    /**
     * 数组 转 对象
     *
     * @param array $arr 数组
     * @return object
     */
    function array_to_object($arr) {
        if (gettype($arr) != 'array') {
            return;
        }
        foreach ($arr as $k => $v) {
            if (gettype($v) == 'array' || getType($v) == 'object') {
                $arr[$k] = (object)array_to_object($v);
            }
        }

        return (object)$arr;
    }
}

if (!function_exists('object_to_array')) {
    /**
     * 对象 转 数组
     *
     * @param object $obj 对象
     * @return array
     */
    function object_to_array($obj) {
        $obj = (array)$obj;
        foreach ($obj as $k => $v) {
            if (gettype($v) == 'resource') {
                return;
            }
            if (gettype($v) == 'object' || gettype($v) == 'array') {
                $obj[$k] = (array)object_to_array($v);
            }
        }

        return $obj;
    }
}

if (!function_exists('checkUserToken')) {
    /**
     * checkUserToken
     * 验证 user token，如果token无效则抛出异常，客户端接收到错误码和提示信息
     *
     * @author zhengkai
     * @date 2017-08-02
     *
     * @param bool $forceAuth 强制登录
     * @return int 返回用户ID
     */
    function checkUserToken(bool $forceAuth = false)
    {
        $request = \think\Request::instance();
        $userId = $request->header('uid');
        $token = $request->header('token');

        // 从缓存中获取 user token
        $cacheToken = \think\Cache::tag('user_token')->get('user_'.$userId, false);

        if (empty($cacheToken) || $token != $cacheToken) {
            if ($forceAuth) app\common\model\ApiResponse::error(1011, '登录超时或未登录');
            else return 0;
        } else {
            $request->bind('uid',$userId);
            $request->bind('token',$token);
            return intval($userId);
        }
    }
}

if (!function_exists('checkUserLogin')) {
    function checkUserLogin(bool $forceAuth = false)
    {
        $request = \think\Request::instance();
        $userId = $request->param('uid');
        if (is_null($userId)) $userId = $request->header('uid');
        $token = $request->param('token');
        if (is_null($token)) $token = $request->header('token');

        // 从缓存中获取 user token
        $cacheToken = \think\Cache::tag('user_token')->get('user_'.$userId, false);

        if (empty($cacheToken) || $token != $cacheToken) {
            if ($forceAuth) throw new \think\exception\HttpResponseException(Response::create('未登录','',403));
            else return 0;
        } else {
            return intval($userId);
        }
    }
}

if (!function_exists('secToTime')) {
    /**
     *      把秒数转换为时分秒的格式
     *      @param Int $times 时间，单位 秒
     *      @return String
     */
    function secToTime($times){
        //$result = '00:00:00';
        $result = '00:00';
        if ($times>0) {
            $hour = floor($times/3600);
            $minute = floor(($times-3600 * $hour)/60);
            $second = floor((($times-3600 * $hour) - 60 * $minute) % 60);
            // $result = $hour.':'.$minute.':'.$second;
            $result = sprintf("%02d", $minute).':'.sprintf("%02d", $second);
        }
        return $result;
    }
}

if (!function_exists('getImages')) {
    /**
     * getImages
     * 获取文章中的所有图片
     *
     * @author zhengkai
     * @date 2018-04-23
     *
     * @param $content
     * @param string $order
     * @return string
     */
    function getImages($content, $order='ALL')
    {
        $pattern = "/<img.*?src=[\'|\"](.*?(?:[\.gif|\.jpg|\.jpeg|\.png|\.bmp|\.webp]))[\'|\"].*?[\/]?>/";
        preg_match_all($pattern, $content,$match);
        if(isset($match[1])&&!empty($match[1])){
            if($order==='ALL'){
                return $match[1];
            }
            if(is_numeric($order)&&isset($match[1][$order])){
                return $match[1][$order];
            }
        }
        return '';
    }
}

if (!function_exists('getAudio')) {
    /**
     * getAudio
     * 获取文章中的所有音频地址
     *
     * @author zhengkai
     * @date 2018-04-23
     *
     * @param $content
     * @param string $order
     * @return string
     */
    function getAudio($content, $order='ALL')
    {
        $pattern = "/<audio.*?src=[\'|\"](.*?(?:[\.mp3|\.wav]))[\'|\"].*?[\/]?>/";
        preg_match_all($pattern, $content,$match);
        if(isset($match[1])&&!empty($match[1])){
            if($order==='ALL'){
                return $match[1];
            }
            if(is_numeric($order)&&isset($match[1][$order])){
                return $match[1][$order];
            }
        }
        return '';
    }
}

if (!function_exists('getVideo')) {
    /**
     * getVideo
     * 获取文章中的所有视频地址
     *
     * @author zhengkai
     * @date 2018-04-23
     *
     * @param $content
     * @param string $order
     * @return string
     */
    function getVideo($content, $order='ALL')
    {
        $pattern = "/<video.*?src=[\'|\"](.*?(?:[\.mp4|\.flv]))[\'|\"].*?[\/]?>/";
        preg_match_all($pattern, $content,$match);
        if(isset($match[1])&&!empty($match[1])){
            if($order==='ALL'){
                return $match[1];
            }
            if(is_numeric($order)&&isset($match[1][$order])){
                return $match[1][$order];
            }
        }
        return '';
    }
}

if (!function_exists('num2tring')) {
    /**
     * num2tring
     * 数字达到一千转成1k
     *
     * @author zhengkai
     * @date 2018-04-28
     *
     * @param $num
     * @return string
     */
    function num2tring($num) {
        /*if ($num >= 10000) {
            $num = round($num / 10000 * 100) / 100 .' W';
        } elseif($num >= 1000) {
            $num = round($num / 1000 * 100) / 100 . ' K';
        } else {
            $num = $num;
        }*/
        if($num >= 1000) {
            $num = round($num / 1000 * 100) / 100 . 'K';
        } else {
            $num = $num;
        }
        return $num;
    }
}

if (!function_exists('')) {
    /**
     * countDataNum
     * 统计数据处理
     * 实际数据+运营数据
     *
     * @author zhengkai
     * @date 2018-04-28
     *
     * @param int $old  实际数据
     * @param int $new  运营数据
     * @param bool $format 是否格式化，1000=>1k、10000=>1w
     * @return string
     */
    function countDataNum($old, $new, $format=false)
    {
        $newNum = intval($old)>0?(intval($old)+intval($new)):intval($new);

        if ($format) $newNum = num2tring($newNum);

        return $newNum?:0;
    }
}

if (!function_exists('str_cut')) {
    /**
     * 字符截取 支持UTF8/GBK
     * @param $string
     * @param $length
     * @param $dot
     */
    define('CHARSET', 'utf-8');
    function str_cut($string, $length, $dot = '...') {
        $strlen = strlen($string);
        if($strlen <= $length) return $string;
        $string = str_replace(array(' ','&nbsp;', '&amp;', '&quot;', '&#039;', '&ldquo;', '&rdquo;', '&mdash;', '&lt;', '&gt;', '&middot;', '&hellip;'), array('∵',' ', '&', '"', "'", '“', '”', '—', '<', '>', '·', '…'), $string);
        $strcut = '';
        if(strtolower(CHARSET) == 'utf-8') {
            $length = intval($length-strlen($dot)-$length/3);
            $n = $tn = $noc = 0;
            while($n < strlen($string)) {
                $t = ord($string[$n]);
                if($t == 9 || $t == 10 || (32 <= $t && $t <= 126)) {
                    $tn = 1; $n++; $noc++;
                } elseif(194 <= $t && $t <= 223) {
                    $tn = 2; $n += 2; $noc += 2;
                } elseif(224 <= $t && $t <= 239) {
                    $tn = 3; $n += 3; $noc += 2;
                } elseif(240 <= $t && $t <= 247) {
                    $tn = 4; $n += 4; $noc += 2;
                } elseif(248 <= $t && $t <= 251) {
                    $tn = 5; $n += 5; $noc += 2;
                } elseif($t == 252 || $t == 253) {
                    $tn = 6; $n += 6; $noc += 2;
                } else {
                    $n++;
                }
                if($noc >= $length) {
                    break;
                }
            }
            if($noc > $length) {
                $n -= $tn;
            }
            $strcut = substr($string, 0, $n);
            $strcut = str_replace(array('∵', '&', '"', "'", '“', '”', '—', '<', '>', '·', '…'), array(' ', '&amp;', '&quot;', '&#039;', '&ldquo;', '&rdquo;', '&mdash;', '&lt;', '&gt;', '&middot;', '&hellip;'), $strcut);
        } else {
            $dotlen = strlen($dot);
            $maxi = $length - $dotlen - 1;
            $current_str = '';
            $search_arr = array('&',' ', '"', "'", '“', '”', '—', '<', '>', '·', '…','∵');
            $replace_arr = array('&amp;','&nbsp;', '&quot;', '&#039;', '&ldquo;', '&rdquo;', '&mdash;', '&lt;', '&gt;', '&middot;', '&hellip;',' ');
            $search_flip = array_flip($search_arr);
            for ($i = 0; $i < $maxi; $i++) {
                $current_str = ord($string[$i]) > 127 ? $string[$i].$string[++$i] : $string[$i];
                if (in_array($current_str, $search_arr)) {
                    $key = $search_flip[$current_str];
                    $current_str = str_replace($search_arr[$key], $replace_arr[$key], $current_str);
                }
                $strcut .= $current_str;
            }
        }
        return $strcut.$dot;
    }
}

if (!function_exists('changeCny')) {
    /**
     * 人民币转换大写
     */
    function changeCny($ns)
    {
        static $cnums=array("零","壹","贰","叁","肆","伍","陆","柒","捌","玖"),
        $cnyunits=array("","圆","角","分"),
        $grees=array("","拾","佰","仟","万","拾","佰","仟","亿");
        list($ns1,$ns2)=explode(".",$ns,2);

        $ns2=array_filter(array($ns2[1],$ns2[0])); //转为数组

        $arrayTemp=_cny_map_unit(str_split($ns1),$grees);


        $ret=array_merge($ns2,array(implode("",$arrayTemp),"")); //处理整数

        $arrayTemp=_cny_map_unit($ret,$cnyunits);

        $ret=implode("",array_reverse($arrayTemp));     //处理小数

        return str_replace(array_keys($cnums),$cnums,$ret);
    }
    function _cny_map_unit($list,$units) {
        $ul=count($units);
        $xs=array();
        foreach (array_reverse($list) as $x) {
            $l=count($xs);

            if ($x!="0" || !($l%4)){

                $n=($x=='0'?'':$x).($units[($l)%$ul]);
            }else{
                $n=is_array($xs) && is_array($xs[0])&&is_numeric($xs[0][0])?$x:'';
            }
            array_unshift($xs,$n);
        }
        return $xs;
    }
}
