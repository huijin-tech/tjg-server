<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2017-09-14
 * @Time: 17:23
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： jiguang.php
 */
namespace library;

use think\Config;
use think\Log;

class JPushZDY{
    private $appKey; // 待发送的应用程序(appKey)，只能填一个。
    private $masterSecret; // 主密码
    private $production = false;

    const URL = 'https://api.jpush.cn/v3/push'; // 推送的地址

    private static $instance = null;
    public static function getInstance() :JPushZDY {
        if (empty(self::$instance)) {
            $appKey = Config::get('jiguang.appKey');
            $masterSecret = Config::get('jiguang.masterSecret');
            $production = Config::get('jiguang.production');
            self::$instance = new self($appKey, $masterSecret, $production);
        }
        return self::$instance;
    }

    //若实例化的时候传入相应的值则按新的相应值进行
    public function __construct($appKey, $masterSecret, $production = false) {
        $this->appKey = $appKey;
        $this->masterSecret = $masterSecret;
        $this->production = $production;
    }
    //极光推送的类
    //文档见：http://docs.jpush.cn/display/dev/Push-API-v3


    const DEFAULT_RECEIVER = 'all';
    /*  $receiver 接收者的信息
     all 字符串 该产品下面的所有用户. 对app_key下的所有用户推送消息
    tag(20个)Array标签组(并集): tag=>array('昆明','北京','曲靖','上海');
    tag_and(20个)Array标签组(交集): tag_and=>array('广州','女');
    alias(1000)Array别名(并集): alias=>array('93d78b73611d886a74*****88497f501','606d05090896228f66ae10d1*****310');
    registration_id(1000)注册ID设备标识(并集): registration_id=>array('20effc071de0b45c1a**********2824746e1ff2001bd80308a467d800bed39e');
    */
    // $content 推送的内容。
    // $m_type 推送附加字段的类型(可不填) http,tips,chat....
    // $m_txt 推送附加字段的类型对应的内容(可不填) 可能是url,可能是一段文字。
    // $m_time 保存离线时间的秒数默认为一天(可不传)单位为秒
    public function push($receiver = null, $content = '', $extras = null, $m_time = '86400'){
        $base64=base64_encode($this->appKey.':'.$this->masterSecret);
        $header=array("Authorization:Basic $base64","Content-Type:application/json");
        $data = array();
        $data['platform'] = 'all'; // 目标用户终端手机的平台类型android,ios,winphone
        // 推送目标
        $audience = [
            'tag' => [
                $this->production ? 'release' : 'debug',
            ],
        ];
        if ($receiver && $receiver!=self::DEFAULT_RECEIVER) {
            $audience['alias'] = $receiver;
        }
        $data['audience'] = $audience;

        if (empty($extras)) $extras = new \stdClass();
        // 推送通知
        $data['notification'] = array(
            //统一的模式--标准模式
            "alert"=>$content,

            //安卓自定义
            "android"=>array(
                "title"=>"",
                'builder_id' => 1,
                'extras' => $extras
            ),

            //ios的自定义
            'ios'=>[
                'badge' => '+1',
                'sound' => 'default',
                'extras' => $extras
            ]
        );

        //附加选项
        $data['options'] = array(
            "sendno"=>time(),
            "time_to_live"=>$m_time, //保存离线时间的秒数默认为一天
            "apns_production"=>$this->production, //布尔类型   指定 APNS 通知发送环境：0开发环境，1生产环境。或者传递false和true
        );
        $param = json_encode($data);
        $res = $this->push_curl($param,$header);

        if ($res) {       // 得到返回值--成功已否后面判断
            return $res;
        } else {          // 未得到返回值--返回失败
            return false;
        }
    }

    //推送的Curl方法
    public function push_curl($param="",$header="") {
        if (empty($param)) { return false; }
        $postUrl = self::URL;
        $curlPost = $param;
        $ch = curl_init(); // 初始化curl
        curl_setopt($ch, CURLOPT_URL,$postUrl); // 抓取指定网页
        curl_setopt($ch, CURLOPT_HEADER, 0); // 设置header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // 要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_POST, 1); // post提交方式
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$header); // 增加 HTTP Header（头）里的字段
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // 终止从服务端进行验证
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        $data = curl_exec($ch); // 运行curl

        $errNo = curl_errno($ch);
        $errMsg = curl_error($ch);
        curl_close($ch);
        if ($errNo) {
            Log::error("$errNo: $errMsg");
            return null;
        }
        return json_decode($data,true);
    }

    public static function pushUsers($users, $content = '', $extras = null) {
        if (!is_array($users)) $users = [$users];
        return self::getInstance()->push($users, $content, $extras);
    }

    public static function pushAll($content = '',$extras = null) {
        return self::getInstance()->push(self::DEFAULT_RECEIVER, $content, $extras);
    }

}