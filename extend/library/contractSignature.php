<?php
/**
 * Created by PhpStorm.
 *
 * @Date: 2017-10-31
 * @Time: 10:06
 * @Author: cdkay
 * @Email: network@iyuanma.net
 *
 * @File： contractSignature.php
 */
namespace library;

use Curl\Curl;
use think\Request;

/**
 * 电子合同签章处理
 *
 * @author zhengkai
 * @date 2017-10-31
 *
 * Class contractSignature
 * @package library
 */
class contractSignature {
    protected static $instance=null;

    /**
     * @return null|static
     */
    public static function get_instance()
    {
        if (empty(self::$instance)) {
            self::$instance = new static('http://183.62.121.155/webapi/myapp/', 'TGW_COMMONKEY', 'commonkey');
        }

        return self::$instance;
    }

    private $url;
    private $secretKey;
    private $secretId;
    protected $curl;
    private function __construct($url, $secretKey, $secretId)
    {
        $this->url = $url;
        $this->key = $secretKey;
        $this->id = $secretId;

        $this->curl = new Curl();
    }

    /**
     * curlRemoteData
     * 请求远程接口
     *
     * @author zhengkai
     * @date 2017-10-31
     *
     * @param string $api 接口
     * @param array $param 请求参数
     * @return mixed
     * @throws \Exception
     */
    private function curlRemoteData($api, $param=[])
    {
        /***************对请求参数 按参数名 做字典序升序排列，注意此排序区分大小写*************/
        $arr = array_merge(['time'=>time(), 'secretId'=>$this->id], $param);
        ksort($arr);

        /**********************************生成签名原文**********************************
         * 将 请求方法, URI地址,及排序好的请求参数  按照下面格式  拼接在一起, 生成签名原文
         *secretId=xxxxx&time=14875345&token=xxxxxxxxxx&userid=xxxxx
         * ****************************************************************************/
        $SigTxt = "";
        $isFirst = true;
        foreach ($arr as $key => $value)
        {
            if (!$isFirst) $SigTxt = $SigTxt."&";
            $isFirst= false;

            /*拼接签名原文时，如果参数名称中携带_，需要替换成.*/
            if(strpos($key, '_')) $key = str_replace('_', '.', $key);

            $SigTxt=$SigTxt.$key."=".$value;
        }

        /*********************根据签名原文字符串 $SigTxt，生成签名 Signature******************/
        $Signature = base64_encode(hash_hmac('sha1', $SigTxt, $this->secretKey, true));


        /***************拼接请求串,对于请求参数及签名，需要进行urlencode编码********************/
        $Req = "sign=".urlencode($Signature);
        foreach ($arr as $key => $value)
        {
            $Req=$Req."&".$key."=".urlencode($value);
        }
        parse_str($Req, $paramArr); // 将url地址参数转成数组形式

        if (Request::instance()->isPost()) {
            $this->curl->post($this->url.$api, $paramArr);
        } else if (Request::instance()->isGet()) {
            $this->curl->get($this->url.$api, $paramArr);
        }

        $result = json_decode(json_encode($this->curl->response), true);

        if (200 != $result['ret']) {
            throw new \Exception($result['msg']);
        }

        $data = $result['data'];
        if (0 != $data['code']) {
            throw new \Exception($data['msg']);
        }

        return $data;
    }

    /**
     * createPdf
     * 生成PDF电子签章后的电子合同文件
     *
     * @author zhengkai
     * @date 2017-10-31
     *
     * @param string $contractNo 合同编号
     * @param string $realName 用户真实姓名
     * @param string $cardNo 身份证号码
     * @param string $mobile 手机号码
     * @param float $price 产品价格
     * @param string $productName 产品名称
     * @return mixed
     */
    public function createPdf($contractNo, $realName, $cardNo, $mobile, $price, $productName)
    {

        $data = [
            'contractNo' => $contractNo, // 合同编号
            'name' => $realName, // 用户姓名(真实姓名)
            'idNumber' => $cardNo, // 身份证号码
            'phone' => $mobile, // 联系电话
            'svcCost' => $price, // 商品单价
            'svcName' => $productName, // 商品名称
        ];
        $result = $this->curlRemoteData('Signature/CreateSignaturePdfForCourse', $data);

        return $result;
    }

    const NO_PREFIX = 'DLHX-TL';
    public static function generateNo($no) {

        $serial = 1;
        $year = date('Y');
        $lastYear = substr($no, strlen(self::NO_PREFIX),4);
        if ($year == $lastYear) {
            if ($no) $serial = intval(substr($no,strlen(self::NO_PREFIX) + 4,5)) + 1;
        } else {
            $serial = 1;
        }

        $no = sprintf('%s%s%05d',self::NO_PREFIX, $year, $serial);
        return $no;
    }

    public static function fixUrl($url) {
        return 'http://183.62.121.155'.parse_url($url,PHP_URL_PATH);
    }

}